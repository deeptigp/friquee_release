function plotTsne(toCompress, feat, assignments)
    SAVEPATH = '../interestingness/Results/';
    addpath('t-SNE/');
    
    if(nargin < 1) % no arguments are passed
        load('../interestingness/Data/fc5Features.mat');
        feat = fc5Features;
        fN = size(feat, 2);    
        toCompress = 0;
    end
    
    toCompress = 0;
    
    if(toCompress)
        K = 1000;
        A = zeros(K,fN);

        for i = 1:K
            A(i,:) = randn(1,fN);
        end

        compressedFeat = A*feat';
        feat  = compressedFeat';

    end

    if(nargin < 1) % no arguments are passed
        numClusters = 3;
        feat = normr(feat);

        tic
        [centers, assignments] = vl_kmeans(feat', numClusters, 'Initialization', 'plusplus');
        toc
    end
    
    h1 = figure;
    hist(single(assignments))

    colors = {'r','g','b','k','m','c'};
    shapes = {'+','d','*','o','.','x'};


    tsneFC = tsne(feat);
    h2 = figure;

    for i = 1:numClusters
        plot(tsneFC(find(assignments == i),:),strcat(colors{i},shapes{i}))
        if(i==1)
            hold on
        end
    end
    %legend({'1','2','3','4','5','6'});
    %title(strcat('tSNE of compressed fc7 features ( K =', num2str(K),')'));

    title(strcat('tSNE of fc5 features (num Clusters  =', num2str(numClusters),')'));

    print(h2, strcat(SAVEPATH,'tsne-fc5-',num2str(numClusters)),'-djpeg');
end