function [medLCC, medRHO] = executeSVM1(data,labels, trainTestSplit, trainFile, testFile)
    load(trainFile);
    load(testFile);
    
    trainIndx = TrainingNIter(:,trainTestSplit);
    testIndx = TestingNIter(:,trainTestSplit);
        
    trainY = labels(trainIndx)';
    testY = labels(testIndx)';

    trainFeats = data(trainIndx,:);
    testFeats = data(testIndx,:);

    
    [size(trainFeats) size(testFeats)]
    
    % I want to capture the running time per image.
    [medLCC, medRHO] = runSVM1(trainFeats,trainY,testFeats,testY);
end