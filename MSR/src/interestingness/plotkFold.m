%%Plotting some stats for kFold
SAVEPATH = 'Results/plots/';
load('Results/kFold_567.mat');

%% Average of abs differences for just pool5 across all images.

mp5 = mean(kFold_567.absDiffAll(:,1));
std5 = std(kFold_567.absDiffAll(:,1));

mp6 = mean(kFold_567.absDiffAll(:,2));
std6 = std(kFold_567.absDiffAll(:,2));

mp7 = mean(kFold_567.absDiffAll(:,3));
std7 = std(kFold_567.absDiffAll(:,3));

mpAll = mean(kFold_567.minVAll);
stdAll = std(kFold_567.minVAll);

fig1 = figure;

plot([mp5,mp6,mp7,mpAll],'r+','LineWidth',20);
hold on;
plot([std5, std6, std7, stdAll],'b*','LineWidth',20)
legend('Mean of the absolute diffs','Std of the absolute diffs')
ax = gca;
ax.XTick = [1 2 3 4];
ax.XTickLabel = {'pool5','fc6','fc7','bestOf3'};
ylabel('Mean of the absolute differences');
print(fig1, strcat(SAVEPATH,'plot_avgAbsDiff_kFold567'),'-djpeg');