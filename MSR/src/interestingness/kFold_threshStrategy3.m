%% Initializations
load('Results/kFold_567.mat');
thresh = 0.03;

classLabels = zeros(1,2222);

globalSet = [1:2222];

del56 = kFold_567.absDiffAll(:,1) - kFold_567.absDiffAll(:,2);

del57 = kFold_567.absDiffAll(:,1) - kFold_567.absDiffAll(:,3);


class1 = (del56 <= -thresh & del57 <= -thresh);

class0 = (del56 > thresh | del57 > thresh);

classLabels(class1) = 1;

notFC5Indexes = setdiff(globalSet,find(class1));

%% for fc6

del65 = kFold_567.absDiffAll(notFC5Indexes,2) - kFold_567.absDiffAll(notFC5Indexes,1);

del67 = kFold_567.absDiffAll(notFC5Indexes,2) - kFold_567.absDiffAll(notFC5Indexes,3);

class1 = (del65 <= -thresh & del67 <= -thresh);

class0 = (del65 > thresh | del67 > thresh);

unUsedClass = setdiff(notFC5Indexes,union(notFC5Indexes(class0),notFC5Indexes(class1)));

classLabels(notFC5Indexes(class1)) = 2;

%% fc7

notFC6Indexes = setdiff(notFC5Indexes,notFC5Indexes(class1));
classLabels(notFC6Indexes) = 3;

% del75 = kFold_567.absDiffAll(notFC6Indexes,3) - kFold_567.absDiffAll(notFC6Indexes,1);
% 
% del76 = kFold_567.absDiffAll(notFC6Indexes,3) - kFold_567.absDiffAll(notFC6Indexes,2);
% 
% class1 = (del75 <= -thresh & del76 <= -thresh);
% 
% class0 = (del75 > thresh | del76 > thresh);



%fc7ClassLabels(class0) = 0;
kFold_567.strategy3DerivedLabels = classLabels;

%save('kFold_567_strat3_thresh0.03.mat','kFold_567');

%% Save the labels
% save('Data/pool5ClassLabels.mat','pool5ClassLabels');
% save('Data/fc6ClassLabels.mat','fc6ClassLabels');
% save('Data/fc7ClassLabels.mat','fc7ClassLabels');
