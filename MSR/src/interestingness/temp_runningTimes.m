featComputeTime = [ 0.0777 0.0397 0.0248];
perLayerFeatComputeTime = [ 0.1420 0.1172 0.0775 0.0777]; % fc7-fc6-pool5-pool5Compressed
totalClassificationTime =0;

totalTestImgs = size(cascadeClassifierResults_aggre567_thresh_linearC_pool5.testIndx,1);

numDataForBC = [size(cascadeClassifierResults_aggre567_thresh_linearC_pool5.testIndx,1) size(cascadeClassifierResults_aggre567_thresh_linearC_fc6.testIndx,1) size(cascadeClassifierResults_aggre567_thresh_linearC_fc7.testIndx,1)];

totalClassificationTime = cascadeClassifierResults_aggre567_thresh_linearC_pool5.runTimeForAllImgs + cascadeClassifierResults_aggre567_thresh_linearC_fc6.runTimeForAllImgs;
if(exist('cascadePredictedResults_aggre567_thresh_linearC_fc7'))
    totalRegressionTime = cascadePredictedResults_aggre567_thresh_linearC_pool5.runTimeForAllImgs5 + cascadePredictedResults_aggre567_thresh_linearC_fc6.runTimeForAllImgs6 + cascadePredictedResults_aggre567_thresh_linearC_fc7.runTimeForAllImgs7;
else
    totalRegressionTime = cascadePredictedResults_aggre567_thresh_linearC_pool5.runTimeForAllImgs5 + cascadePredictedResults_aggre567_thresh_linearC_fc6.runTimeForAllImgs6;
end

totalFeatureComputeTime = sum(numDataForBC.*featComputeTime);

totalTestTime = totalClassificationTime + totalRegressionTime + totalFeatureComputeTime;

individualFeatCompTime = totalTestImgs.*perLayerFeatComputeTime;

