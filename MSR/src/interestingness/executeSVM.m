%DATA_PATH = '/media/deepti/I/data/interestingness/';
%load(strcat(DATA_PATH,'imNet/Data/groundTruthLabels_10Class.mat'));
% 
% labels = groundTruthLabels;
% 
% load(strcat(DATA_PATH,'imNet/Results/kFold_567_10Class.mat'));
% 
% load(strcat(DATA_PATH,'imNet/Data/pool5Features_10Class.mat'));
% 
% load(strcat(DATA_PATH,'imNet/Data/fc6Features_10Class.mat'));
% 
% load(strcat(DATA_PATH,'imNet/Data/fc7Features_10Class.mat'));
% 
% load(strcat(DATA_PATH,'imNet/Data/TrainingNIter_10Class.mat'));
% load(strcat(DATA_PATH,'imNet/Data/TestingNIter_10Class.mat'));
% [medACC, testY, Y_hat, runTimeForAllImgs] = executeSVM(fc7Features,groundTruthLabels,1, strcat(DATA_PATH,'imNet/Data/TrainingNIter_10Class.mat') , strcat(DATA_PATH,'imNet/Data/TestingNIter_10Class.mat'), 'fc7');

%[medACC, testY, Y_hat, runTimeForAllImgs] = executeSVM(pool5Features_compressed, interestingness_score, 1, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat', 'pool5');

function [medLCC, medRHO, Y_hat, rmse, psnrVal, runTimeForAllImgs, runTimePerImg] = executeSVM(data,labels, trainTestSplit, trainFile, testFile, layer)
load(trainFile);
load(testFile);

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

trainY = labels(trainIndx)';
testY = labels(testIndx)';

trainFeats = data(trainIndx,:);
testFeats = data(testIndx,:);

[size(trainY) size(testY)]

[size(trainFeats) size(testFeats)]

% I want to capture the running time per image.
[medLCC, medRHO, Y_hat, rmse, psnrVal, runTimeForAllImgs, runTimePerImg] = temp_linearSVR(trainFeats,trainY',testFeats,testY',layer);
%save(['imNet/models/trainedModel_',layer,'.mat'],'trainedModel');
end


