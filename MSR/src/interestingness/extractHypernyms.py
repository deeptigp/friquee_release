import numpy as np
import scipy.io as sio
import random
from textblob import Word
from nltk.corpus import wordnet as wn
from sklearn.cluster import KMeans, spectral_clustering
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def readMatFile(fName, key):
	""" This method reads a mat file and converts to an ndarray and returns the array
	fName: Name of the mat file
	key: Name of the structure that is stored in the matfile.
	"""

	mat_contents = sio.loadmat(fName)
	retArray = mat_contents[key]
	return retArray

def getFirstSynsets(memClassLabels):

	numImages = len(memClassLabels)

	allSynsets = []
	allHypernyms = []

	dictSynsets = dict()

	globalCounter = 0

	allWordPresence = []

	imagesUnderConsider = []

	for i in xrange(numImages):
		w = memClassLabels[i][0][0].encode('ascii','ignore').strip() # the first word

		word = Word(w)

		if not word.synsets:
			continue

		imagesUnderConsider.append(i)

		syn = word.synsets[0]

		allSynsets.append(syn)

		hp = syn.hypernym_paths()

		hp = hp[0] #Consider the first path.

		allHypernyms.append(hp)

		wPres = []

		for h in xrange(len(hp)):
			if(hp[h] not in dictSynsets):
				dictSynsets[hp[h]] = globalCounter
				wPres.append(globalCounter)

				globalCounter = globalCounter+1
			else:
				wPres.append(dictSynsets[hp[h]])

		allWordPresence.append(wPres)

	return allSynsets,allHypernyms, dictSynsets, allWordPresence, np.array(imagesUnderConsider)

def formBoGFeatures(lenDict, allWordPresence):
	perImageFeatures = []
	# For each image
	for i in range(0,len(allWordPresence)):
		x = np.zeros(lenDict)
		x[allWordPresence[i]] = 1
		perImageFeatures.append(x)

	return perImageFeatures

#Given synsetS (N), return NXN matrix of pairwise similairties
def getSimilarity(allSynsets):

	numSynsets = len(allSynsets)

	simiMatrix = np.ones((numSynsets, numSynsets))

	for i in range(numSynsets):
		for j in range(i,numSynsets):
			s1 = allSynsets[i]
			s2 = allSynsets[j]

			simiMatrix[i,j] = wn.lch_similarity(s1,s2)

	return simiMatrix

def kMeansCluster(perImageFeatures, numClusters):
	kMeanEst = KMeans(n_clusters=numClusters)
	kMeanEst.fit(perImageFeatures)
	labels = kMeanEst.labels_
	return labels

def spectralCluster(simiMatrix, numClusters):
	#labels = spectral_clustering(simiMatrix, n_clusters=4, eigen_solver='arpack')
	labels = spectral_clustering(simiMatrix, n_clusters=numClusters)
	return labels

##incomplete.
def displayImages(labels, numClusters):
	memFileNames = readMatFile('Data/memFileNames.mat','memFileNames')
	randTestImgs = 15

	classes = np.array(range(numClusters))
	SAVE_PATH = '/media/deepti/I/data/ImageData/memorabilityImages/'
	for c in classes:
		print 'Class === ', c
		imgIndxs = imagesUnderConsider[labels == c]

		for i in range(randTestImgs):
			print allSynsets[imgIndxs[i]], memFileNames[imgIndxs[i]]
			imName = SAVE_PATH + memFileNames[imgIndxs[i]][0][0].encode('ascii','ignore').strip()
			img=mpimg.imread(imName)

			plt.subplot(numClusters, randTestImgs, randTestImgs*(c-1) +i+1)
			plt.imshow(img)
			plt.axis('off')

	plt.show()

if __name__ == '__main__':

	numClusters = 3

	memDataClassLabels = readMatFile('Results/memDataClassLabels.mat','memDataClassLabels')
	(allSynsets, allHypernyms, dictSynsets, allWordPresence, imagesUnderConsider) = getFirstSynsets(memDataClassLabels)

	simiMatrix = getSimilarity(allSynsets)

	perImageFeatures = formBoGFeatures(len(dictSynsets), allWordPresence)

	kMeanLabels = kMeansCluster(perImageFeatures, numClusters)

	plt.hist(kMeanLabels)

	plt.show()

	spectralLabels = spectralCluster(simiMatrix, numClusters)

	plt.hist(kMeanLabels)

	plt.show()



	## KMeans: Display images belong to each class.

