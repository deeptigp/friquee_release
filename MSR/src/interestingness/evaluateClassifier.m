%% This method runs an SVR on the set of datapoints that got classified to have the current layer as the label. This is just to evaluate if the binary classifier is doing a good job.. For ex: If 20 data points got the label as pool5, we use pool5Features to predict their interestingness scores in this method.
function predictedResults = evaluateClassifier(classifierResults, layer,trainTestSplit, data)
    load('Data/interestingness_scores.mat');

    labels = interestingness_score';

    predictedResults = struct;

    class1Indices = classifierResults.testIndx(classifierResults.Y_hat==1);
    trainIndx = classifierResults.trainIndx;

    trainX = data(trainIndx,:);
    
    testX = data(class1Indices,:);
    
    trainY = labels(trainIndx);
    testY = labels(class1Indices);
    
    % Lets train an SVRs
    if(strcmp(layer,'pool5'))
        disp('pool5')
        [eLCC5,eRHO5, ePred5, eRMSE5, psnrVal5, runTimeForAllImgs5, runTimePerImg5] = temp_linearSVR(trainX,trainY, testX, testY,'pool5');
        predictedResults.eLCC5 = eLCC5;
        predictedResults.eRHO5 = eRHO5;
        predictedResults.ePred5 = ePred5;
        predictedResults.rmse5  = eRMSE5;
        predictedResults.psnrVal5  = psnrVal5;
        predictedResults.runTimeForAllImgs5 = runTimeForAllImgs5;
        predictedResults.runTimePerImg5 = runTimePerImg5;
  
    elseif(strcmp(layer,'fc6'))
        disp('fc6')
        [eLCC6,eRHO6, ePred6, eRMSE6, psnrVal6, runTimeForAllImgs6, runTimePerImg6] = temp_linearSVR(trainX,trainY, testX, testY,'fc6');
        predictedResults.eLCC6 = eLCC6;
        predictedResults.eRHO6 = eRHO6;
        predictedResults.ePred6 = ePred6;
        predictedResults.rmse6  = eRMSE6;
        predictedResults.psnrVal6  = psnrVal6;
        predictedResults.runTimeForAllImgs6 = runTimeForAllImgs6;
        predictedResults.runTimePerImg6 = runTimePerImg6;
  
    elseif(strcmp(layer,'fc7'))
        disp('fc7')
        [eLCC7,eRHO7, ePred7, eRMSE7, psnrVal7, runTimeForAllImgs7, runTimePerImg7] = temp_linearSVR(trainX,trainY, testX, testY,'fc7');
        predictedResults.eLCC7 = eLCC7;
        predictedResults.eRHO7 = eRHO7;
        predictedResults.ePred7 = ePred7;
        predictedResults.rmse7  = eRMSE7;
        predictedResults.psnrVal7  = psnrVal7;
        predictedResults.runTimeForAllImgs7 = runTimeForAllImgs7;
        predictedResults.runTimePerImg7 = runTimePerImg7;
  
    end

    predictedResults.class1Indices = class1Indices;
    predictedResults.eGT = labels(class1Indices);
end