function goldTestLayerLabels = getOracleLayerLabels(trainTestSplit)

%trainTestSplit = 1;

load('Data/interestingness_scores.mat');

labels = interestingness_score';

load('Data/pool5Features_compressed.mat');

load('Data/fc6Features.mat');

load('Data/fc7Features.mat');

load('Data/TrainingNIter.mat');
load('Data/TestingNIter.mat');


trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

numImages = length(labels);

predictedClassLabels = zeros(length(testIndx),3);

absDiffAll = zeros(length(testIndx),3);

predictedInterestingnessScoresAll = zeros(numImages,3);

minVAll = zeros(1,numImages);
testLabels = labels(testIndx)';


[medLCC5, medRHO5, Y_hat5] = runSVM3(pool5Features_compressed(trainIndx,:),labels(trainIndx),pool5Features_compressed(testIndx,:),labels(testIndx),'pool5');

[lcc6, rho6, Y_hat6] = runSVM3(fc6Features(trainIndx,:),labels(trainIndx),fc6Features(testIndx,:),labels(testIndx),'fc6');

[lcc7, rho7, Y_hat7] = runSVM3(fc7Features(trainIndx,:),labels(trainIndx),fc7Features(testIndx,:),labels(testIndx),'fc7');

absDiff = [ abs(Y_hat5-testLabels')  abs(Y_hat6-testLabels') abs(Y_hat7-testLabels')];

% 1: stands for fc5, 2 for fc6, 3 for fc7
[minV, minInd] = min(absDiff,[],2);
predictedClassLabels = minInd;
absDiffAll= absDiff;

kFold_567.derivedGTLabels = predictedClassLabels;
kFold_567.absDiffAll = absDiffAll;
kFold_567.minVAll = minVAll;

goldTestLayerLabels = kFold_strategy2(kFold_567);


save(strcat('Results/goldLayerLabels_int_',num2str(trainTestSplit),'.mat'),'goldTestLayerLabels');
%end