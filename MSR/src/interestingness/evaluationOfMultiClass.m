load('Results/multiClass.mat');
load('Data/pool5Features.mat');
load('Data/fc6Features.mat');
load('Data/fc7Features.mat');
    
load('Data/interestingness_scores.mat');
    
load('Results/multiClass.mat');

trainIndx = predictedResults.trainIndxs;

testIndx = predictedResults.testIndxs;

labels = interestingness_score;


%% On the test data with pool5 labels, use fc5 and fc6 Features.
% 
% [eLCC56,eRHO56, ePred56, rmse56] = temp_linearSVR(fc6Features(trainIndx,:),labels(trainIndx)',fc6Features(predictedResults.class1Indices,:),labels(predictedResults.class1Indices)','fc6');
% 
% [eLCC57,eRHO57, ePred57, rmse57] = temp_linearSVR(fc7Features(trainIndx,:),labels(trainIndx)',fc7Features(predictedResults.class1Indices,:),labels(predictedResults.class1Indices)','fc7');


% [eLCC65,eRHO65, ePred65, rmse65] = temp_linearSVR(pool5Features(trainIndx,:),labels(trainIndx)',pool5Features(predictedResults.class2Indices,:),labels(predictedResults.class2Indices)','fc5');
% 
% [eLCC67,eRHO67, ePred67, rmse67] = temp_linearSVR(fc7Features(trainIndx,:),labels(trainIndx)',fc7Features(predictedResults.class2Indices,:),labels(predictedResults.class2Indices)','fc7');

[eLCC75,eRHO75, ePred75, rmse75] = temp_linearSVR(pool5Features(trainIndx,:),labels(trainIndx)',pool5Features(predictedResults.class3Indices,:),labels(predictedResults.class3Indices)','fc5');

[eLCC76,eRHO76, ePred76, rmse76] = temp_linearSVR(fc7Features(trainIndx,:),labels(trainIndx)',fc7Features(predictedResults.class3Indices,:),labels(predictedResults.class3Indices)','fc7');
