%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM regressor for NIter numbere of times.
function [accuracy, Y_hat] = multiClassSVC(trainX, trainY, testX, testY)
%function [accur] = multiClassSVC(trainX, trainY, testX, testY)
    addpath('..');
    C = []; 
    gamma = [];
    
%     for i = 1:3
%         C = [C 2^i];
%     end
%     
%     for i = -10:-7
%         gamma = [gamma 2^i];
%     end

    C = 1; gamma = 2^-8;
    
    accur = zeros(length(C), length(gamma));
    
    fid = fopen('train_ind11.txt','w');

    for itr_im = 1:size(trainX,1)
        fprintf(fid,'%d ',trainY(itr_im,1));
        for itr_feat =1:size(trainX,2)
            fprintf(fid,'%d:%f ',itr_feat,trainX(itr_im,itr_feat));
        end
        fprintf(fid,'\n');
    end

    fclose(fid);

    % Execute it.
    system('./svm-scale2 -l -1 -u 1 -s range11 train_ind11.txt > train_scale11');

    % Construct the file ( input ) suitably for testing the SVM
    fid = fopen('test_ind11.txt','w');
    for itr_im = 1:size(testX,1)
        fprintf(fid,'%d ',testY(itr_im,1));
        for itr_param = 1:size(testX,2)
            fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);

    % Scale the data.
    system('./svm-scale2  -r range11 test_ind11.txt > test_ind11_scaled');

    %% t: Kernel type. %s: svm type. g: gamma c: cost.
    %  system('svm-train  -s 0 -t 2 -b 1 -q train_scale11 model11');
    for cItr = 1:length(C)
        for gItr = 1:length(gamma)
            cmd = strcat('./svm-train2  -t 2 -s 1 -b 1 -g',{' '},num2str(gamma(gItr)),' -c',{' '},num2str(C(cItr)),' -q train_scale11 model11');
            cmd{1}
            system(cmd{1});
            tStart = tic; 
            system('./svm-predict2  -b 0 test_ind11_scaled model11 output11.txt > dump11');
            runTimePerImg = toc(tStart);
            runTimePerImg = runTimePerImg/length(testY)
            disp(strcat('Prediction Time for',num2str(length(testY)), 'images = ', num2str(runTimePerImg)))
            load output11.txt;
            % Predicted scores
            Y_hat=output11;
           accuracy = sum(Y_hat == testY)/length(testY)
           accur(cItr,gItr) = accuracy;
        end
    end
end