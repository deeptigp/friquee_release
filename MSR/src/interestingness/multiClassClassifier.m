%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7

function multiClassClassifier()


    load('Data/pool5Features.mat');
    load('Data/fc6Features.mat');
    load('Data/fc7Features.mat');
    
    load('Data/interestingness_scores.mat');
    
    load('Results/kFold_567.mat');
    
    classifierResults = struct;
    
    predictedClassLabels = kFold_567.derivedGTLabels;
    
     labels = interestingness_score;
    
    % Generate a random train/test split.
    numImages = size(pool5Features,1);

    trainSplit = round(0.8*numImages);

    indx = randperm(numImages);

    trainIndx = indx(1:trainSplit);
    testIndx = indx(1+trainSplit:end);

    [accuracy, Y_hat] = multiClassSVC(pool5Features(trainIndx,:),predictedClassLabels(trainIndx)',pool5Features(testIndx,:),predictedClassLabels(testIndx)');

    classifierResults.accuracy = accuracy;
    classifierResults.Y_hat = Y_hat;
    classifierResults.Y_hat = Y_hat;
    
    save('Results/multiClass.mat','kFold_567','predictedResults');
end