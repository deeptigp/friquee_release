DATA_PATH = '/media/deepti/I/data/interestingness/';
load(strcat(DATA_PATH,'imNet/Data/groundTruthLabels_10Class.mat'));

labels = groundTruthLabels;

load(strcat(DATA_PATH,'imNet/Results/kFold_567_10Class.mat'));

load(strcat(DATA_PATH,'imNet/Data/pool5Features_10Class.mat'));

load(strcat(DATA_PATH,'imNet/Data/fc6Features_10Class.mat'));

load(strcat(DATA_PATH,'imNet/Data/fc7Features_10Class.mat'));

load(strcat(DATA_PATH,'imNet/Data/TrainingNIter_10Class.mat'));
load(strcat(DATA_PATH,'imNet/Data/TestingNIter_10Class.mat'));

trainTestSplit = 1;

testIndx = TestingNIter(:,trainTestSplit);

testY = groundTruthLabels(testIndx);

[medACC7, testY7, Y_hat, runTimeForAllImgs7] = testSVM_fast(fc7Features(testIndx,:), testY, 'fc7');

[medACC6, testY6, Y_hat, runTimeForAllImgs6] = testSVM_fast(fc6Features(testIndx,:), testY, 'fc6');

[medACC5, testY5, Y_hat, runTimeForAllImgs5] = testSVM_fast(pool5Features(testIndx,:), testY, 'pool5');
