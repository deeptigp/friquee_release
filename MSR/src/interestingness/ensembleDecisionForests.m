function [accuracy, predictedLabels, runTimeForAllImgs, scores, stdevs, BaggedEnsemble] = ensembleDecisionForests(trainX, trainY, testX, testY)
    rng(1945,'twister');
    numTrees = 30;

    BaggedEnsemble = TreeBagger(numTrees,trainX,trainY,'OOBPrediction','on');%,'InBagFraction',0.3,'SampleWithReplacement','on')
    
    BaggedEnsemble.DefaultYfit = '';
    
    oobErrorBaggedEnsemble = oobError(BaggedEnsemble);
    plot(oobErrorBaggedEnsemble)
    xlabel 'Number of grown trees';
    ylabel 'Out-of-bag classification error';

    
    tStart = tic; 
    
    [Y_hat,scores,stdevs] = predict(BaggedEnsemble,testX);
    
    runTimeForAllImgs = toc(tStart);
    
    predictedLabels = zeros(length(testY),1);
    
    for i = 1:length(testY)
        predictedLabels(i) = str2num(Y_hat{i,1});
    end
    
    accuracy = sum(predictedLabels == testY)/length(testY);
    
    sum(predictedLabels)
    %% Calculate the margins..
%     figure
%     plot(oobMeanMargin(BaggedEnsemble));
%     xlabel('Number of Grown Trees');
%     ylabel('Out-of-Bag Mean Classification Margin');
%     
%     
%     %% Plot the ROC curve.
%     [Yfit,Sfit] = oobPredict(BaggedEnsemble);
%     [fpr,tpr] = perfcurve(BaggedEnsemble.Y,Sfit(:,2),'1');
%     figure
%     plot(fpr,tpr);
%     xlabel('False Positive Rate');
%     ylabel('True Positive Rate');
%     
%     
%     
%     [fpr,accu,thre] = perfcurve(BaggedEnsemble.Y,Sfit(:,2),'1','YCrit','Accu');
%     figure(5);
%     plot(thre,accu);
%     xlabel('Threshold for ''good'' Returns');
%     ylabel('Classification Accuracy');
end