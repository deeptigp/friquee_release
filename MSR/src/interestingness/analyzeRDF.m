% This script tries to find the best parameters for RDF

%numLeaves = 50, for fc6;
%numLeaves = 50, for fc7;
%numLeaves = 50, for pool5;

expt = 'numTrees';
load('Data/fc7Features.mat');
load('Results/kFold_567.mat');


load('Data/TrainingNIter.mat');
load('Data/TestingNIter.mat');

trainIndx = TrainingNIter(:,2);
testIndx = TestingNIter(:,2);
numTrees = 50;


%% Features and labels.
X = fc7Features;
%X = compressData(X, 100);

Y = (kFold_567.derivedGTLabels ==1)';

trainX = X(trainIndx, :);
trainY = Y(trainIndx, :);

testX = X(testIndx, :);
testY = Y(testIndx, :);

if(strcmp(expt,'numLeaves'))
    leaf = [5 10 20 50];
    
    col = 'rbcmy';

    figure
    for i=1:length(leaf)
        b = TreeBagger(numTrees,X,Y,'OOBPred','On','MinLeaf',leaf(i));
        plot(oobError(b),col(i));
        hold on;
    end
    xlabel 'Number of Grown Trees';
    ylabel 'Mean Squared Error' ;
    legend({'5' '10' '20' '50'},'Location','NorthEast');
    hold off;
end



if(strcmp(expt,'numTrees'))
    numTrees = [1 5 10 ];%20 30 40 50 100 200 300];
    
    col = 'rbcmyk';
    accuracy = [];
    label1 = [];
    figure
    for i=1:length(numTrees)
        b = TreeBagger(numTrees(i),trainX,trainY,'OOBPred','On');
        %plot(oobError(b),col(i));
        
         [Y_hat,scores,stdevs] = predict(b,testX);
    
        predictedLabels = zeros(length(testY),1);

        for j = 1:length(testY)
            predictedLabels(j) = str2num(Y_hat{j,1});
        end

        accur = sum(predictedLabels == testY)/length(testY);
    
        accuracy =[accuracy accur];
        label1 = [label1 sum(predictedLabels)];
        %hold on;
    end
    figure(1);
    plot(numTrees,accuracy,'ro-');
    xlabel 'Number of Grown Trees';
    ylabel 'Accuracy' ;
    figure(2);
    plot(numTrees,label1,'b+-');
    xlabel 'Number of Grown Trees';
    ylabel 'Number of labels belonging to class 1' ;
    %legend({'5' '10' '20' '30' '30' '50'},'Location','NorthEast');
    %hold off;
end

if(strcmp(expt,'featImp'))
    b = TreeBagger(numTrees,X,Y,'OOBVarImp','On','MinLeaf',5);
    figure
    plot(oobError(b));
    xlabel 'Number of Grown Trees';
    ylabel 'Out-of-Bag Mean Squared Error';
end


