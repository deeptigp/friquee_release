%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM regressor for NIter numbere of times.
function [medLCC, medRHO, Y_hat, rmse, psnrVal, runTimeForAllImgs, runTimePerImg] = runSVM3(trainX, trainY, testX, testY, layer)
 C = 0.25;
     gamma = 2^-9;
     
 
     if(strcmp(layer,'fc4') || strcmp(layer,'fc3'))
         gamma = 2^-12;
     end

    addpath('..');
    fid = fopen('train_ind3.txt','w');

    for itr_im = 1:size(trainX,1)
        fprintf(fid,'%d ',trainY(itr_im,1));
        for itr_feat =1:size(trainX,2)
            fprintf(fid,'%d:%f ',itr_feat,trainX(itr_im,itr_feat));
        end
        fprintf(fid,'\n');
    end

    fclose(fid);

    % Execute it.
    system('./svm-scale3 -l -1 -u 1 -s range3 train_ind3.txt > train_scale3');

    % Construct the file ( input ) suitably for testing the SVM
    fid = fopen('test_ind3.txt','w');
    for itr_im = 1:size(testX,1)
        fprintf(fid,'%d ',testY(itr_im,1));
        for itr_param = 1:size(testX,2)
            fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
    
    % Scale the data.    
    system('./svm-scale3  -r range3 test_ind3.txt > test_ind3_scaled');

 
    medLCC = zeros(length(C),length(gamma));
    medRHO = zeros(length(C),length(gamma));

    %% t: Kernel type. %s: svm type. g: gamma c: cost.
    %  system('svm-train3  -s 0 -t 2 -b 1 -q train_scale3 model3');
    
    for cItr = 1:length(C)
        for gItr = 1:length(gamma)
            cmd = strcat('./svm-train3  -t 2 -s 4 -b 1 -g',{' '},num2str(gamma(gItr)),' -c',{' '},num2str(C(cItr)),' -q train_scale3 model3');
            cmd
            system(cmd{1});
            
            tStart = tic;
            %system('svm-predict3  -b 1 test_ind3_scaled model3 output3.txt > dump3');
            system('./svm-predict3  -b 1 test_ind3_scaled model3 output3.txt > dump3');

            runTimeForAllImgs = toc(tStart)

            runTimePerImg = runTimeForAllImgs/length(testY)
            
            load output3.txt;
            % Predicted scores
            Y_hat=output3;

            rho = corr(Y_hat,testY,'type','Spearman');
            lcc = corr(Y_hat,testY,'type','Pearson');

            [lcc rho]
            medLCC(cItr, gItr) = lcc;
            medRHO(cItr, gItr) = rho;
            
            system('rm output3.txt dump3 model3');
        end
    end
    rmse  = sqrt(mean((testY - Y_hat).^2));
    psnrVal = psnr(testY,Y_hat);
   
    system('rm train_scale3 range3 test_ind3.txt train_ind3.txt test_ind3_scaled');
end