load('D:\ImageData\caffeFeatures\featuresImNetTest.mat');

%load('Data\mementoData.mat');

numImNetTest = size(featuresImNetTest,1);
batchSize = 500;
numBatches = numImNetTest/batchSize;
imTestPredict = [];

for i = 1:numBatches
    [(i-1)*batchSize+1 i*batchSize]
    p = predictTeacherIntScores(featuresImNetTest((i-1)*batchSize+1:i*batchSize,:),[1:batchSize]');
    imTestPredict  = [imTestPredict  ;p];
    
    if(rem(i,20)==0)
        save(strcat('imTestPredict_',num2str(i/20),'.mat'),'imTestPredict');
    end
end