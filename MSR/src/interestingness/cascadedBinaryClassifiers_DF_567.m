%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7

function aggregateResults_thresh_RDF = cascadedBinaryClassifiers_DF_567()
    load('Data/interestingness_scores.mat');

    labels = interestingness_score;

    load('Results/kFold_567_strat2_thresh0.07.mat');

    load('Data/pool5Features.mat');

    load('Data/fc6Features.mat');

    load('Data/fc7Features.mat');

    load('Data/TrainingNIter.mat');
    load('Data/TestingNIter.mat');

    trainIndx = TrainingNIter(:,1);
    testIndx = TestingNIter(:,1);

    %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);

    cascadeClassifierResults_aggre567_thresh_RDF_pool5 = struct;
    cascadeClassifierResults_aggre567_thresh_RDF_fc6 = struct;
    cascadeClassifierResults_aggre567_thresh_RDF_fc7 = struct;

    % Generate a random train/test split.
    numImages = length(labels);

    % Get the derived class labels.
    GTLabels = kFold_567.strategy2DerivedLabels;
    %GTLabels = kFold_567.derivedGTLabels;

    %% Get the label indexes.
    class1LabelIndxs = find(GTLabels== 1);
    class2LabelIndxs = find(GTLabels== 2);
    class3LabelIndxs = find(GTLabels== 3);

    binaryLabels1 = zeros(1,numImages);
    binaryLabels2 = zeros(1,numImages);
    binaryLabels3 = zeros(1,numImages);

    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
    binaryLabels2(class2LabelIndxs) = 1; % fc6 labels
    binaryLabels3(class3LabelIndxs) = 1; % fc7 labels


    %% train with fc5, those that say no-fc5, test them with fc6.
    % Normalize the features before passing them through RDFs

    normPool5Features = featureNormalize(pool5Features);
    normFC6Features = featureNormalize(fc6Features);


    trainNX = normPool5Features(trainIndx,:);
    testNX = normPool5Features(testIndx,:);

    [accuracy, Y_hat, runTimeForAllImgs, scores, stdevs,BaggedEnsemble] = ensembleDecisionForests(trainNX,binaryLabels1(trainIndx)',testNX,binaryLabels1(testIndx)');

    cascadeClassifierResults_aggre567_thresh_RDF_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_thresh_RDF_pool5.testIndx = testIndx;
    cascadeClassifierResults_aggre567_thresh_RDF_pool5.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_thresh_RDF_pool5.accuracy = accuracy;
    cascadeClassifierResults_aggre567_thresh_RDF_pool5.runTimeForAllImgs = runTimeForAllImgs;


    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre567_thresh_RDF_pool5.mat'),'cascadeClassifierResults_aggre567_thresh_RDF_pool5');

    cascadePredictedResults_aggre567_thresh_RDF_pool5 = evaluateClassifier_copy2(cascadeClassifierResults_aggre567_thresh_RDF_pool5, 'pool5');

    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre567_thresh_RDF_pool5.mat'),'cascadePredictedResults_aggre567_thresh_RDF_pool5');

    %% Aggregating the predictions of the data points that were predicted to belong to pool5 class..
    aggregatePredictedScores(cascadePredictedResults_aggre567_thresh_RDF_pool5.class1Indices) = cascadePredictedResults_aggre567_thresh_RDF_pool5.ePred5;


    %% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_thresh_RDF_pool5.testIndx(cascadeClassifierResults_aggre567_thresh_RDF_pool5.Y_hat==0);

    fprintf('Number of not fc5 images: %d',length(notFC5Indxs));

    trainNX = normFC6Features(trainIndx,:);
    testNX = normFC6Features(notFC5Indxs,:);

    [accuracy, Y_hat, runTimeForAllImgs, scores, stdevs,BaggedEnsemble] = ensembleDecisionForests(trainNX, binaryLabels2(trainIndx)', testNX, binaryLabels2(notFC5Indxs)');

    cascadeClassifierResults_aggre567_thresh_RDF_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_thresh_RDF_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_thresh_RDF_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_thresh_RDF_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre567_thresh_RDF_fc6.runTimeForAllImgs = runTimeForAllImgs;


    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre567_thresh_RDF_fc6.mat'),'cascadeClassifierResults_aggre567_thresh_RDF_fc6');

    cascadePredictedResults_aggre567_thresh_RDF_fc6 = evaluateClassifier_copy2(cascadeClassifierResults_aggre567_thresh_RDF_fc6, 'fc6');

    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre567_thresh_RDF_fc6.mat'),'cascadePredictedResults_aggre567_thresh_RDF_fc6');

    aggregatePredictedScores(cascadePredictedResults_aggre567_thresh_RDF_fc6.class1Indices) = cascadePredictedResults_aggre567_thresh_RDF_fc6.ePred6;

    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre567_thresh_RDF_fc6.testIndx(cascadeClassifierResults_aggre567_thresh_RDF_fc6.Y_hat==0);

    fprintf('Number of not fc6 images: %d',length(notFC6Indxs));

    Y_hat = ones(size(notFC6Indxs));

    cascadeClassifierResults_aggre567_thresh_RDF_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_thresh_RDF_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre567_thresh_RDF_fc7.Y_hat = Y_hat;

    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre567_thresh_RDF_fc7.mat'),'cascadeClassifierResults_aggre567_thresh_RDF_fc7');

    cascadePredictedResults_aggre567_thresh_RDF_fc7 = evaluateClassifier_copy2(cascadeClassifierResults_aggre567_thresh_RDF_fc7, 'fc7');

    aggregatePredictedScores(cascadePredictedResults_aggre567_thresh_RDF_fc7.class1Indices) = cascadePredictedResults_aggre567_thresh_RDF_fc7.ePred7;
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre567_thresh_RDF_fc7.mat'),'cascadePredictedResults_aggre567_thresh_RDF_fc7');


    nonZeroPredictedScores = aggregatePredictedScores(testIndx);
    aggreSROCC = corr(nonZeroPredictedScores,labels(testIndx)','type','Spearman');
    aggrePLCC = corr(nonZeroPredictedScores,labels(testIndx)');

    aggregateResults_thresh_RDF.predictedScores = aggregatePredictedScores;
    aggregateResults_thresh_RDF.aggreSROCC = aggreSROCC;
    aggregateResults_thresh_RDF.aggrePLCC = aggrePLCC;

    save(strcat('Results/aggregateResults/aggregatePredictedScores_thresh_RDF_fc567.mat'),'aggregateResults_thresh_RDF');

    fprintf('Spearman and Pearson correlation between the aggregated predictions and GT predictions %f %f\n',aggreSROCC,aggrePLCC);
