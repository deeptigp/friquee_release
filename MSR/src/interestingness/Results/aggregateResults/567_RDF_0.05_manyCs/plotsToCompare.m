SAVEPATH = '';
C = [];
for i = -5:1:5
    C = [C 2^i];
end
C = C';
fig1 = figure(1);
plot(C,cascadePredictedResults_aggre567_thresh_RDF_pool5.eRHO5,'ro-');
xlabel('Values of the C parameter of SVR','FontSize', 24);
ylabel('Spearmann Correlation','FontSize', 24);
grid on
print(fig1, strcat(SAVEPATH,'CostVsSROCC.jpg'),'-djpeg');

fig2 = figure(2);
plot(C,cascadePredictedResults_aggre567_thresh_RDF_pool5.runTimeForAllImgs5,'bo-');
xlabel('Values of the C parameter of SVR','FontSize', 24);
ylabel('Running time for 444 images','FontSize', 24);
grid on
print(fig2, strcat(SAVEPATH,'RunningTimeVsSROCC.jpg'),'-djpeg');

fig3 = figure(3);
x = cascadePredictedResults_aggre567_thresh_RDF_pool5.runTimeForAllImgs5;
y = cascadePredictedResults_aggre567_thresh_RDF_pool5.eRHO5;

plot(x,y,'mo-');
xlabel('Running time for 444 images','FontSize', 14);
ylabel('Spearmann Correlation','FontSize', 14);
b = num2str(C); c = cellstr(b);
%axis([5.3 9.8 0.69 0.72])
dx = -0.1;
dy = -0.0007; % displacement so the text does not overlay the data points
text(x+dx, y+dy, c,'FontSize', 12);
title('The change in SVR''s test time and correlation for different values of the cost parameter of SVR','FontSize', 14);
grid on
print(fig3, strcat(SAVEPATH,'timeVsSROCC.jpg'),'-djpeg');