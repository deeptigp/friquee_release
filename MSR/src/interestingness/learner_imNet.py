#! /usr/bin/env python

import os
import subprocess

import scipy.io as sio
import numpy as np
import time
import scipy.stats as sstats
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
import sklearn.grid_search as grid_search
import matplotlib.pyplot as plt
from pandas import DataFrame

def readMatFile(fName, key):
    """ This method reads a mat file and converts to an ndarray and returns the array
    fName: Name of the mat file
    key: Name of the structure that is stored in the matfile.
    """

    mat_contents = sio.loadmat(fName)
    retArray = mat_contents[key]
    return retArray

def generateTrainTestData(features, labels, trainInd, testInd, splitNum):

    # the -1 can be removed if we use python to generate the train/test split files.

    trainInd = trainInd[:,splitNum-1]
    testInd = testInd[:,splitNum-1]

    # This can be removed once we remove the dependency on the mat files.. MATLAB is 1 indexesd while python is 0-indexed
    trainInd = trainInd - 1
    testInd = testInd - 1

    # Separate out train and test features
    trainX = features[trainInd,:]
    testX = features[testInd,:]

    # First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
    trainY = labels[:,trainInd]
    trainY = trainY.transpose()
    trainY = np.reshape(trainY,(len(trainY)))

    # First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
    testY = labels[:,testInd]
    testY = testY.transpose()
    testY = np.reshape(testY,(len(testY)))

    return (trainX, trainY, testX, testY)

def trainSVR(trainX, trainY, testX, testY, cVal, param):
    ''' Initialize and fit an SVR'''
    reg = SVR(C= cVal, gamma  = param)

    start_time = time.time()

    reg.fit(trainX, trainY)

    print("%s seconds to fit SVR" %(time.time() - start_time))

    # Predict it on the test data
    predY = reg.predict(testX)

    # Evaluate on the test data
    corrV = sstats.spearmanr(predY,testY)

    #Fetch SVR parameters.
    svrParams = reg.get_params()

    return (corrV, predY)

def gridSearchSVR(trainX, trainY, testX, testY):
    ''' Initialize and fit an SVR'''
    cVal = [1, 2]

    gVal = np.logspace(-7,-4,4).tolist()

    parameters = {"C":cVal, "gamma":gVal}

    svr = SVR()

    reg = grid_search.GridSearchCV(svr, parameters)

    start_time = time.time()

    reg.fit(trainX, trainY)

    print("%s seconds to fit SVR" %(time.time() - start_time))

    # Predict it on the test data
    predY = reg.predict(testX)

    # Evaluate on the test data
    corrV = sstats.spearmanr(predY,testY)

    #Fetch SVR parameters.
    svrParams = reg.best_estimator_

    print svrParams

    return (corrV,testY, predY)


def trainLearners(features, labels, classNum):
    #print features.shape, labels.shape

    trainInd = readMatFile('Data/imNetTrainTest_'+`classNum`+'.mat','trainIndx')

    testInd = readMatFile('Data/imNetTrainTest_'+`classNum`+'.mat','testIndx')

    trainInd = trainInd.transpose()

    testInd = testInd.transpose()

    trainX, trainY, testX, testY = generateTrainTestData(features,labels,trainInd,testInd,1)

    print trainX.shape, trainY.shape, testX.shape, testY.shape

    corrV, testY, predY = gridSearchSVR(trainX, trainY, testX, testY)

    print corrV

    return (corrV, testY, predY)

def getGoldLabels(gtFC7,labels, imNetfc7):
    testY = np.arange(len(imNetfc7))
    labels = labels.reshape(-1)

    # The params here are C = 2, gamma = 10^-5
    corrV, testY, predY = gridSearchSVR(gtFC7, labels, imNetfc7, testY)

    return predY

if __name__ == '__main__':

    classNum = 9
    print("\n-- get data:")
    caffeFeatDir = '/media/deepti/I/data/ImageData/caffeFeatures/'
    data = np.load(caffeFeatDir+'caffeFeats_class'+`classNum`+'_4-7.npz')
    imNetconv5 = data['conv5']
    imNetfc6 = data['fc6']
    imNetfc7 = data['fc7']

    data2 = np.load(caffeFeatDir+'caffeFeats_mem_4-7.npz')
    gtFC7 = data2['fc7']

    del data, data2

    labels = readMatFile('Data/interestingness_scores.mat','interestingness_score')
    labels = labels.transpose()

    goldFC7 = getGoldLabels(gtFC7,labels, imNetfc7)

    sio.savemat('goldFC7_class'+`classNum`+'.mat',{'goldFC7':goldFC7})

    goldFC7 = readMatFile('goldFC7_class'+`classNum`+'.mat','goldFC7')

    corrVFC6, testYFC6, predYFC6 = trainLearners(imNetfc6,goldFC7, classNum)

    corrVFC5, testYFC5, predYFC5 = trainLearners(imNetconv5, goldFC7, classNum)

    absDiffFC5 = np.reshape(abs(testYFC5 - predYFC5),(len(predYFC5),1))

    absDiffFC6 = np.reshape(abs(testYFC6 - predYFC6),(len(predYFC6),1))


    concatDiff = np.concatenate((absDiffFC5, absDiffFC6),axis=1)

    betterFC  = np.argmin(concatDiff, axis = 1)

    print 'Num of images that prefer fc5, 6 respectively are'
    print (betterFC ==0).sum(), (betterFC ==1).sum()


    df = DataFrame({'gTruth':testYFC6, 'predYFC6':predYFC6, 'predYFC5':predYFC5, 'absDiffFC5':absDiffFC5.reshape(-1), 'absDiffFC6':absDiffFC6.reshape(-1),'betterFC':betterFC.reshape(-1)})

    df.to_excel('Results/class'+`classNum`+'_fc567.xlsx', sheet_name='sheet1', index=['gTruth','predYFC6', 'predYFC5', 'absDiffFC5', 'absDiffFC6', 'betterFC'])

    plt.figure(1)
    plt.scatter(testYFC6,predYFC6,c='b')
    plt.scatter(testYFC5,predYFC5,c='r')

    plt.xlabel('Ground truth')

    plt.ylabel('Predicted scores')

    plt.show()