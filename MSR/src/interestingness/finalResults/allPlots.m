%%%% ---- 1  AlexNet running times %%
close all;
%load('alexNet_perLayerRunTimes.mat');

alexNet_perLayerRunTimes = [
    0.0208
    0.0497
    0.0674
    0.0801
    0.0906
    0.1293
    0.1491];

x = [1: length(alexNet_perLayerRunTimes)];
layerName = {'norm1','norm2','conv3','conv4','pool5','fc6','fc7'};
hFig = figure(1);
set(hFig, 'Position', [0 0 800 600]);
set(gcf,'Units','centimeters');
set(gcf,'PaperPositionMode','auto');

plot(x,alexNet_perLayerRunTimes,'rd-','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','r');

ylabel('Feature computation time (in seconds)');


%% fixing the x-axis label.
axis([0.005,length(layerName), 0,0.16]);
set(gca,'FontSize',18);
set(gca, 'XTick',[1:length(layerName)], 'XTickLabel',layerName);
h = gca;
a=get(h,'XTickLabel');
set(h,'XTickLabel',[]);
b=get(h,'XTick');
rot = 40;
c=get(h,'YTick');
th=text(b,repmat(c(1)-0.1*(c(2)-c(1)),length(b),1),a,'HorizontalAlignment','right','rotation',rot,'FontSize',18);

grid;
box on;

%print(1, '-dpdf','featComputationTime.pdf');
% 
%system('pdfcrop -margins 10 featComputationTime.pdf featComputationTime.pdf');


