#! /usr/bin/env python
''' This script is to mainly try different combinations of fc1-fc7 features of memorability dataset. I've also tried different learners - i.e., SVR and RDF
'''
import os
import subprocess

import scipy.io as sio
import numpy as np
import time
import scipy.stats as sstats
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
import sklearn.grid_search as grid_search
from sklearn.decomposition import PCA

def readMatFile(fName, key):
	""" This method reads a mat file and converts to an ndarray and returns the array
	fName: Name of the mat file
	key: Name of the structure that is stored in the matfile.
	"""
	mat_contents = sio.loadmat(fName)
	retArray = mat_contents[key]
	return retArray

"""  Given
Look out for a few transposes here and there """
def generateTrainTestData(features, labels, trainInd, testInd, splitNum):

	# the -1 can be removed if we use python to generate the train/test split files.

	trainInd = trainInd[:,splitNum-1]
	testInd = testInd[:,splitNum-1]

	# This can be removed once we remove the dependency on the mat files.. MATLAB is 1 indexesd while python is 0-indexed
	trainInd = trainInd - 1
	testInd = testInd - 1

	# Separate out train and test features
	trainX = features[trainInd,:]
	testX = features[testInd,:]

	# First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
	trainY = labels[:,trainInd]
	trainY = trainY.transpose()
	trainY = np.reshape(trainY,(len(trainY)))

	# First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
	testY = labels[:,testInd]
	testY = testY.transpose()
	testY = np.reshape(testY,(len(testY)))

	return (trainX, trainY, testX, testY)


def generateTrainTestData1(features, labels, trainInd, testInd, splitNum):
	trainInd = trainInd[:,splitNum-1]
	testInd = testInd[:,splitNum-1]
	# This can be removed once we remove the dependency on the mat files.. MATLAB is 1 indexesd while python is 0-indexed
	trainInd = trainInd - 1
	testInd = testInd - 1
	# Separate out train and test features
	trainX = features[trainInd,:]
	testX = features[testInd,:]
	# First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
	trainY = labels[trainInd,:]
	testY = labels[testInd,:]
	return (trainX, trainY, testX, testY)

def trainSVR1(trainX, trainY, testX, testY):
	''' Initialize and fit an SVR'''
	reg = SVR(C=1,gamma=0.0000001)

	start_time = time.time()

	reg.fit(trainX, trainY)

	print("%s seconds to fit SVR" %(time.time() - start_time))

	# Predict it on the test data
	predY = reg.predict(testX)

	# Evaluate on the test data
	corrV = sstats.spearmanr(predY,testY)

	#Fetch SVR parameters.
	svrParams = reg.get_params()

	return (corrV,svrParams)

def trainSVR(trainX, trainY, testX, testY):
	''' Initialize and fit an SVR'''
	cVal = [1, 2]

	gVal = np.logspace(-7,-4,4).tolist()

	parameters = {"C":cVal, "gamma":gVal}

	svr = SVR()

	reg = grid_search.GridSearchCV(svr, parameters)

	start_time = time.time()

	reg.fit(trainX, trainY)

	print("%s seconds to fit SVR" %(time.time() - start_time))

	# Predict it on the test data
	predY = reg.predict(testX)

	# Evaluate on the test data
	corrV = sstats.spearmanr(predY,testY)

	#Fetch SVR parameters.
	svrParams = reg.best_estimator_

	return (corrV,svrParams)

def trainDecisionForests(trainX, trainY, testX, testY,nEst):
	dtReg = RandomForestRegressor(n_estimators=nEst)

	start_time = time.time()
	dtReg.fit(trainX, trainY)
	print("%s seconds to fit RDF " %(time.time() - start_time))


	predY=  dtReg.predict(testX)

	# Evaluate on the test data
	corrV = sstats.spearmanr(predY,testY)

	dtParams = dtReg.get_params(deep=True)

	return (corrV, dtParams)

def pcaFit(features, n_comp):
	start_time = time.time()

	pca = PCA(n_components = n_comp)

	pcaFeat = pca.fit_transform(features)

	return pcaFeat


def trainLearners(features, labels):
	#print features.shape, labels.shape

	trainInd = readMatFile('Data/TrainingIter.mat','TrainingIter')

	testInd = readMatFile('Data/TestingIter.mat','TestingIter')

	trainX, trainY, testX, testY = generateTrainTestData(features,labels,trainInd,testInd,1)

	print trainX.shape, trainY.shape, testX.shape, testY.shape

	print 'Training an SVR'
	corrV, svrParams = trainSVR(trainX, trainY, testX, testY)

	print corrV, svrParams

if __name__ == '__main__':

	print("\n-- get data:")
	caffeFeatDir = '/media/deepti/I/data/ImageData/caffeFeatures/'

	data = np.load(caffeFeatDir+'caffeFeats_mem_4-7.npz')
	# fc6 = data['fc6']
	# fc7 = data['fc7']
	# conv5 = data['conv5']

	pool5 = data['pool5']
	conv4 = data['conv4']

	data2 = np.load(caffeFeatDir+'caffeFeats_mem_1-3.npz')

	conv3 = data2['conv3']
	conv2 = data2['conv2']
	conv1 = data2['conv1']

#    features = np.concatenate((conv3, conv4),axis=1) ## pool5, fc6

	labels = readMatFile('Data/interestingness_scores.mat','interestingness_score')
	del data, data2
#    trainLearners(features,labels)

	## fc3-fc5
	# features = np.concatenate((conv3,conv4,conv5),axis=1)
	# #features.shape
	# trainLearners(features,labels)

	# # fc2-fc3
	# features = np.concatenate((conv2, conv3), axis = 1)
	# trainLearners(features, labels)

	# #fc2-fc4
	# features = np.concatenate((conv2, conv3, conv4), axis = 1)
	# trainLearners(features, labels)
	#fc1-fc2
	# features = np.concatenate((conv1, conv2), axis =1)
	# trainLearners(features, labels)

	# #fc1-fc3
	# features = np.concatenate((conv1, conv2, conv3), axis = 1)
	# trainLearners(features, labels)

	#fc1-fc4
	features = np.concatenate((conv1, conv2, conv3, conv4), axis = 1)

	pcaFeat = pcaFit(features, 40)
	print("%s seconds to fit SVR" %(time.time() - start_time))
	trainLearners(pcaFeat, labels)
