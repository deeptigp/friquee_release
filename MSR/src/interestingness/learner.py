#! /usr/bin/env python

import os
import subprocess
import sys
import scipy.io as sio
import numpy as np
import time
import scipy.stats as sstats
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
import sklearn.grid_search as grid_search
import matplotlib.pyplot as plt
from pandas import DataFrame
from sklearn import preprocessing
from sklearn.cluster import KMeans

def readMatFile(fName, key):
	""" This method reads a mat file and converts to an ndarray and returns the array
	fName: Name of the mat file
	key: Name of the structure that is stored in the matfile.
	"""

	mat_contents = sio.loadmat(fName)
	retArray = mat_contents[key]
	return retArray

def generateTrainTestData(features, labels, trainInd, testInd, splitNum):
	# the -1 can be removed if we use python to generate the train/test split files.

	trainInd = trainInd[:,splitNum-1]
	testInd = testInd[:,splitNum-1]

	# This can be removed once we remove the dependency on the mat files.. MATLAB is 1 indexesd while python is 0-indexed
	trainInd = trainInd - 1
	testInd = testInd - 1

	# Separate out train and test features
	trainX = features[trainInd,:]
	testX = features[testInd,:]

	# First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
	trainY = labels[:,trainInd]
	trainY = trainY.transpose()
	trainY = np.reshape(trainY,(len(trainY)))

	# First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
	testY = labels[:,testInd]
	testY = testY.transpose()
	testY = np.reshape(testY,(len(testY)))

	return (trainX, trainY, testX, testY)

def generateTrainTestData_multi(features, labels, trainInd, testInd, splitNum):
	trainInd = trainInd[:,splitNum-1]
	testInd = testInd[:,splitNum-1]
	# This can be removed once we remove the dependency on the mat files.. MATLAB is 1 indexesd while python is 0-indexed
	trainInd = trainInd - 1
	testInd = testInd - 1
	# Separate out train and test features
	trainX = features[trainInd,:]
	testX = features[testInd,:]
	# First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
	trainY = labels[trainInd,:]
	testY = labels[testInd,:]
	return (trainX, trainY, testX, testY)

def trainSVR(trainX, trainY, testX, testY, cVal, param):
	''' Initialize and fit an SVR'''
	reg = SVR(C= cVal, gamma  = param)

	start_time = time.time()
	reg.fit(trainX, trainY)

	print testX[0,:].shape

	# Predict it on the test data
	start_time = time.time()
	predY = reg.predict(testX[0,:])

	testTime = time.time() - start_time

	print("%s seconds to predict the interenstingness score of 1 image" %(testTime))

	# Evaluate on the test data
	predY = reg.predict(testX)
	corrV = sstats.spearmanr(predY,testY)

	#Fetch SVR parameters.
	#svrParams = reg.get_params()
	#return (testTime, predY)
	return (corrV, predY)

def gridSearchSVR(trainX, trainY, testX, testY):
	''' Initialize and fit an SVR'''
	cVal = [1, 2, 3, 4]

	gVal = np.logspace(-7,-1,7).tolist()

	parameters = {"C":cVal, "gamma":gVal}

	svr = SVR()

	reg = grid_search.GridSearchCV(svr, parameters)

	start_time = time.time()

	reg.fit(trainX, trainY)

	print("%s seconds to fit SVR" %(time.time() - start_time))

	# Predict it on the test data
	predY = reg.predict(testX)

	# Evaluate on the test data
	corrV = sstats.spearmanr(predY,testY)

	#Fetch SVR parameters.
	svrParams = reg.best_estimator_

	print svrParams

	return (corrV, predY)

def trainDecisionForests(trainX, trainY, testX, testY,nEst):
	dtReg = RandomForestRegressor(n_estimators=nEst)

	start_time = time.time()
	dtReg.fit(trainX, trainY)
	print("%s seconds to fit RDF " %(time.time() - start_time))


	predY=  dtReg.predict(testX)

	# Evaluate on the test data
	corrV = sstats.spearmanr(predY,testY)

	dtParams = dtReg.get_params(deep=True)

	return (corrV, dtParams)

def trainLearners(features, labels, cVal, param):
	#print features.shape, labels.shape
	trainInd = readMatFile('Data/TrainingIter.mat','TrainingIter')
	testInd = readMatFile('Data/TestingIter.mat','TestingIter')
	trainX, trainY, testX, testY = generateTrainTestData(features,labels,trainInd,testInd,1)
	print 'RBF'
	corrV, predY = trainSVR(trainX, trainY, testX, testY, cVal, param)
	print corrV
	#corrV, predY = gridSearchSVR(trainX, trainY, testX, testY)
	print 'Linear'
	corrV, predY = linearSVR(trainX, trainY, testX, testY)
	print corrV
	return (corrV, testY, predY)

def compressFeatures(feat, dimK):
#Given feat, compress them to dimK dimensions.
	nI,nF = feat.shape # Num. of images X num. features
	A = np.zeros((dimK,nF))

	for i in range(0,dimK):
		A[i,:] = np.random.normal(0,1,(1,nF))

	feat = feat.transpose()
	csFeat = np.dot(A,feat)
	csFeat = csFeat.transpose()

	return csFeat

def compare567():
	corrVFC7, testYFC7, predYFC7 = trainLearners(memFc7,labels, 1, 0.0)

	corrVFC6, testYFC6, predYFC6 = trainLearners(memFc6,labels, 1, 4.6*pow(10,-6))

	corrVFC5, testYFC5, predYFC5 = trainLearners(memConv5, labels, 2, 1*pow(10,-7))

	absDiffFC5 = np.reshape(abs(testYFC5 - predYFC5),(len(predYFC5),1))

	absDiffFC6 = np.reshape(abs(testYFC6 - predYFC6),(len(predYFC6),1))

	absDiffFC7 = np.reshape(abs(testYFC7 - predYFC7),(len(predYFC7),1))


	concatDiff = np.concatenate((absDiffFC5, absDiffFC6, absDiffFC7),axis=1)

	betterFC  = np.argmin(concatDiff, axis = 1)

	print 'Num of images that prefer fc5, 6, 7 respectively are'
	print (betterFC ==0).sum(), (betterFC ==1).sum(), (betterFC ==2).sum()


	#df = DataFrame({'gTruth':testYFC6, 'predYFC6':predYFC6, 'predYFC5':predYFC5, 'absDiffFC5':absDiffFC5.reshape(-1), 'absDiffFC6':absDiffFC6.reshape(-1),'betterFC':betterFC.reshape(-1)})

	#df.to_excel('Results/class1_fc567.xlsx', sheet_name='sheet1', index=['gTruth','predYFC6', 'predYFC5', 'absDiffFC5', 'absDiffFC6', 'betterFC'])

	plt.figure(1)
	plt.scatter(testYFC7,predYFC7,c='r')
	plt.scatter(testYFC6,predYFC6,c='g')
	plt.scatter(testYFC5,predYFC5,c='b')

	plt.xlabel('Ground truth')
	plt.ylabel('Predicted scores')

	plt.show()

def linearSVR(trainX, trainY, testX, testY):
	clf = SVR(kernel='linear')
	clf.fit(trainX, trainY)


	print testX[0,:].shape

	start_time = time.time()

	predY = clf.predict(testX[0,:])

	testTime = time.time() - start_time

	print("%s seconds to fit SVR" %(testTime))

	predY = clf.predict(testX)
	corrV = sstats.spearmanr(predY,testY)
	return (corrV, predY)


def gridSearchLinearSVR(trainX, trainY, testX, testY):
	''' Initialize and fit an SVR'''
	#cVal = [0.25, 0.5, 1, 2, 3, 4]
	cVal = [2]
	parameters = {'kernel':['linear'], 'C':cVal}
	svr = SVR()
	reg = grid_search.GridSearchCV(svr, parameters)
	start_time = time.time()
	reg.fit(trainX, trainY)
	print("%s seconds to fit SVR" %(time.time() - start_time))
	# Predict it on the test data
	predY = reg.predict(testX)
	# Evaluate on the test data
	corrV = sstats.spearmanr(predY,testY)
	#Fetch SVR parameters.
	svrParams = reg.best_estimator_
	print svrParams
	return (corrV, predY)

def testCompressedFeatures(features, labels):
	#List the different dimensionalities we want to reduce it to.
	K_val = [50, 200, 500, 1000, 2500, 4096, 7500, 9000] # for fc5

	numKVals = len(K_val)
	bestCorr = []
	linearTestTime = []
	rbfTestTime = []

	trainInd = readMatFile('Data/TrainingIter.mat','TrainingIter')
	testInd = readMatFile('Data/TestingIter.mat','TestingIter')

	for i in xrange(numKVals):

		csFeats = compressFeatures(features, K_val[i])
		X_scaled = preprocessing.scale(csFeats)
		trainX, trainY, testX, testY = generateTrainTestData(X_scaled,labels,trainInd,testInd,1)
		tTimeL, predY = linearSVR(trainX, trainY, testX, testY)
		tTimeR, predY = trainSVR(trainX, trainY, testX, testY,1,1*pow(10,-7))
		print 'Appending to the array'
		#bestCorr.append(corrV)
		linearTestTime.append(tTimeL)
		rbfTestTime.append(tTimeR)


	# fig1 = plt.figure()
	# plt.scatter(K_val,bestCorr,c='r')
	# plt.xlabel('K values')
	# plt.ylabel('Correlations')

	fig= plt.figure()
	plt.plot(K_val,linearTestTime,'ro-', label="Linear-SVR")
	plt.plot(K_val,rbfTestTime,'b+-', label="RBF-SVR")

	legend = plt.legend(loc='upper center', shadow=True, fontsize='x-large')

	plt.xlabel('Dimensionality of the compressed features')
	plt.ylabel('SVR prediction time per image')
	plt.show()

	return (linearTestTime, rbfTestTime)

def clusterFeatures(features, numKCenters):
	k_means = KMeans(n_clusters=numKCenters)
	k_means.fit(features)
	values = k_means.cluster_centers_.squeeze()
	labels = k_means.labels_



if __name__ == '__main__':

	print("\n-- get data:")
	caffeFeatDir = '/media/deepti/I/data/ImageData/caffeFeatures/'

	data = np.load(caffeFeatDir+'caffeFeats_mem_4-7.npz')

	data1 = np.load(caffeFeatDir+'caffeFeats_mem_1-3.npz')

	memConv5 = data['conv5']
	memFc6 = data['fc6']
	memFc7 = data['fc7']

	memPool5 = data['pool5']

	layer4 = data['conv4']

	layer3 = data1['conv3']

	layer2 = data1['pool2']

	layer1 = data1['pool1']


	del data

	labels = readMatFile('Data/interestingness_scores.mat','interestingness_score')

	#(linearTestTime, rbfTestTime) = testCompressedFeatures(memConv5, labels)

	#bestCorr5RBF = testCompressedFeatures(memConv5, labels)

#	corrVFC7, testYFC7, predYFC7 = trainLearners(memFc7, labels, 1, 1*pow(10,-7))

#	corrVFC6, testYFC6, predYFC6 = trainLearners(memFc6, labels, 1, 1*pow(10,-7))

	#corrVFC5, testYFC5, predYFC5 = trainLearners(memPool5, labels, 1, 1*pow(10,-7))

	#corrVFC4, testYFC4, predYFC4 = trainLearners(layer4, labels, 1, 1*pow(10,-7))

#	corrVFC3, testYFC3, predYFC3 = trainLearners(layer3, labels, 1, 1*pow(10,-7))

	corrVFC2, testYFC2, predYFC2 = trainLearners(layer2, labels, 1, 1*pow(10,-7))

	corrVFC1, testYFC1, predYFC1 = trainLearners(layer1, labels, 1, 1*pow(10,-7))