function I = imgread1(imNum, dirName)
    I = imread(strcat(dirName,'mem_',num2str(imNum),'.jpg'));
end