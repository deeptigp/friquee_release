individualFeatCompTime = [63.05 52.04 34.41 34.50];

s1 = load('finalResults/perLayerResults_1_10_split4.mat');
s1 = s1.perLayerResults_1_10_split;
s2 = load('finalResults/perLayerResults_1_10_split5.mat');
s2 = s2.perLayerResults_1_10_split;
s3 = load('finalResults/perLayerResults_1_10_split6.mat');
s3 = s3.perLayerResults_1_10_split;

%% aggregate results 
a1 = load('finalResults/aggregateResults/4/aggregatePredictedScores_thresh_fc567.mat');
a1 = a1.aggregateResults_thresh_linearC;

a2 = load('finalResults/aggregateResults/5/aggregatePredictedScores_thresh_fc567.mat');
a2 = a2.aggregateResults_thresh_linearC;

a3 = load('finalResults/aggregateResults/6/aggregatePredictedScores_thresh_fc567.mat');
a3 = a3.aggregateResults_thresh_linearC;


meanSROCC7 = mean([s1.medRHO7 s2.medRHO7 s3.medRHO7]);

meanLCC7 = mean([s1.medLCC7 s2.medLCC7 s3.medLCC7]);

meanSROCC6 = mean([s1.medRHO6 s2.medRHO6 s3.medRHO6]);

meanLCC6 = mean([s1.medLCC6 s2.medLCC6 s3.medLCC6]);

meanSROCC5 = mean([s1.medRHO5 s2.medRHO5 s3.medRHO5]);

meanLCC5 = mean([s1.medLCC5 s2.medLCC5 s3.medLCC5]);

meanSROCC5c = mean([s1.medRHO5c s2.medRHO5c s3.medRHO5c]);

meanLCC5c = mean([s1.medLCC5c s2.medLCC5c s3.medLCC5c]);


meanRunTime7 = mean([s1.runTimeForAllImgs7 s2.runTimeForAllImgs7 s3.runTimeForAllImgs7]);

meanRunTime6 = mean([s1.runTimeForAllImgs6 s2.runTimeForAllImgs6 s3.runTimeForAllImgs6]);

meanRunTime5 = mean([s1.runTimeForAllImgs5 s2.runTimeForAllImgs5 s3.runTimeForAllImgs5]);

meanRunTimeCompress5 = mean([s1.runTimeForAllImgs5c s2.runTimeForAllImgs5c s3.runTimeForAllImgs5c]);

meanTotalTime = individualFeatCompTime + [meanRunTime7 meanRunTime6 meanRunTime5 meanRunTimeCompress5];

%%--cascaded results -- %%

meanLCCCascade = mean([a1.aggrePLCC a2.aggrePLCC a3.aggrePLCC]);

meanRHOCascade = median([a1.aggreSROCC a2.aggreSROCC a3.aggreSROCC]);

meanCTimeCascade = mean([a1.totalClassificationTime a2.totalClassificationTime a3.totalClassificationTime]);

meanRTimeCascade = mean([a1.totalRegressionTime a2.totalRegressionTime a3.totalRegressionTime]);

meanFCTimeCascade = mean([a1.totalFeatureComputeTime a2.totalFeatureComputeTime a3.totalFeatureComputeTime]);

meanTotalTestTimeCascade = mean([a1.totalTestTime  a2.totalTestTime a3.totalTestTime]);


%%Performance of the gating functions..
g51 = load('finalResults/aggregateResults/4/cascadeClassifierResults_aggre567_thresh_linearC_pool5.mat');
g51 = g51.cascadeClassifierResults_aggre567_thresh_linearC_pool5;
g52 = load('finalResults/aggregateResults/5/cascadeClassifierResults_aggre567_thresh_linearC_pool5.mat');
g52 = g52.cascadeClassifierResults_aggre567_thresh_linearC_pool5;
g53 = load('finalResults/aggregateResults/6/cascadeClassifierResults_aggre567_thresh_linearC_pool5.mat');
g53 = g53.cascadeClassifierResults_aggre567_thresh_linearC_pool5;

gf5Accur = mean([g51.accuracy(1) g52.accuracy(1) g53.accuracy(1)]);
gf5Prec = mean([g51.mPrec g52.mPrec g53.mPrec]);
gf5Recall = mean([g51.mRecall g52.mRecall g53.mRecall]);


g61 = load('finalResults/aggregateResults/4/cascadeClassifierResults_aggre567_thresh_linearC_fc6.mat');
g61 = g61.cascadeClassifierResults_aggre567_thresh_linearC_fc6;
g62 = load('finalResults/aggregateResults/5/cascadeClassifierResults_aggre567_thresh_linearC_fc6.mat');
g62 = g62.cascadeClassifierResults_aggre567_thresh_linearC_fc6;
g63 = load('finalResults/aggregateResults/6/cascadeClassifierResults_aggre567_thresh_linearC_fc6.mat');
g63 = g63.cascadeClassifierResults_aggre567_thresh_linearC_fc6;

gf6Accur = mean([g61.accuracy(1) g62.accuracy(1) g63.accuracy(1)]);
gf6Prec = mean([g61.mPrec g62.mPrec g63.mPrec]);
gf6Recall = mean([g61.mRecall g62.mRecall g63.mRecall]);


aggr3Splits = struct;


%%Gating funcs. performance
aggr3Splits.gf5Accur = gf5Accur;
aggr3Splits.gf5Prec = gf5Prec;
aggr3Splits.gf5Recall = gf5Recall;


aggr3Splits.gf6Accur = gf6Accur;
aggr3Splits.gf6Prec = gf6Prec;
aggr3Splits.gf6Recall = gf6Recall;


aggr3Splits.meanSROCC7 = meanSROCC7;
aggr3Splits.meanLCC7 = meanLCC7;

aggr3Splits.meanSROCC6 = meanSROCC6;
aggr3Splits.meanLCC6 = meanLCC6;

aggr3Splits.meanSROCC5 = meanSROCC5;
aggr3Splits.meanLCC5 = meanLCC5;

aggr3Splits.meanSROCC5c = meanSROCC5c;
aggr3Splits.meanLCC5c = meanLCC5c;

aggr3Splits.meanRunTime7 = meanRunTime7;
aggr3Splits.meanRunTime6 = meanRunTime6;
aggr3Splits.meanRunTime5 = meanRunTime5;
aggr3Splits.meanRunTimeCompress5 = meanRunTimeCompress5;

aggr3Splits.meanTotalTime = meanTotalTime;

aggr3Splits.meanLCCCascade = meanLCCCascade;
aggr3Splits.meanRHOCascade = meanRHOCascade;
aggr3Splits.meanCTimeCascade = meanCTimeCascade;
aggr3Splits.meanRTimeCascade = meanRTimeCascade;
aggr3Splits.meanFCTimeCascade = meanFCTimeCascade;
aggr3Splits.meanTotalTestTimeCascade = meanTotalTestTimeCascade;

save('finalResults/aggr3Splits_4-6.mat','aggr3Splits');
