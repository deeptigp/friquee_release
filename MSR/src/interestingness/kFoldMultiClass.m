clear;
load('Data/layer3Features.mat');
load('Data/layer4Features.mat');
load('Data/pool5Features.mat');
load('Data/fc6Features.mat');
load('Data/fc7Features.mat');

load('Data/interestingness_scores.mat');

labels = interestingness_score;

numImages = size(pool5Features,1);

kFold = 5;

foldIndices = crossvalind('Kfold', 2222, kFold);

predictedClassLabels = zeros(1,numImages);

absDiffAll = zeros(numImages,3);

minVAll = zeros(1,numImages);

kFold_567 = struct;

for k = 1:kFold
    k
    testIndx = find(foldIndices==k);
    trainIndx = find(foldIndices ~=k);
    
    testLabels = labels(testIndx);
    trainLabels = labels(trainIndx);
    
    [size(testIndx) size(trainIndx)]
    
  %  [lcc3,rho3, pred3] = temp_linearSVR(layer3Features(trainIndx,:),trainLabels',layer3Features(testIndx,:),testLabels','fc3');
    
%    [lcc4,rho4, pred4] = temp_linearSVR(layer4Features(trainIndx,:),trainLabels',layer4Features(testIndx,:),testLabels','fc4');
    
    [lcc5,rho5, pred5] = temp_linearSVR(pool5Features(trainIndx,:),trainLabels',pool5Features(testIndx,:),testLabels','fc5');

    [lcc6,rho6, pred6] = temp_linearSVR(fc6Features(trainIndx,:),trainLabels',fc6Features(testIndx,:),testLabels','fc6');

    [lcc7,rho7, pred7] = temp_linearSVR(fc7Features(trainIndx,:),trainLabels',fc7Features(testIndx,:),testLabels','fc7');

    absDiff = [ abs(pred5-testLabels')  abs(pred6-testLabels') abs(pred7-testLabels')];% ...
        % abs(pred4-testLabels')];


    % 1: stands for fc5, 2 for fc6, 3 for fc7
    [minV, minInd] = min(absDiff,[],2);
    predictedClassLabels(testIndx) = minInd; 
    absDiffAll(testIndx,:) = absDiff;
    minVAll(testIndx) = minV;
end
kFold_567.foldIndices = foldIndices;
kFold_567.derivedGTLabels = predictedClassLabels;
kFold_567.absDiffAll = absDiffAll;
kFold_567.minVAll = minVAll;

save('Results/kFold_567_run2.mat','kFold_567');
