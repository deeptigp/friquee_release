%%%%% 1. Getting the individual perf. metrics
% load('Data/interestingness_scores.mat');
% 
% groundTruthLabels = interestingness_score';
% 
% load('Data/pool5Features.mat');
% load('Data/pool5Features_compressed.mat');
% 
% load('Data/fc6Features.mat');
% 
% load('Data/fc7Features.mat');
% 
% for split = 4:6
%     
% 
%     [medLCC7, medRHO7, Y_hat7, rmse, psnrVal, runTimeForAllImgs7, runTimePerImg] = executeSVM(fc7Features,groundTruthLabels, split, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat', 'fc7');
%     [medLCC6, medRHO6, Y_hat6, rmse, psnrVal, runTimeForAllImgs6, runTimePerImg] = executeSVM(fc6Features,groundTruthLabels, split, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat', 'fc6');
%     [medLCC5, medRHO5, Y_hat5, rmse, psnrVal, runTimeForAllImgs5, runTimePerImg] = executeSVM(pool5Features,groundTruthLabels, split, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat', 'pool5');
%     [medLCC5c, medRHO5c, Y_hat5c, rmse, psnrVal, runTimeForAllImgs5c, runTimePerImg] = executeSVM(pool5Features_compressed,groundTruthLabels, split, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat', 'pool5');
%     
%     %[medLCC4, testY, Y_hat4, runTimeForAllImgs4] = executeSVM(conv4Features_compressed,groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'), 'conv4')
%     
%     perLayerResults_1_10_split = struct;
%     perLayerResults_1_10_split .medLCC7 = medLCC7;
%     perLayerResults_1_10_split .medRHO7 = medRHO7;
%     perLayerResults_1_10_split .Y_hat7 = Y_hat7;
%     perLayerResults_1_10_split .runTimeForAllImgs7 = runTimeForAllImgs7;
%     
%     perLayerResults_1_10_split .medLCC6 = medLCC6;
%     perLayerResults_1_10_split .medRHO6 = medRHO6;
%     perLayerResults_1_10_split .Y_hat6 = Y_hat6;
%     perLayerResults_1_10_split .runTimeForAllImgs6 = runTimeForAllImgs6;
%     
%     perLayerResults_1_10_split .medLCC5 = medLCC5;
%     perLayerResults_1_10_split .medRHO5 = medRHO5;
%     perLayerResults_1_10_split .Y_hat5 = Y_hat5;
%     perLayerResults_1_10_split .runTimeForAllImgs5 = runTimeForAllImgs5;
%     
%     perLayerResults_1_10_split .medLCC5c = medLCC5c;
%     perLayerResults_1_10_split .medRHO5c = medRHO5c;
%     perLayerResults_1_10_split .Y_hat5c = Y_hat5c;
%     perLayerResults_1_10_split .runTimeForAllImgs5c = runTimeForAllImgs5c;
%     
%     
%     save(strcat('finalResults/perLayerResults_1_10_split',num2str(split),'.mat'),'perLayerResults_1_10_split');
%     cascadedBinaryClassifiers_interestingness_567(split);
% end
% 

%%==COMPUTING THE COST AND THE ACCURACY THAT WE CAN OBTAIN ASSUMING OUR
%%CLASSIFIER IS PERFECT ===%%%

meanFeatTime = 0;
meanSVRTime  = 0;
meanTotalTime  = 0;
meanPLCC = 0;
meanSROCC = 0;


for split = 4:6
    aggregateResults{split} =  final_perfect_cascade(split);
    meanFeatTime  = meanFeatTime + aggregateResults{split}.totalFeatureComputeTime;
    meanSVRTime  = meanSVRTime + aggregateResults{split}.totalRegressionTime;
    meanTotalTime  = meanTotalTime + aggregateResults{split}.totalTestTime;
    meanPLCC  = meanPLCC + aggregateResults{split}.aggrePLCC;
    meanSROCC  = meanSROCC + aggregateResults{split}.aggreSROCC;
end

meanFeatTime = meanFeatTime/3;
meanSVRTime = meanSVRTime/3;
meanTotalTime = meanTotalTime/3;
meanPLCC  = meanPLCC/3;
meanSROCC  = meanSROCC/3;

bestAchievable = struct;
bestAchievable.meanFeatTime = meanFeatTime;
bestAchievable.meanSVRTime = meanSVRTime;
bestAchievable.meanTotalTime = meanTotalTime;
bestAchievable.meanPLCC = meanPLCC;
bestAchievable.meanSROCC = meanSROCC;

save('finalResults/bestAchievable.mat','bestAchievable');