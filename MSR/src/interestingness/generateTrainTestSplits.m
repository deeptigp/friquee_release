%numImages  = 32500; % for a random set of 5 classes from imNet
numImages = 13000;
trainSplit = 0.8;
    
numTrainImgs = round(trainSplit*numImages);
    
TrainingNIter = [];
TestingNIter = [];

for n = 1:10
    indx = randperm(numImages);

    trainIndx = indx(1:numTrainImgs);
    testIndx = indx(1+numTrainImgs:end);
    
    TrainingNIter = [TrainingNIter trainIndx'];
    TestingNIter = [TestingNIter testIndx'];
end

save('imNet/Data/TrainingNIter_10Class.mat','TrainingNIter');
save('imNet/Data/TestingNIter_10Class.mat','TestingNIter');