dirName = '/media/deepti/SamsungSSD/ImageNet/imageNet_Test/ILSVRC2012_img_val/';

imagefiles = dir(strcat(dirName,'*.JPEG'));    

imNetValImgFileNames = {};

for ii=1:length(imagefiles)
   imNetValImgFileNames{ii} = [dirName imagefiles(ii).name];
end

save('imNetValImgFileNames.mat','imNetValImgFileNames');