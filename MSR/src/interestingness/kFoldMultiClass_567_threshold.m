clear;
load('Data/pool5Features.mat');
load('Data/fc6Features.mat');
load('Data/fc7Features.mat');

load('Data/interestingness_scores.mat');

labels = interestingness_score;

numImages = size(pool5Features,1);

kFold = 5;

foldIndices = crossvalind('Kfold', 2222, kFold);

predictedClassLabels = zeros(1,numImages);

absDiffAll = zeros(numImages,3);

predictedInterestingnessScoresAll = zeros(numImages,3);

minVAll = zeros(1,numImages);

kFold_567 = struct;

for k = 1:kFold
    k
    testIndx = find(foldIndices==k);
    trainIndx = find(foldIndices ~=k);
    
    testLabels = labels(testIndx);
    trainLabels = labels(trainIndx);
    
    [size(testIndx) size(trainIndx)]
        
    [medLCC5, medRHO5, testY5, Y_hat5, runTimeForAllImgs5, runTimePerImg5] = runSVM1(pool5Features(trainIndx,:),trainLabels',pool5Features(testIndx,:),testLabels','fc5');

    [medLCC6, medRHO6, testY6, Y_hat6, runTimeForAllImgs6, runTimePerImg6] = runSVM1(fc6Features(trainIndx,:),trainLabels',fc6Features(testIndx,:),testLabels','fc6');

    [medLCC7, medRHO7, testY7, Y_hat7, runTimeForAllImgs7, runTimePerImg7] = runSVM1(fc7Features(trainIndx,:),trainLabels',fc7Features(testIndx,:),testLabels','fc7');
    
    absDiff = [ abs(pred5-testLabels')  abs(pred6-testLabels') abs(pred7-testLabels')];
     
    % 1: stands for fc5, 2 for fc6, 3 for fc7
    [minV, minInd] = min(absDiff,[],2);
    predictedClassLabels(testIndx) = minInd; 
    predictedInterestingnessScoresAll(testIndx,:) = [pred5 pred6 pred7];
    absDiffAll(testIndx,:) = absDiff;
    minVAll(testIndx) = minV;
end

kFold_567.foldIndices = foldIndices;
kFold_567.derivedGTLabels = predictedClassLabels;
kFold_567.absDiffAll = absDiffAll;
kFold_567.predictedInterestingnessScoresAll = predictedInterestingnessScoresAll;
kFold_567.minVAll = minVAll;

save('Results/kFold_567_thresh.mat','kFold_567');
