function vecN = featureNormalize(X)
    %# get max and min
    maxVec = max(X(:));
    minVec = min(X(:));

    %# normalize to -1...1
    vecN = ((X-minVec)./(maxVec-minVec) - 0.5 ) *2;
end