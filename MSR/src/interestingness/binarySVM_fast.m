function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg, mPrec, mRecall] = binarySVM_fast(trainX, trainY, testX, testY)
    % Create a model
   C = '2'; gamma = '1';
   
   trainOpts = ['-t 0 -s 0 -b 1 -g ',gamma ' -c ',C];
    
    trainedModel = svmtrain(double(trainY),double(trainX),trainOpts);
    
    tStart = tic;
    
    [Y_hat, medACC , ~] = svmpredict (double(testY), double(testX), trainedModel, '-b 0 -q');
    runTimeForAllImgs = toc(tStart);
    
    runTimePerImg = runTimeForAllImgs/length(testY);
    [mPrec, mRecall] = computePrecRecall(testY, Y_hat);
end