%% This script is to use fc7-pool5 features and train simple regressors
load('Data/fc7Features.mat');
load('Data/fc6Features.mat');
load('Data/pool5Features.mat');
load('Data/layer4Features.mat');
load('Data/layer3Features.mat');
load('Data/interestingness_scores.mat');

load('Data/TrainingNIter.mat');
load('Data/TestingNIter.mat');

load('Results/kFold_34567.mat');

labels = interestingness_score;

numImages = length(labels);

class1LabelIndxs = find(kFold_34567.derivedGTLabels == 1); %pool5
class2LabelIndxs = find(kFold_34567.derivedGTLabels == 2);%layer6 
class3LabelIndxs = find(kFold_34567.derivedGTLabels == 3);%layer 7
class4LabelIndxs = find(kFold_34567.derivedGTLabels == 4); % Layer4
class5LabelIndxs = find(kFold_34567.derivedGTLabels == 5); % Layer3

binaryLabels1 = zeros(1,numImages);
binaryLabels2 = zeros(1,numImages);
binaryLabels3 = zeros(1,numImages);
binaryLabels4 = zeros(1,numImages); % Layer4
binaryLabels5 = zeros(1,numImages); % Layer3


%% Set the class labels for train and test data.
binaryLabels1(class1LabelIndxs) = 1; %pool5
binaryLabels2(class2LabelIndxs) = 1; % fc6
binaryLabels3(class3LabelIndxs) = 1; %fc7
binaryLabels4(class4LabelIndxs) = 1; %Layer4
binaryLabels5(class5LabelIndxs) = 1; %Layer3

trainIndx = TrainingNIter(:,1);
testIndx = TestingNIter(:,1);

fc34567SVC = struct;

% [accuracy, Y_hat, runTimePerImg3, ~] = binarySVC(layer3Features(trainIndx,:),binaryLabels5(trainIndx)',layer3Features(testIndx,1),[binaryLabels5(testIndx(1))]);
% 
% fc34567SVC.bSVCRunTime3 = runTimePerImg3;
% 
% [accuracy, Y_hat, runTimePerImg4, ~] = binarySVC(layer4Features(trainIndx,:),binaryLabels4(trainIndx)',layer4Features(testIndx,1),[binaryLabels4(testIndx(1))]);
% 
% fc34567SVC.bSVCRunTime4 = runTimePerImg4;

[accuracy, Y_hat, runTimePerImg5, ~] = binarySVC1(pool5Features(trainIndx,:),binaryLabels1(trainIndx)',pool5Features(testIndx,1),[binaryLabels1(testIndx(1))]);

fc34567SVC.bSVCRunTime5 = runTimePerImg5;

[accuracy, Y_hat, runTimePerImg6, ~] = binarySVC1(fc6Features(trainIndx,:),binaryLabels2(trainIndx)',fc6Features(testIndx,1),[binaryLabels2(testIndx(1))]);

fc34567SVC.bSVCRunTime6 = runTimePerImg6;

[accuracy, Y_hat, runTimePerImg7, ~] = binarySVC1(fc7Features(trainIndx,:),binaryLabels3(trainIndx)',fc7Features(testIndx,1),[binaryLabels3(testIndx(1))]);

fc34567SVC.bSVCRunTime7 = runTimePerImg7;

save('Results/fc34567SVC.mat','fc34567SVC');