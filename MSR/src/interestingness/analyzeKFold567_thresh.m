toPlot = 0;
thresh = 0.05;
load('Results/kFold_567.mat');

absDiff = kFold_567.absDiffAll;
editedDerivedLabels  = kFold_567.derivedGTLabels;

%% Find the max and min value.
%{
max5 = max(absDiff(:,1));
min5 = min(absDiff(:,1));
med5 = median(absDiff(:,1));

max6 = max(absDiff(:,2));
min6 = min(absDiff(:,2));
med6 = median(absDiff(:,2));

max7 = max(absDiff(:,3));
min7 = min(absDiff(:,3));
med7 = median(absDiff(:,3));

maxB3 = max(kFold_567.minVAll)
minB3 = min(kFold_567.minVAll);
medB3 = median(kFold_567.minVAll);
%}

%% Find all the images for which fc5 was chosen as the label.
%Global indexes
indxLayer5 = find(kFold_567.derivedGTLabels == 1);

%% Compute how different the absDiffs were for fc6 and fc7 for these labels.
fc6Diff_fc5 = abs(absDiff(indxLayer5,2)-absDiff(indxLayer5,1));

fc7Diff_fc5 = abs(absDiff(indxLayer5,3)-absDiff(indxLayer5,1));

%% For all those where pool5 was chosen, check if fc7 can be assigned, and do so if allowed.
changeFC5ToFC7Indxs = indxLayer5(fc7Diff_fc5 <= thresh );
length(changeFC5ToFC7Indxs)
editedDerivedLabels(changeFC5ToFC7Indxs) = 3;

checkFC5ToFC6Indxs = indxLayer5(fc6Diff_fc5 <= thresh );

remainingIndxs = setdiff(checkFC5ToFC6Indxs,changeFC5ToFC7Indxs);

length(remainingIndxs)
editedDerivedLabels(remainingIndxs) = 2;


if(toPlot == 1)
    fig1 = figure;
    hist([fc6Diff_fc5 fc7Diff_fc5]);
    legend('fc6','fc7');
    title('Distribution of how much fc7 and fc6s predictions differ from pool5s');
    print(fig1, strcat(SAVEPATH,'diffOfDiff5_67_kFold567'),'-djpeg');
end

%% === %% 
%indxLayer6 = find(kFold_567.derivedGTLabels == 2);
indxLayer6 = find(editedDerivedLabels == 2);

%% Compute how different the absDiffs were for fc7 for these labels.
fc7Diff_fc6 = abs(absDiff(indxLayer6,3)-absDiff(indxLayer6,2));

changeFC6ToFC7Indxs = indxLayer6(fc7Diff_fc6 <= thresh );
length(changeFC6ToFC7Indxs)
editedDerivedLabels(changeFC6ToFC7Indxs) = 3;


if(toPlot == 1)
    fig2 = figure;
    hist(fc7Diff_fc6,30);
    xlabel('|fc6Diff - fc7Diff|')
    ylabel('Count');
    title('Distribution of how much fc7 predictions differ from fc6s');
    print(fig2, strcat(SAVEPATH,'diffOfDiff6_7_kFold567'),'-djpeg');
end

kFold_567.editedDerivedLabels = editedDerivedLabels;

save('Results/kFold_567_thresh.mat','kFold_567');