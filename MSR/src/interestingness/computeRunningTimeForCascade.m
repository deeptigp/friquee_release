function [totalClassificationTime, totalRegressionTime, totalFeatureComputeTime, totalTestTime, perLayerRunningTime, perLayerFeatTime] = computeRunningTimeForCascade()
load('Results/aggregateResults/interestingness/cascadePredictedResults_aggre567_thresh_linearC_pool5.mat');
load('Results/aggregateResults/interestingness/cascadePredictedResults_aggre567_thresh_linearC_fc6.mat');
load('Results/aggregateResults/interestingness/cascadePredictedResults_aggre567_thresh_linearC_fc7.mat');

load('Results/aggregateResults/interestingness/cascadeClassifierResults_aggre567_thresh_linearC_fc7.mat');
load('Results/aggregateResults/interestingness/cascadeClassifierResults_aggre567_thresh_linearC_fc6.mat');
load('Results/aggregateResults/interestingness/cascadeClassifierResults_aggre567_thresh_linearC_pool5.mat');

featComputeTime = [ 0.0775 0.0397 0.0248];

perLayerRunningTime = 0;
perLayerFeatTime = 0;

perLayerFeatComputeTime = [ 0.1420 0.1172 0.0775]; % fc7-fc6-pool5

numDataForBC = [size(cascadeClassifierResults_aggre567_thresh_linearC_pool5.testIndx,1) size(cascadeClassifierResults_aggre567_thresh_linearC_fc6.testIndx,1) size(cascadeClassifierResults_aggre567_thresh_linearC_fc7.testIndx,1)];

totalClassificationTime = cascadeClassifierResults_aggre567_thresh_linearC_pool5.runTimeForAllImgs + cascadeClassifierResults_aggre567_thresh_linearC_fc6.runTimeForAllImgs;

totalRegressionTime = cascadePredictedResults_aggre567_thresh_linearC_pool5.runTimeForAllImgs5 + cascadePredictedResults_aggre567_thresh_linearC_fc6.runTimeForAllImgs6 + cascadePredictedResults_aggre567_thresh_linearC_fc7.runTimeForAllImgs7;

totalFeatureComputeTime = sum(numDataForBC.*featComputeTime); 

totalTestTime = totalClassificationTime + totalRegressionTime + totalFeatureComputeTime;

%perLayerRunningTime = numTestImages*perLayerFeatComputeTime + [perLayerResults.runTimeForAllImgs7 perLayerResults.runTimeForAllImgs6 perLayerResults.runTimeForAllImgs5];

%perLayerFeatTime = numTestImages*perLayerFeatComputeTime ;
% load('imNet/Results/aggregateResults/567/cascadeClassifierTimeAnalysis567.mat');
% 
% % total feat extraction time
% tFeatTime = sum(cascadeClassifierTimeAnalysis567.elementsPerLayerforBC.*cascadeClassifierTimeAnalysis567.featExtractionPerImg);
% tBCTime = sum(cascadeClassifierTimeAnalysis567.binaryClassifierTime);
% tSVRTime = sum(cascadeClassifierTimeAnalysis567.allSVRRunTimes);
% 
% totalRunTime = tFeatTime + tBCTime + tSVRTime
% 
% cascadeClassifierTimeAnalysis567.totalFeatTime = tFeatTime;
% cascadeClassifierTimeAnalysis567.totalBCTime = tBCTime;
% cascadeClassifierTimeAnalysis567.totalSVRTime = tSVRTime;
% cascadeClassifierTimeAnalysis567.totalRunTime = totalRunTime;
% 
% save('imNet/Results/aggregateResults/567/cascadeClassifierTimeAnalysis567.mat','cascadeClassifierTimeAnalysis567');

%load('imNet/Results/aggregateResults/4567/cascadeClassifierTimeAnalysis4567.mat');

% total feat extraction time
% tFeatTime = sum(cascadeClassifierTimeAnalysis4567.elementsPerLayerforBC.*cascadeClassifierTimeAnalysis4567.featExtractionPerImg);
% tBCTime = sum(cascadeClassifierTimeAnalysis4567.binaryClassifierTime);
% tSVRTime = sum(cascadeClassifierTimeAnalysis4567.allSVRRunTimes);
% 
% totalRunTime = tFeatTime + tBCTime + tSVRTime
% 
% cascadeClassifierTimeAnalysis4567.totalFeatTime = tFeatTime;
% cascadeClassifierTimeAnalysis4567.totalBCTime = tBCTime;
% cascadeClassifierTimeAnalysis4567.totalSVRTime = tSVRTime;
% cascadeClassifierTimeAnalysis4567.totalRunTime = totalRunTime;

%save('imNet/Results/aggregateResults/4567/cascadeClassifierTimeAnalysis4567.mat','cascadeClassifierTimeAnalysis4567');