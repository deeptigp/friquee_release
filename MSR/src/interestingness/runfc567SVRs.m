%% This script is to use fc7-pool5 features and train simple regressors
load('Data/fc7Features.mat');
load('Data/fc6Features.mat');
load('Data/pool5Features.mat');
load('Data/layer4Features.mat');
load('Data/layer3Features.mat');
load('Data/interestingness_scores.mat');

fc34567SVR = struct;

[medLCC7, medRHO7, Y_hat7, runTimeForAllImgs7, runTimePerImg7] = executeSVM(fc7Features,interestingness_score, 1, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat','fc7');
fc34567SVR.lcc7 = medLCC7;
fc34567SVR.rho7 = medRHO7;
fc34567SVR.pred7 = Y_hat7;
fc34567SVR.runTimeForAllImgs7 = runTimeForAllImgs7;
fc34567SVR.runTimePerImg7 = runTimePerImg7;

[medLCC6, medRHO6, Y_hat6, runTimeForAllImgs6, runTimePerImg6] = executeSVM(fc6Features,interestingness_score, 1, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat','fc6');
fc34567SVR.lcc6 = medLCC6;
fc34567SVR.rho6 = medRHO6;
fc34567SVR.pred6 = Y_hat6;
fc34567SVR.runTimeForAllImgs6 = runTimeForAllImgs6;
fc34567SVR.runTimePerImg6 = runTimePerImg6;

[medLCC5, medRHO5, Y_hat5, runTimeForAllImgs5, runTimePerImg5] = executeSVM(pool5Features,interestingness_score, 1, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat','pool5');

fc34567SVR.lcc5 = medLCC5;
fc34567SVR.rho5 = medRHO5;
fc34567SVR.pred5 = Y_hat5;
fc34567SVR.runTimeForAllImgs5 = runTimeForAllImgs5;
fc34567SVR.runTimePerImg5 = runTimePerImg5;

[medLCC4, medRHO4, Y_hat4, runTimeForAllImgs4, runTimePerImg4] = executeSVM(layer4Features,interestingness_score, 1, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat','layer4');

fc34567SVR.lcc4 = medLCC4;
fc34567SVR.rho4 = medRHO4;
fc34567SVR.pred4 = Y_hat4;
fc34567SVR.runTimeForAllImgs4 = runTimeForAllImgs4;
fc34567SVR.runTimePerImg4 = runTimePerImg4;


[medLCC3, medRHO3, Y_hat3, runTimeForAllImgs3, runTimePerImg3] = executeSVM(layer3Features,interestingness_score, 1, 'Data/TrainingNIter.mat', 'Data/TestingNIter.mat','layer3');

fc34567SVR.lcc3 = medLCC3;
fc34567SVR.rho3 = medRHO3;
fc34567SVR.pred3 = Y_hat3;
fc34567SVR.runTimeForAllImgs3 = runTimeForAllImgs3;
fc34567SVR.runTimePerImg3 = runTimePerImg3;

save('Results/fc34567SVR.mat','fc34567SVR');