function strategy2DerivedLabels = kFold_final(layer5Feat, layer6Feat, layer7Feat, labels, trainTestSplit)
    
numImages = size(layer5Feat,1);

predictedClassLabels = zeros(numImages,3); % obtained from pool5, fc6, and fc7.

absDiffAll = zeros(numImages,3);

predictedInterestingnessScoresAll = zeros(numImages,3);

minVAll = zeros(1,numImages);
kFold = 5;

foldIndices = crossvalind('Kfold', numImages, kFold);

kFold_567 = struct;

for k = 1:kFold
    k
    testIndx = find(foldIndices==k);
    trainIndx = find(foldIndices ~=k);
    
    testLabels = labels(testIndx);
    trainLabels = labels(trainIndx);
    
    [size(testIndx) size(trainIndx)]
    
    [medLCC5, medRHO5, Y_hat5] = runSVM1(layer5Feat(trainIndx,:),trainLabels',layer5Feat(testIndx,:),testLabels','fc5');

    [medLCC6, medRHO6, Y_hat6] = runSVM1(layer6Feat(trainIndx,:),trainLabels',layer6Feat(testIndx,:),testLabels','fc6');

    [medLCC7, medRHO7, Y_hat7] = runSVM1(layer7Feat(trainIndx,:),trainLabels',layer7Feat(testIndx,:),testLabels','fc7');
    
    absDiff = [ abs(Y_hat5-testLabels')  abs(Y_hat6-testLabels') abs(Y_hat7-testLabels')];
     
    % 1: stands for fc5, 2 for fc6, 3 for fc7
    [minV, minInd] = min(absDiff,[],2);
    predictedClassLabels(testIndx) = minInd; 
    predictedInterestingnessScoresAll(testIndx,:) = [Y_hat5 Y_hat6 Y_hat7];
    absDiffAll(testIndx,:) = absDiff;
    minVAll(testIndx) = minV;
end
%% Logic to assign class labels pool5 (1), fc6 (2), or fc7 (3)


kFold_567.foldIndices = foldIndices;
kFold_567.derivedGTLabels = predictedClassLabels;
kFold_567.absDiffAll = absDiffAll;
kFold_567.predictedInterestingnessScoresAll = predictedInterestingnessScoresAll;
kFold_567.minVAll = minVAll;

strategy2DerivedLabels = kFold_strategy2(kFold_567);

kFold_567.strategy2DerivedLabels = strategy2DerivedLabels;

save(strcat('Results/kFold_567_',num2str(trainTestSplit),'.mat'),'kFold_567');
end

