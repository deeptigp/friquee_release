% Cluster fc7/6/5 into clusters and build classifiers to each cluster.
%clear;
% load('Data/fc5Features.mat');
% load('Data/fc6Features.mat');
% load('Data/fc7Features.mat');
% load('Data/interestingness_scores.mat');
% load('Data/memFileNames.mat');
% 
% imageDir = '/media/deepti/I/data/ImageData/memorabilityImages/';
% 
% feat = fc5Features;
% feat = normr(feat);
% 
% corrPerClass5 = [];
% corrPerClass6 = [];
% corrPerClass7 = [];
% 

%clear fc5Features;

% labels = interestingness_score;
% 
% numClasses = 3;
% 
% [centers, assignments] = vl_kmeans(feat', numClasses, 'Initialization', 'plusplus');
% 
% clear centers;
% 
% hist(single(assignments))


%% Gather points belonging to each cluster.
for i = 3:numClasses
    
    ind = find(assignments == i);
    
    % Randomly visualize 5 of these images.
    for k = 1:10
        subplot(3,10,(i-1)*10+k)
        imshow(strcat(imageDir,memFileNames{ind(k)}));
    end
    
    numImages = sum(assignments == i);
    trainSplit = round(0.8*numImages);
    
    [i numImages]
    
    % All data points.
    X5 = fc5Features(ind,:);
    X6 = fc6Features(ind,:);
    X7 = fc7Features(ind,:);
    
    Y = labels(ind);
    
    % Run this for 5 random train/test splits.
    for k= 3:5
        
        disp(strcat('Split',num2str(k)));
        
        indx = randperm(numImages);

        trainIndx = indx(1:trainSplit);
        testIndx = indx(1+trainSplit:end);

        [lcc5,rho5] = linearSVR(X5(trainIndx,:),Y(trainIndx)',X5(testIndx,:),Y(testIndx)');

        [lcc6,rho6] = linearSVR(X6(trainIndx,:),Y(trainIndx)',X6(testIndx,:),Y(testIndx)');

        [lcc7,rho7] = linearSVR(X7(trainIndx,:),Y(trainIndx)',X7(testIndx,:),Y(testIndx)');

        corrPerClass5  = [corrPerClass5  ; [lcc5 rho5]];
        corrPerClass6  = [corrPerClass6  ; [lcc6 rho6]];
        corrPerClass7  = [corrPerClass7  ; [lcc7 rho7]];
    end
end

%save('Results/kMeansCluster_run_5splits.mat','corrPerClass5','corrPerClass6','corrPerClass7','assignments');
%% Train and test SVRS for fc5,fc6,fc7 per cluster.

