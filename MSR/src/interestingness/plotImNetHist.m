load('imNet/Results/kFold_567_10Class_k4.mat');

numImages = length(kFold_567.foldIndices);
GTLabels = kFold_567.derivedLayerLabels;


labels = {'pool5', 'fc6', 'fc7'};
numClasses = length(labels);

fig = figure;
hist(GTLabels);
set(gca,'Xtick',[1:1:numClasses],'XTickLabel',[1:numClasses])
set(gca,'XTickLabel',labels)
title('Derived ground truth labels on all images from k-fold');
[n,x] = hist(GTLabels);
barstrings = num2str(n');
text(x,n,barstrings,'horizontalalignment','center','verticalalignment','bottom');
print(fig,'imNet/Results/kFold567_derivedLayerLabels','-djpeg');