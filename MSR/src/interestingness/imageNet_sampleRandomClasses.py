''' This program samples 10 random ImageNet classes and extracts different levels of Caffe features.

'''
import numpy as np
import random
import os, os.path
import scipy.io as sio

caffe_root = '/home/deepti/research/caffe/'
save_dir = '/home/deepti/research/MSR/src/interestingness/Data/'

imagenet_labels_filename = caffe_root + 'data/ilsvrc12/synset_words.txt'
imagenet_synsets_filename = caffe_root + 'data/ilsvrc12/synsets.txt'

labels = np.loadtxt(imagenet_labels_filename, str, delimiter='\t')
synsets = np.loadtxt(imagenet_labels_filename, str, delimiter='\t')

#x = range(0,len(synsets))

#random.shuffle(x)

#choose  top 10 random synsets
#print synsets[x[0:10]]

# The current choice is the following:
# Indices [53, 927, 946, 773, 344, 986, 499, 131, 931, 638]


# Generate train test splits for all 10 classes
trainFraction = 0.8
for i in range(1,11):
    dirName = '/media/deepti/I/data/ImageData/ImageNet/Images/'+ `i` +'/'
    imgNames = sorted(os.listdir(dirName))

    imgNames = [dirName + s for s in imgNames]
    numFiles = len(imgNames)
    numTrain = int(round(trainFraction*numFiles))

    x = np.array(range(0,numFiles)).transpose()
    random.shuffle(x)

    trainIndx = x[0:numTrain]
    testIndx = x[numTrain+1:]

    sio.savemat(save_dir+'imNetTrainTest_'+`i`+'.mat',{"imgNames":imgNames,"trainIndx":trainIndx,"testIndx":testIndx})