load('Data/pool5Features.mat');
load('Data/TestingNIter.mat');

for split = 1:3
    testIndx = find(TestingNIter(:,split));
    X = pool5Features(testIndx,:);
    size(X)
    tic; 
    Y = compressData(X,4096); 
    toc;
end