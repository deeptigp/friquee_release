%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM classifier for NIter numbere of times.
function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg] = testSVM(testX, testY, layer)
    addpath('..');
    DATA_PATH = '/media/deepti/SamsungSSD/ImageNet/tmp/';

    testIndFile = strcat(DATA_PATH,'test_ind_',layer,'.txt');
    rangeFile = strcat(DATA_PATH,'range_',layer);
    modelFile = strcat(DATA_PATH,'model_',layer);

    testScaleFile = strcat(DATA_PATH,'test_scale_',layer);


    % Construct the file ( input ) suitably for testing the SVM
    fid = fopen(testIndFile,'w');
    for itr_im = 1:size(testX,1)
        fprintf(fid,'%d ',testY(itr_im,1));
        for itr_param = 1:size(testX,2)
            fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
        end
        fprintf(fid,'\n');
    end
    
    fclose(fid);

    % Scale the data.
    system(['./svm-scale  -r',' ', rangeFile,' ',testIndFile ' > ',testScaleFile]);

    medACC = zeros(length(C),length(gamma));

    tStart = tic;

    system(['./svm-predict  -b 0 ',testScaleFile, ' ', modelFile,' ','output.txt > dump']);

    runTimeForAllImgs = toc(tStart);

    runTimePerImg = runTimeForAllImgs/length(testY);

    fprintf('Prediction Time for 1 image and all images is %f %d respectively',runTimePerImg, runTimeForAllImgs);

    load output.txt;
    % Predicted scores
    Y_hat=output;
    acc = mean(Y_hat == testY);

    medACC(cItr, gItr) = acc;
    system('rm output.txt dump');
end