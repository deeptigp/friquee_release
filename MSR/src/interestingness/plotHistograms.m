load('Results/kFold_34567.mat');
load('Data/TestingNIter.mat');

testIndx = TestingNIter(:,1);

numImages = 2222;
GTLabels = kFold_34567.derivedGTLabels;


labels = {'pool5', 'fc6', 'fc7','layer4', 'layer3'};
numClasses = length(labels);

fig = figure;
hist(GTLabels);
set(gca,'Xtick',[1:1:numClasses],'XTickLabel',[1:numClasses])
set(gca,'XTickLabel',labels)
title('Derived ground truth labels on all images from k-fold');

print(fig,'Results/all_kFold34567','-djpeg');

%% Figure 2 %%

fig = figure;
hist(GTLabels(testIndx));
set(gca,'Xtick',[1:1:numClasses],'XTickLabel',[1:numClasses])
set(gca,'XTickLabel',labels)
title('Derived ground truth labels on test images from k-fold');

print(fig,'Results/test_kFold34567','-djpeg');

for classLabel = 1:numClasses
    load(strcat('Results/classifierResults_34567__',labels{classLabel},'.mat'));
    binaryLabels = zeros(1,numImages);
    classLabelIndxs = find(GTLabels == classLabel);
    binaryLabels(classLabelIndxs) = 1;
    fig = plotconfusion(binaryLabels(testIndx'),classifierResults.Y_hat');
    title(strcat('Confusion Matrix of ',labels{classLabel}));
    print(fig,strcat('Results/k34567_',labels{classLabel}),'-djpeg');
end

numGTPoints = [];
numTestPoints = [];
fig = figure;
for classLabel = 1:numClasses
    load(strcat('Results/classifierResults_34567__',labels{classLabel},'.mat'));
    binaryLabels = zeros(1,numImages);
    classLabelIndxs = find(GTLabels == classLabel);
    binaryLabels(classLabelIndxs) = 1;
    numGTPoints = [numGTPoints sum(binaryLabels(testIndx))];
    numTestPoints = [numTestPoints  sum(classifierResults.Y_hat)]; 
end

bar([numGTPoints'  numTestPoints'],0.3)
set(gca,'XTickLabel',labels)
print(fig,'Results/k34567_testLabels','-djpeg');

% h1 = findobj(gca,'Type','patch');
% set(h1,'FaceColor','r','EdgeColor','w');
% hold on;
% bar(numTestPoints);
% 
