function [medACC, testY, Y_hat, runTimeForAllImgs] = testSVM_fast(testX, testY, layer)
    DIRPATH = 'imNet/models/';
    load([DIRPATH,'trainedModel_',layer,'.mat']);
    tStart = tic;
    [Y_hat, medACC , ~] = svmpredict (testY, double(testX), trainedModel, '-b 0 -q');
    runTimeForAllImgs = toc(tStart)
end