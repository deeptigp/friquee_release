%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM regressor for NIter numbere of times.
%
%function [acc,prec,rec] = runBrisque(trainX,trainY,testX,testY)
function [lcc, rho] = runBrisque(trainX,trainY,testX,testY)
addpath('..');
fid = fopen('train_ind.txt','w');

for itr_im = 1:size(trainX,1)
    fprintf(fid,'%d ',trainY(itr_im,1));
    for itr_feat =1:size(trainX,2)
        fprintf(fid,'%d:%f ',itr_feat,trainX(itr_im,itr_feat));
    end
    fprintf(fid,'\n');
end

fclose(fid);

% Execute it.
system('./svm-scale -l -1 -u 1 -s range train_ind.txt > train_scale');

C = 1; gamma = 10^-7;

%% t: Kernel type. %s: svm type. g: gamma c: cost.
%  system('svm-train  -s 0 -t 2 -b 1 -q train_scale model');
%     system('svm-train  -s 3 -t 4 -c 1 -q train_scale model');
cmd = strcat('./svm-train  -t 2 -s 4 -b 1 -g',{' '},num2str(gamma),' -c',{' '},num2str(C),' -q train_scale model');
system(cmd{1})

% Construct the file ( input ) suitably for testing the SVM
fid = fopen('test_ind.txt','w');
for itr_im = 1:size(testX,1)
    fprintf(fid,'%d ',testY(itr_im,1));
    for itr_param = 1:size(testX,2)
        fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
    end
    fprintf(fid,'\n');
end

fclose(fid);
system('./svm-scale  -r range test_ind.txt > test_ind_scaled');
%system('svm-predict  -b 1 test_ind_scaled model output.txt > dump');
system('./svm-predict  -b 1 test_ind_scaled model output.txt > dump');

load output.txt;
% Predicted MOS scores
Y_hat=output;

rho = corr(Y_hat,testY,'type','Spearman');
lcc = corr(Y_hat,testY,'type','Pearson');


end