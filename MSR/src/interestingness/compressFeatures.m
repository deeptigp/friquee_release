clear;
load('Data/pool5Features.mat');
load('Data/interestingness_scores.mat');
 
feat = pool5Features;
clear pool5Features;

fN = size(feat,2);
compressType = 'CS';

corrs = [];
runTimePerImgs = [];
%% Compression
if(strcmp(compressType,'CS'))
    %diffK = [50, 200, 500, 1000, 2500, 4096, 7500, 10000, 15000, 25000, 30000 40000];
    diffKFC7 = [50, 200, 500, 1000, 2500, 4000];
    
    for K = diffKFC7
        A = zeros(K,fN);
        
        for i = 1:K
            A(i,:) = randn(1,fN);
        end

        Y = A*feat';
        Y = Y';
        
        clear A;
        
        [lcc, rho]=executeSVM1(Y,interestingness_score,1,'Data/TrainingIter.mat','Data/TestingIter.mat');
        corrs  = [corrs ; [lcc rho]];
        %runTimePerImgs =[runTimePerImgs ; runTimePerImg];
    end
else
    %% PCA
    for K = 100:200:4000
        [coeff,score,latent] = pca(friqueeALL);
        [medRHO2, medLCC2]=runBrisque_copy(score(:,1:K),MOSDay,1,'TrainingNIter50.mat','TestingNIter50.mat');
        corrFC7_2 = [corrFC7_2  ; [medRHO2 medLCC2]];
        break;
    end
end

plot(diffK, corrs(1:end-1,2),'b--o');
hold on;
scatter(fN,corrs(end,2),70,'r','filled');
set(gca,'XTickLabel',num2str(get(gca,'XTick').'));
xlabel('Dimensions of the compressed features');
ylabel('Spearman correlations');
title('The correlation scores when compressed FC7 features are used');
