clear;
load('Data/pool5Features.mat');
load('Data/fc6Features.mat');
load('Data/fc7Features.mat');

load('Data/interestingness_scores.mat');

labels = interestingness_score;

numImages = size(pool5Features,1);

trainSplit = round(0.8*numImages);

indx = randperm(numImages);
    
trainIndx = indx(1:trainSplit);
testIndx = indx(1+trainSplit:end);

testLabels = labels(testIndx);

[lcc5,rho5, pred5] = temp_linearSVR(pool5Features(trainIndx,:),labels(trainIndx)',pool5Features(testIndx,:),labels(testIndx)','fc5');

[lcc6,rho6, pred6] = temp_linearSVR(fc6Features(trainIndx,:),labels(trainIndx)',fc6Features(testIndx,:),labels(testIndx)','fc6');

[lcc7,rho7, pred7] = temp_linearSVR(fc7Features(trainIndx,:),labels(trainIndx)',fc7Features(testIndx,:),labels(testIndx)','fc7');

absDiff = [abs(pred5-testLabels')  abs(pred6-testLabels') abs(pred7-testLabels')];


% 1: stands for fc5, 2 for fc6, 3 for fc7
[minV, minInd] = min(absDiff,[],2);

fc567_run4 = struct('minInd',minInd,'rho5',rho5,'rho6',rho6,'rho7',rho7,'pred5',pred5,'pred6',pred6,'pred7',pred7);
save('Results/fc567_run4.mat','fc567_run4');

return;
testClassLabels = minInd;

%% For Training data, the class labels.
trainLabels = labels(trainIndx);

multiClass = struct('trainIndx',trainIndx, 'testIndx',testIndx,'trainLabels',trainLabels,'testLabels',testLabels,'rho5',rho5,'rho6',rho6,'rho7',rho7,'pred5',pred5,'pred6',pred6,'pred7',pred7);

[lcc5T,rho5T, runTimePerImg5, pred5] = temp_linearSVR(fc5Features(testIndx,:),labels(testIndx)',fc5Features(trainIndx,:),labels(trainIndx)');

[lcc6T,rho6T, runTimePerImg6, pred6] = temp_linearSVR(fc6Features(testIndx,:),labels(testIndx)',fc6Features(trainIndx,:),labels(trainIndx)');

[lcc7T,rho7T, runTimePerImg7, pred7] = temp_linearSVR(fc7Features(testIndx,:),labels(testIndx)',fc7Features(trainIndx,:),labels(trainIndx)');

absDiff_train = [abs(pred5-trainLabels')  abs(pred6-trainLabels') abs(pred7-trainLabels')];

% 1: stands for fc5, 2 for fc6, 3 for fc7
[minV, minInd] = min(absDiff_train,[],2);

trainClassLabels = minInd;

%% For test data, we now have the labels whether fc5, 6 or 7 is better. (1,2 or 3)
% Given fc5 features, can we predict if 1,2, or 3 is the best?

[Y_hat, runTimePerImg] = linearSVC(fc5Features(testIndx,:),testClassLabels, fc5Features(trainIndx,:),trainClassLabels);