%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7
function aggregateResults_thresh_linearC = final_perfect_cascade(trainTestSplit)
   load('Data/interestingness_scores.mat');

    labels = interestingness_score;
    
    load('Results/kFold_567_strat2_thresh0.03.mat');

    load('Data/pool5Features_compressed.mat');
    
    load('Data/fc6Features.mat');
    
    load('Data/fc7Features.mat');
    
    load('Data/TrainingNIter.mat');
    load('Data/TestingNIter.mat');
   
   % trainTestSplit = 1;
   foldIndices = kFold_567.foldIndices;
 
    testIndx = find(foldIndices == trainTestSplit);
    trainIndx = find(foldIndices ~= trainTestSplit);
   
    %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);
    
    cascadeClassifierResults_aggre567_thresh_linearC_pool5 = struct;
    cascadeClassifierResults_aggre567_thresh_linearC_fc6 = struct;
    cascadeClassifierResults_aggre567_thresh_linearC_fc7 = struct;
    
    % Generate a random train/test split.
    numImages = length(labels);

    % Get the derived class labels.
    GTLabels = kFold_567.strategy2DerivedLabels;
    
    %% Get the label indexes.
    class1LabelIndxs = find(GTLabels== 1);
    class2LabelIndxs = find(GTLabels== 2);
    class3LabelIndxs = find(GTLabels== 3);

    binaryLabels1 = zeros(1,numImages);
    binaryLabels2 = zeros(1,numImages);
    binaryLabels3 = zeros(1,numImages);
    
    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1;
    binaryLabels2(class2LabelIndxs) = 1;
    binaryLabels3(class3LabelIndxs) = 1;

    
    %% train with fc5, those that say no-fc5, test them with fc6.
    cascadeClassifierResults_aggre567_thresh_linearC_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_thresh_linearC_pool5.testIndx = testIndx;
    cascadeClassifierResults_aggre567_thresh_linearC_pool5.Y_hat = binaryLabels1(testIndx)';
    cascadeClassifierResults_aggre567_thresh_linearC_pool5.runTimeForAllImgs = 0;
    
    
    cascadePredictedResults_aggre567_thresh_linearC_pool5 = evaluateClassifier(cascadeClassifierResults_aggre567_thresh_linearC_pool5, 'pool5');
    
    %% Aggregating the predictions of the data points that were predicted to belong to pool5 class..
    aggregatePredictedScores(cascadePredictedResults_aggre567_thresh_linearC_pool5.class1Indices) = cascadePredictedResults_aggre567_thresh_linearC_pool5.ePred5;
    
    
    %% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_thresh_linearC_pool5.testIndx(cascadeClassifierResults_aggre567_thresh_linearC_pool5.Y_hat==0); 
    
    cascadeClassifierResults_aggre567_thresh_linearC_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_thresh_linearC_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_thresh_linearC_fc6.Y_hat = binaryLabels2(notFC5Indxs)';
    cascadeClassifierResults_aggre567_thresh_linearC_fc6.runTimeForAllImgs = 0;
    
    cascadePredictedResults_aggre567_thresh_linearC_fc6 = evaluateClassifier(cascadeClassifierResults_aggre567_thresh_linearC_fc6, 'fc6');
    
    aggregatePredictedScores(cascadePredictedResults_aggre567_thresh_linearC_fc6.class1Indices) = cascadePredictedResults_aggre567_thresh_linearC_fc6.ePred6;
    
    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre567_thresh_linearC_fc6.testIndx(cascadeClassifierResults_aggre567_thresh_linearC_fc6.Y_hat==0); 
    Y_hat = ones(size(notFC6Indxs));
    
    cascadeClassifierResults_aggre567_thresh_linearC_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_thresh_linearC_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre567_thresh_linearC_fc7.Y_hat = Y_hat;
    
   
    cascadePredictedResults_aggre567_thresh_linearC_fc7 = evaluateClassifier(cascadeClassifierResults_aggre567_thresh_linearC_fc7, 'fc7');
    
    aggregatePredictedScores(cascadePredictedResults_aggre567_thresh_linearC_fc7.class1Indices) = cascadePredictedResults_aggre567_thresh_linearC_fc7.ePred7;
   
    
    nonZeroPredictedScores = aggregatePredictedScores(testIndx);
    aggreSROCC = corr(nonZeroPredictedScores,labels(testIndx)','type','Spearman');
    aggrePLCC = corr(nonZeroPredictedScores,labels(testIndx)');
    
    aggregateResults_thresh_linearC.predictedScores = aggregatePredictedScores;
    aggregateResults_thresh_linearC.aggreSROCC = aggreSROCC;
    aggregateResults_thresh_linearC.aggrePLCC = aggrePLCC;
 
    temp_runningTimes;
    aggregateResults_thresh_linearC.totalTestTime = totalTestTime;
    aggregateResults_thresh_linearC.totalClassificationTime = totalClassificationTime;
    aggregateResults_thresh_linearC.totalFeatureComputeTime = totalFeatureComputeTime;
    aggregateResults_thresh_linearC.totalRegressionTime = totalRegressionTime;

 %   save(strcat('Results/aggregateResults/interestingness/',num2str(trainTestSplit),'/aggregatePredictedScores_thresh_fc567.mat'),'aggregateResults_thresh_linearC');
    
  %  fprintf('Spearman and Pearson correlation between the aggregated predictions and GT predictions %f %f\n',aggreSROCC,aggrePLCC);
    