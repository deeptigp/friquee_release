%% This method is the right way to assign class labels.. We start with the assumption that fc5 is enough for all the data points.. We then take the data points where fc5 might not be the right label and test if fc6 is better.

toPlot = 1;
thresh = 0.03;
load('Results/kFold_567.mat');
SAVEPATH = 'Results/plots/';


absDiff = kFold_567.absDiffAll;
editedDerivedLabels  = kFold_567.derivedGTLabels;

%% Find all the images where absDiff(fc5) <=0.03 
fc5Indexes = find(absDiff(:,1) <= thresh);

%% Find all the images where absDiff(fc5) > 0.03 
notFc5Indexes = find(absDiff(:,1) > thresh);



%% Find all the images for which fc5 was chosen as the label.
%Global indexes
indxLayer7 = find(kFold_567.derivedGTLabels == 3);

%% Compute how different the absDiffs were for fc6 and pool5 for these labels.

fc5Diff_fc7 = abs(absDiff(indxLayer7,3)-absDiff(indxLayer7,1));

fc6Diff_fc7 = abs(absDiff(indxLayer7,3)-absDiff(indxLayer7,2));


%% For all those where fc7 was chosen, check if pool5 can be assigned, and do so if allowed.
changeFC7ToFC5Indxs = indxLayer7(fc5Diff_fc7 <= thresh );
length(changeFC7ToFC5Indxs)
editedDerivedLabels(changeFC7ToFC5Indxs) = 1;

%%Check between fc7 and fc6.
checkFC7ToFC6Indxs = indxLayer7(fc6Diff_fc7 <= thresh );

remainingIndxs = setdiff(checkFC7ToFC6Indxs,changeFC7ToFC5Indxs);

length(remainingIndxs)
editedDerivedLabels(remainingIndxs) = 2;


if(toPlot == 1)
    fig1 = figure;
    hist([fc6Diff_fc7 fc5Diff_fc7]);
    legend('fc6','fc7');
    title('Distribution of how much fc7 and fc6s predictions differ from pool5s');
    print(fig1, strcat(SAVEPATH,'diffOfDiff5_67_kFold567'),'-djpeg');
end

%% === %% 
indxLayer6 = find(editedDerivedLabels == 2);

%% Compute how different the absDiffs were for fc7 for these labels.
fc6Diff_fc5 = abs(absDiff(indxLayer6,1)-absDiff(indxLayer6,2));

changeFC6ToFC5Indxs = indxLayer6(fc6Diff_fc5 <= thresh );
length(changeFC6ToFC5Indxs)
editedDerivedLabels(changeFC6ToFC5Indxs) = 1;


if(toPlot == 1)
    fig2 = figure;
    hist(fc6Diff_fc5,30);
    xlabel('|fc6Diff - fc5Diff|')
    ylabel('Count');
    title('Distribution of how much fc6 predictions differ from fc5s');
    print(fig2, strcat(SAVEPATH,'diffOfDiff6_5_kFold567'),'-djpeg');
end

kFold_567.editedDerivedLabels = editedDerivedLabels;

save('Results/kFold_567_thresh_down0.03.mat','kFold_567');