%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM regressor for NIter numbere of times.
function [medLCC, medRHO, Y_hat, rmse, psnrVal, runTimeForAllImgs, runTimePerImg] = temp_linearSVR(trainX, trainY, testX, testY,layer)    
     C = 0.25; %2^1;
     gamma = 2^-9;
    
     %% For the purpose of testing.. REMOVE THIS
%      C = [];
%      for i = -5:1:5
%          C= [C 2^i];
%      end
%    
    if(strcmp(layer,'layer4') || strcmp(layer,'layer3'))
         gamma = 2^-12;
     end
     
    medLCC = zeros(length(C),length(gamma));
    medRHO = zeros(length(C),length(gamma));

    runTimeForAllImgs = zeros(length(C),length(gamma));
    
    fid = fopen('train_ind11.txt','w');

    for itr_im = 1:size(trainX,1)
        fprintf(fid,'%d ',trainY(itr_im,1));
        for itr_feat =1:size(trainX,2)
            fprintf(fid,'%d:%f ',itr_feat,trainX(itr_im,itr_feat));
        end
        fprintf(fid,'\n');
    end

    fclose(fid);

    % Execute it.
    system('./svm-scale -l -1 -u 1 -s range11 train_ind11.txt > train_scale11');

    % Construct the file ( input ) suitably for testing the SVM
    fid = fopen('test_ind11.txt','w');
    for itr_im = 1:size(testX,1)
        fprintf(fid,'%d ',testY(itr_im,1));
        for itr_param = 1:size(testX,2)
            fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);


    % Scale the data.
    system('./svm-scale  -r range11 test_ind11.txt > test_ind11_scaled');

    %% t: Kernel type. %s: svm type. g: gamma c: cost.

    for cItr = 1:length(C)
        for gItr = 1:length(gamma)
            cmd = strcat('./svm-train  -t 2 -s 4 -b 1 -g',{' '},num2str(gamma(gItr)),' -c',{' '},num2str(C(cItr)),' -q train_scale11 model11');
            
            cmd{1}
            
            system(cmd{1});

            tStart = tic;

            system('./svm-predict  -b 1 test_ind11_scaled model11 output11.txt > dump11');

            testingTimeForAllImgs = toc(tStart)

            runTimePerImg = testingTimeForAllImgs/length(testY)

            fprintf('Prediction Time for 1 image and all images is %f %d respectively',runTimePerImg, testingTimeForAllImgs);

            load output11.txt;
            % Predicted scores
            Y_hat=output11;

            rho = corr(Y_hat,testY,'type','Spearman');
            lcc = corr(Y_hat,testY,'type','Pearson');
            medLCC(cItr,gItr) = lcc;
            medRHO(cItr,gItr) = rho;
            runTimeForAllImgs(cItr, gItr) = testingTimeForAllImgs;
            
            system('rm output11.txt dump11 model11');
        end
    end
    
    rmse  = sqrt(mean((testY - Y_hat).^2));
    psnrVal = psnr(testY,Y_hat);
    
    system('rm train_scale11 range11 test_ind11.txt train_ind11.txt test_ind11_scaled');
end