%% COPY FILES TO SEPARATE DIRS
[trainlist, testlist, trainLabels, testLabels, labelSet] = setupTrainTestSets('dataset', '99calories');

list.train = trainlist;
list.test = testlist;

dataDir = '\\msr-arrays\Scratch\msr-pool\Matlab\t-oscb\data\testdata\IQAinput';
tt = {'train', 'test'};

for ttItt = 1 : 2
    thisDir = fullfile(dataDir,  tt{ttItt});
    mkdir(thisDir)
    for fileItt = 1 : numel(list.(tt{ttItt}));
        [PATHSTR,NAME,EXT] = fileparts(list.(tt{ttItt}){fileItt});
        copyfile(list.(tt{ttItt}){fileItt}, fullfile(thisDir, [NAME, EXT]));
    end
    
    
end
%%
program = 'C:\code\external\IQAMetric\IQAMetric.exe';
outDir = '\\msr-arrays\Scratch\msr-pool\Matlab\t-oscb\data\testdata\IAQout';
features = {'pyrHist1D', 'pyrHist2D', 'pyrStat1D', 'pyrCorr2D', 'anis', 'twoColor', 'patchPCA'};

for featItt = 1 : numel(features)
    for ttItt = 1 : 2
        system(sprintf('%s -i %s -o %s -f %s', program, ...
            fullfile(dataDir,  tt{ttItt}), fullfile(outDir,  tt{ttItt}), features{featItt}));
    end
end





%%
file = '\\msr-arrays\Scratch\msr-pool\Matlab\t-oscb\data\neeltest\639.jpg';
system(sprintf('%s -e pyrHist1D', program, file));