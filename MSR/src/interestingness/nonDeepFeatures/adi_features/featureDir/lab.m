load('\\msr-arrays\Scratch\msr-pool\Matlab\t-oscb\data\testdata\2013-08-30\st1\data.mat');
load('\\msr-arrays\Scratch\msr-pool\Matlab\t-oscb\data\testdata\2013-08-30\st1\config.mat');

[solverStruct, errorStruct, gridParams] = solverParams('liblin');
gridParams.edgeLength = 1;
gridParams.start = 1;

trainData.features = sparse(load_features('lbp', 'train', c, '99eatsbig'));
trainData.labels = st.trainLabels;
trainData = mapToIntegerLabels(trainData);
trainData.fromfile = (1:numel(trainData.labels))';

[res, meta] = trainSVMlocalCV(trainData, 3, gridParams, errorStruct, solverStruct);