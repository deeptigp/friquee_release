function [trainlists, testlists, names] = featureSetup(varargin)
error('this file is deprectated');

addpath(genpath(fullfile(getdirs('coderoot'), 'external', 'features')));
addpath(genpath(fullfile(getdirs('coderoot'), 'external', 'filecentral')));
addpath(genpath(fullfile(getdirs('coderoot'), 'external', 'beijbomLib')));
addpath(genpath(fullfile(getdirs('coderoot'), '99eats')));

debug = false;
[varnames, varvals] = var2varvallist(debug);
[debug] = varargin_helper(varnames, varvals, varargin{:});


names = {'99eats'};

files = struct2cell(rdir(fullfile(getdirs('dataroot'), '\99eats\organized\*.jpg')));
files = files(1,:);
trainlists = {files};
testlists = {files(1)};

if(debug)
    trainlists{1} = trainlists{1}(1:5);
end

end