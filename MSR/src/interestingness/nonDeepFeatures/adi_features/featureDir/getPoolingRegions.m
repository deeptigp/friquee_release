function pools = getPoolingRegions(method, mode, input, pyramidLevels)
% pools = getPoolingRegions(method, type, input, pyramidLevels)
% 
% return pooling areas for INPUT input
% INPUT pyramidLevels is an iteger indicating the nbr of pools
% INPUT method is 'rotInv', 'traditional', 'landscape'
% getPoolingRegions operate in two modes, 'sparse', and 'dense'.
%
% === 'sparse' ===
% INPUT input should be a struct with 
% input.x: column indices of feature points
% input.y: row indices of feature points
% input.wid: max width
% input.hgt: max height
% OUTPUT pools is a binary matrix where each column contain index of point
% that should be pooled together. 
%
% === 'dense' ===
% INPUT input is simply an image (graylevel or RGB)
% OUPUT pools is a cell array of cropped images
%
%

switch mode
    
    case 'dense'
        I = input;
        imageInfo.wid = size(I, 2);
        imageInfo.hgt = size(I, 1);
        pools = cell(pyramidLevels, 1);
    case 'sparse'
        imageInfo = input;
        pools = false(numel(imageInfo.x), pyramidLevels);
        
end

switch method
    
    case 'rotInv'
        z = min(imageInfo.wid, imageInfo.hgt);
        
        zs = [inf z ./ (1.75).^(1 : pyramidLevels)]; %edge lengths of the pooling area. The first is the whole image.
        
        center = round([imageInfo.wid imageInfo.hgt] / 2);
        for poolItt = 1 : pyramidLevels + 1
            
            switch mode
                case 'sparse'
                    % figure out which feature data lies within the integration area
                    pools(:, poolItt) =    imageInfo.x > center(1) - zs(poolItt) & ...
                        imageInfo.x < center(1) + zs(poolItt) & ...
                        imageInfo.y > center(2) - zs(poolItt) & ...
                        imageInfo.y < center(2) + zs(poolItt);
                case 'dense'
                    pools{poolItt} = imcrop(I, round([ ...   
                        center(1) - zs(poolItt), ...
                        center(2) - zs(poolItt), ...
                        2 * zs(poolItt), ...
                        2 * zs(poolItt)]));
            end
            
        end
        
        
    case 'traditional'
        
        
    case 'landscape'
        
        
        
        
        
end