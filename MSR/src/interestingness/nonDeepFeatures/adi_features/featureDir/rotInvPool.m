function beta = rotInvPool(this_feat, imageInfo, pyramidLevels, encode_method, dictionary)

z = min(imageInfo.wid, imageInfo.hgt);

zs = [inf z ./ 1.5.^(1 : pyramidLevels)]; %edge lengths of the pooling area. The first is the whole image.

center = round([imageInfo.wid imageInfo.hgt] / 2);

% encode whole image to get the dimensions right
switch encode_method
    case 'llc'
        nwords = size(dictionary, 1);
    case 'fisher_mmm'
        nwords = numel(fisherEncode('mmm', dictionary, this_feat));
    case 'fisher_gmm'
        nwords = numel(fisherEncode('gmm', dictionary, this_feat));
end

beta = zeros(1, (pyramidLevels + 1) * nwords);

for poolItt = 1 : pyramidLevels + 1
    
    % figure out which feature data lies within the integration area
    ok =    imageInfo.x > center(1) - zs(poolItt) & ...
        imageInfo.x < center(1) + zs(poolItt) & ...
        imageInfo.y > center(2) - zs(poolItt) & ...
        imageInfo.y < center(2) + zs(poolItt);
    
    betaInd = ((poolItt - 1) * nwords) + 1 : poolItt * nwords;
    
    switch encode_method
        case 'llc'
            code = sparse(LLC_coding_appr(dictionary, this_feat(ok, :), p.llcknn));
            beta(betaInd) = full(max(code(ok,:))); % maxpooling...
        case 'fisher_mmm'
            beta(betaInd) = fisherEncode('mmm', dictionary, this_feat(ok, :));
        case 'fisher_gmm'
            beta(betaInd) = fisherEncode('gmm', dictionary, this_feat(ok, :));
    end
    
end
beta = beta ./ sqrt(sum(beta.^2, 2)); % normalize...
end
