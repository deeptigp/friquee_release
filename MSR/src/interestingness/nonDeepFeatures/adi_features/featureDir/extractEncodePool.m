function pooledFeatures = extractEncodePool(img, feature, c)

p = c.feature_config.(feature);
dictionary = p.dictionary;

[this_feat, info.x, info.y, info.wid, info.hgt] = extract_feature(feature, img, c);

%%% GET POOLING REGIONS
pools = getPoolingRegions(p.pooling_method, 'sparse', info, p.pyramid_levels);

%%% FIGURE OUT DIMENSIONALITY OF OUTPUT CODES
switch p.encode_method
    case 'llc'
        nwords = size(dictionary, 1);
    case 'fisher_mmm'
        nwords = numel(fisherEncode('mmm', dictionary, this_feat));
    case 'fisher_gmm'
        nwords = numel(fisherEncode('gmm', dictionary, this_feat));
    case 'fisher_gmm-white'
        nwords = numel(fisherEncode('gmm-white', dictionary, this_feat));
end

%%% DO ENCODING AND POOLING
pooledFeatures = zeros(1, size(pools, 2) * nwords);
for poolItt = 1 : size(pools, 2)
    
    poolInd = ((poolItt - 1) * nwords) + 1 : poolItt * nwords;
    
    switch p.encode_method
        case 'llc'
            code = sparse(LLC_coding_appr(dictionary, this_feat(pools(:, poolItt), :), p.llcknn));
            pooledFeatures(poolInd) = full(max(code)); % maxpooling...
        case 'fisher_mmm'
            pooledFeatures(poolInd) = fisherEncode('mmm', dictionary, this_feat(pools(:, poolItt), :));
        case 'fisher_gmm'
            pooledFeatures(poolInd) = fisherEncode('gmm', dictionary, this_feat(pools(:, poolItt), :));
        case 'fisher_gmm-white'
            pooledFeatures(poolInd) = fisherEncode('gmm-white', dictionary, this_feat(pools(:, poolItt), :));
    end
    
end

pooledFeatures = pooledFeatures ./ sqrt(sum(pooledFeatures.^2, 2)); % normalize...

end