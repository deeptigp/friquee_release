function beta = lbp_feature_rotInv(I, pyramidLevels)

imageInfo.wid = size(I, 2);
imageInfo.hgt = size(I, 1);

z = min(imageInfo.wid, imageInfo.hgt);

zs = [inf z ./ 1.75.^(1 : pyramidLevels)]; %edge lengths of the pooling area. The first is the whole image.

center = round([imageInfo.wid imageInfo.hgt] / 2);

nwords = 59;
beta = zeros(1, (pyramidLevels + 1) * nwords);
mapping=getmapping(8,'u2');
for poolItt = 1 : pyramidLevels + 1
    
    Icrop = imcrop(I, round([   center(1) - zs(poolItt), ...
        center(2) - zs(poolItt), ...
        2 * zs(poolItt), ...
        2 * zs(poolItt)]));
    
    betaInd = ((poolItt - 1) * nwords) + 1 : poolItt * nwords;
    
    beta(betaInd) = lbp(Icrop,1,8,mapping,'h');
    beta(betaInd) = beta(betaInd) ./ sqrt(sum(beta(betaInd).^2)); % L2 normalize...
end



end