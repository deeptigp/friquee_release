function F = makeRFSfilters(stds, oriented, SUP)
% Returns the RFS filter bank of size 49x49x38 in F. The MR8, MR4 and
% MRS4 sets are all derived from this filter bank. To convolve an
% image I with the filter bank you can either use the matlab function
% conv2, i.e. responses(:,:,i)=conv2(I,F(:,:,i),'valid'), or use the
% Fourier transform.

         
SCALEX = stds(oriented == 1); % Sigma_{x} for the oriented filters
SCALEX_nonoriented = stds(oriented == 0); 

NORIENT = 6;              % Number of orientations
if nargin < 3
    SUP = max(stds) * 5;         % Support of the largest filter (must be odd) def 49
end

if(mod(SUP,2) == 0) %make sure its an odd number.
    SUP = SUP + 1;
end


NROTINV = sum(oriented == 0);
NBAR=length(SCALEX)*NORIENT;
NEDGE=length(SCALEX)*NORIENT;
NF=NBAR+NEDGE+NROTINV;
F=zeros(SUP,SUP,NF);
hsup=(SUP-1)/2;
[x,y]=meshgrid([-hsup:hsup],[hsup:-1:-hsup]);
orgpts=[x(:) y(:)]';

count=1;
for scale=1:length(SCALEX),
    for orient=0:NORIENT-1,
        angle=pi*orient/NORIENT;  % Not 2pi as filters have symmetry
        c=cos(angle);s=sin(angle);
        rotpts=[c -s;s c]*orgpts;
        F(:,:,count)=makefilter(SCALEX(scale),0,1,rotpts,SUP);
        F(:,:,count+NEDGE)=makefilter(SCALEX(scale),0,2,rotpts,SUP);
        count=count+1;
    end;
end;

NROTINV = sum(oriented == 0);

for i = 0 : NROTINV - 1

    F(:,:,NBAR+NEDGE+i*2+1)=normalise(fspecial('gaussian',SUP, SCALEX_nonoriented(i+1)));
    F(:,:,NBAR+NEDGE+i*2+2)=normalise(fspecial('log',SUP, SCALEX_nonoriented(i+1)));
    
end

return

function f=makefilter(scale,phasex,phasey,pts,sup)
gx=gauss1d(3*scale,0,pts(1,:),phasex);
gy=gauss1d(scale,0,pts(2,:),phasey);
f=normalise(reshape(gx.*gy,sup,sup));
return

function g=gauss1d(sigma,mean,x,ord)
% Function to compute gaussian derivatives of order 0 <= ord < 3
% evaluated at x.

x=x-mean;num=x.*x;
variance=sigma^2;
denom=2*variance;
g=exp(-num/denom)/sqrt(pi*denom);
switch ord,
    case 1, g=-g.*(x/variance);
    case 2, g=g.*((num-variance)/(variance^2));
end;
return

function f=normalise(f), f=f-mean(f(:)); f=f/sum(abs(f(:))); return