function [p] = config_color(c)

p.grid_spacing = 4; % distance between grid centers
p.patch_sizes = [6 8 10 12 14 16];


p.pyramid_levels = 4;
p.llcknn = 3;
p.maxsize = 500;
p.pooling_method = 'rotInv';

% dictionary parameters
p.dictionary_size = 1024;
p.num_images = 2000;
p.descPerImage = 1000;
p.num_desc = 5e5;

p.train_file = '%s/train_color_%d.mat';
p.test_file = '%s/test_color_%d.mat';
p.dictionary_file = '%s/dictionary_color_%s_%d.mat';

tmp = load('DD50_w2c');
p.w2c = tmp.w2c;
p.num_colors = size(p.w2c, 2);

% normalization settings
p.hellingerNormalize = 0;
p.l1normalize = 1;
p.dictionary_method = 'kmeans';
p.encode_method = 'llc';

end
