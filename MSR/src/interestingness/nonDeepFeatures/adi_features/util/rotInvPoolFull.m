function beta = rotInvPoolFull(I, pyramidLevels, nwords)
% this function does mean pooling using a rotational invariant spatial
% pyramid.

imageInfo.wid = size(I, 2);
imageInfo.hgt = size(I, 1);

z = min(imageInfo.wid, imageInfo.hgt);

zs = [inf z ./ 1.5.^(1 : pyramidLevels)]; %edge lengths of the pooling area. The first is the whole image.

center = round([imageInfo.wid imageInfo.hgt] / 2);

beta = zeros(1, (pyramidLevels + 1) * nwords);

for poolItt = 1 : pyramidLevels + 1
    
    Icrop = imcrop(I, round([   center(1) - zs(poolItt), ...
        center(2) - zs(poolItt), ...
        2 * zs(poolItt), ...
        2 * zs(poolItt)]));
    
    betaInd = ((poolItt - 1) * nwords) + 1 : poolItt * nwords;
    
    beta(betaInd) = hist(Icrop(:), 1 : nwords);
    beta(betaInd) = beta(betaInd) ./ sum(beta(betaInd)); % normalize...

end

end