function [] = openPool(s)
if(matlabpool('size')==0)
    if(s==0)
        matlabpool;
    elseif(s>1)
        feval('matlabpool', 'SCR_DCS_Cluster', s);
    else
        matlabpool(s);
    end
    
end
