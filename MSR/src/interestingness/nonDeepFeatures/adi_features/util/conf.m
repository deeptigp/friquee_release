function [c] = conf(cache_folder)
if(~exist('cache_folder', 'var'))
  c.cache = 'cache/';
else
  c.cache = [cache_folder '/'];
end

c.batch_size = 500;

c.cores = 20;
c.feature_list = {'color', 'lbp', 'mr8'};
c.dirName = 'C:\ImageData\MemorabilityImages\';

for i=1:length(c.feature_list)
	feat = c.feature_list{i};
	c.feature_config.(feat) = [];
	if(exist(['config_' feat]))
  	c.feature_config.(feat) = feval(['config_' feat], c);
	end
end

stream = RandStream('mt19937ar','Seed', sum(100*clock));
RandStream.setGlobalStream(stream);
