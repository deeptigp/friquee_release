function [p] = config_lbp(c)
p.feature_size = 59*(1+4+16);
p.maxsize = 500;
p.pooling_method = 'rotInv';
p.pyramid_levels = 5;
p.train_file = ['%s/train_lbp_' num2str(p.feature_size) '.mat'];
p.test_file = ['%s/test_lbp_' num2str(p.feature_size) '.mat'];

p.hellingerNormalize = 0;
p.l1normalize = 1;
p.encode_method = 'llc';