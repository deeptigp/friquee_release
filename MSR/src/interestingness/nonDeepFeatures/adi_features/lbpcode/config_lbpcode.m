function [p] = config_lbpcode(c)

p.grid_spacing = 8; % distance between grid centers
p.patch_sizes = 16;
p.imgscales = 1;

p.pyramid_levels = 4;
p.llcknn = 3;
p.maxsize = 500;
p.pooling_method = 'rotInv';

% dictionary parameters
p.dictionary_size = 1024;
p.num_images = 2000;
p.descPerImage = 1000;
p.num_desc = 5e5;

p.train_file = '%s/train_lbpcode_%d.mat';
p.test_file = '%s/test_lbpcode_%d.mat';
p.dictionary_file = '%s/dictionary_lbpcode_%s_%d.mat';

p.feature_size = 59;

% normalization settings
p.hellingerNormalize = 0;
p.l1normalize = 1;
p.dictionary_method = 'kmeans';
p.encode_method = 'llc';