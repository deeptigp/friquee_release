function [feat, x, y, wid, hgt] = extract_lbpcode(img, c)
if(~exist('c', 'var'))
    c = conf();
end

featname = 'lbpcode';
p = c.feature_config.(featname);

if(size(img, 3)==2)
    img = rgb2gray(img);
end


x = cell(numel(p.imgscales) * numel(p.patch_sizes), 1);
y = cell(numel(p.imgscales) * numel(p.patch_sizes), 1);
feat = cell(numel(p.imgscales) * numel(p.patch_sizes), 1);

for scaleItt = 1 : numel(p.imgscales)
    img_resize = imresize(img, p.imgscales(scaleItt));
    img_lbp = lbp(img_resize,1,8,getmapping(8,'u2'),'dummyinput');
    [hgt, wid, ~] = size(img_lbp);
    for i = 1 : numel(p.patch_sizes)
        index = (scaleItt - 1) * numel(p.patch_sizes) + i;
        patch_size = p.patch_sizes(i);
        numPixelsPerPatch = patch_size.^2;
        grid_spacing = max(1, round(p.grid_spacing * p.imgscales(scaleItt)));
        [x{index}, y{index}, gridX, gridY] = create_grid(hgt, wid, grid_spacing, patch_size);
        
        feat{index} = cell2mat(arrayfun(@(x, y) histc(reshape(img_lbp(y:y+patch_size-1, x:x+patch_size-1), [numPixelsPerPatch 1]), 0:p.feature_size-1), gridX(:), gridY(:), 'UniformOutput', false)')';
        feat{index} = feat{index} ./ patch_size^2;
        
        x{index} = round(x{index} / p.imgscales(scaleItt)); %rescale to original scale!!
        y{index} = round(y{index} / p.imgscales(scaleItt)); %rescale to original scale!!
    end
    
end

x = cell2mat(x(:));
y = cell2mat(y(:));
feat = cell2mat(feat);

if p.l1normalize
    feat = feat ./ repmat(sum(feat, 2), 1, size(feat, 2));
end

if p.hellingerNormalize
    feat = feat ./ repmat(sum(feat, 2), 1, size(feat, 2));
    feat = feat.^(.5);
end

end
