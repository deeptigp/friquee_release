function [p] = config_color(c)
p.grid_spacing = 4; % distance between grid centers

p.pyramid_levels = 5;
p.llcknn = 3;
p.maxsize = 500;
p.rotInvPool = 1;

% dictionary parameters
p.train_file = '%s/train_ddcolor_%d.mat';
p.test_file = '%s/test_ddcolor_%d.mat';

tmp = load('DD50_w2c_fast.mat');
p.w2c = tmp.w2c;
p.num_colors = size(p.w2c, 2);

p.feature_size = 50*(p.pyramid_levels+1);


end