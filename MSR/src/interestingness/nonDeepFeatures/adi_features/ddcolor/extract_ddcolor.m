function [feat, x, y, wid, hgt] = extract_ddcolor(img, c)

featname = 'ddcolor';
p = c.feature_config.(featname);

img = double(img);
index_px = floor(img(:,:,1)/8)+32*floor(img(:,:,2)/8)+32*32*floor(img(:,:,3)/8) + 1;

img_encoded = p.w2c(index_px);

feat = rotInvPoolFull(img_encoded, p.pyramid_levels, 50);

x = []; y=[]; wid=[]; hgt=[];

end