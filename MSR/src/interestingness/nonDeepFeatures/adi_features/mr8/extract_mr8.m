function [feat, x, y, wid, hgt] = extract_mr8(img, c)

if(~exist('c', 'var'))
    c = conf();
end

feature = 'mr8';
p = c.feature_config.(feature);

if(size(img, 3)==3)
    img = rgb2gray(img);
end

x = cell(numel(p.imgscales), 1);
y = cell(numel(p.imgscales), 1);
feat = cell(numel(p.imgscales), 1);

for scaleItt = 1 : numel(p.imgscales)
    img_resize = imresize(img, p.imgscales(scaleItt));
    FIC = applyMaximumResponseFilterBank(img_resize, p.filterMeta);
    [hgt, wid, ~] = size(FIC);
    grid_spacing = max(1, round(p.grid_spacing * p.imgscales(scaleItt)));
    [gridX, gridY] = meshgrid(1:grid_spacing:wid, 1:grid_spacing:hgt);

    feat{scaleItt} = cell2mat(arrayfun(@(ty, tx) reshape(FIC(ty,tx, :), 1, 10), gridY(:), gridX(:), 'UniformOutput', 0 ));
    
    x{scaleItt} = round(gridX(:) / p.imgscales(scaleItt)); %rescale to original scale!!
    y{scaleItt} = round(gridY(:) / p.imgscales(scaleItt)); %rescale to original scale!!
    
end

x = cell2mat(x);
y = cell2mat(y);
feat = cell2mat(feat);

end


