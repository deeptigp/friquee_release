function p = config_mr8(dummy)

filterParams.stds = [1 2 4 8 3];
filterParams.oriented = [1 1 1 1 0];

p.filterMeta = getFilterMeta(filterParams);

p.imgscales = 1;
p.grid_spacing = 2; % distance between grid centers
p.pyramid_levels = 5;
p.llcknn = 3;
p.maxsize = 500;
p.pooling_method = 'rotInv';

% dictionary parameters
p.dictionary_size = 1024;
p.num_images = 2000;
p.descPerImage = 1000;
p.num_desc = 5e5;

p.train_file = '%s/train_mr8_%d.mat';
p.test_file = '%s/test_mr8_%d.mat';
p.dictionary_file = '%s/dictionary_mr8_%s_%d.mat';
p.hellingerNormalize = 0;
p.l1normalize = 1;
p.dictionary_method = 'kmeans';
p.encode_method = 'llc';


end


function filterMeta = getFilterMeta(filterParams)

F = makeRFSfilters(filterParams.stds, filterParams.oriented, 49);

filterMeta.Fids = [];
for i = 1 : sum(filterParams.oriented)  * 2
    filterMeta.Fids = [filterMeta.Fids ; ones(6,1) * i];
end
for i = sum(filterParams.oriented)  * 2 + 1: length(filterParams.oriented) * 2
    filterMeta.Fids = [filterMeta.Fids ; ones(1,1) * i];
end
filterMeta.nbrOriented = sum(filterParams.oriented);
filterMeta.nbrCircular = sum(~filterParams.oriented);

% Fcolstack = zeros(size(F, 3), 49^2);
% for i = 1 : size(F, 3)
%     Fcolstack(i, :) = reshape(F(:,:,i), numel(F(:, :, i)), 1)';
% end
filterMeta.F = F;


end