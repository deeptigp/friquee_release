function [descriptor A] = encodeWithDictionary(samples, D, encodeParams)

A = [];
switch encodeParams.method
    
    case 'none'
        descriptor = rowVector(samples(:));
        
    case 'second_moment'
        descriptor(1, 1) = mean(samples(:)); 
        descriptor(1, 2) = std(samples(:)); 
    
    case 'first_moment'
        descriptor = mean(samples);
    
    case 'fisher'
%         handle = mexFisherEncodeHelper('init', gmm_mat, fisher_params);
%         FV = mexFisherEncodeHelper('encode', handle, x, <weights>);
%         mexFisherEncodeHelper('clear', handle);
        encoder = FisherEncoder(D.model, encodeParams.params);
        descriptor = encoder.encode(single(samples'));
        encoder.delete();
        
    case 'sc'
        A = (mexLasso(samples', D', encodeParams.scParams))';
        A = [max(A, 0) max(-A, 0)];
        descriptor = max(A,[], 1);
        
    case 'nonlin-fixed'
        A = (D * samples')';
        A = [max(A - encodeParams.alpha, 0) max(-A - encodeParams.alpha, 0)];
        descriptor = max(A,[], 1);
        
    case 'sqeuclidean'
         
        A = zeros(size(samples, 1), 1);
        
        % divide in 100 parts
        P = unique(round(linspace(0, size(samples, 1), 100)));
        
        for i = 2 : length(P)
            thisChunk = samples(P(i-1) + 1 : P(i), :);
            [A(P(i-1) + 1 : P(i), :), ~] = vgg_nearest_neighbour(thisChunk', D');
        end
        
        descriptor = (hist(A, 1 : size(D, 1)) ./ numel(A))';
        
    case 'svmrbf'
        dummyLabels = ones(size(samples, 1), 1);
        A = svmpredict(dummyLabels, samples, D.model);
        A = (hist(A, 1 : D.model.nr_class) ./ numel(A))';
        
    case 'svmlin'
        A = predict(ones(size(samples, 1), 1), sparse(samples), D.model);
        A = (hist(A, 1 : D.model.nr_class) ./ numel(A))';
        
    case 'svmlin_sumdecvals'
        [~, decval] = linsvmClassify(D, samples);
        A = sum(decval, 1);
        
    case 'svmlin_sumdecvals_sigmoid'
        [~, decval] = linsvmClassify(D, samples);
        s = sigmoid(decval, encodeParams.params.scale, encodeParams.params.bias);
        A = sum(s, 1);
        
    case 'svmlin_sumdecvals_threshhold'
        [~, decval] = linsvmClassify(D, samples);
        s = max(0, decval - encodeParams.params.alpha);
        A = sum(s, 1);
        
        
        
end

end