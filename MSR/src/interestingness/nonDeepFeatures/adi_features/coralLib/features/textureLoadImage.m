function Icell = textureLoadImage(imPath, featParams)

Iin = imread(imPath);
Iin = coralPreProcess(Iin, featParams.preprocess);
Iin = coralSetColor(Iin, featParams.color);
Iin = imresize(Iin, 1/2^(featParams.scale - 1));

Icell = cell(1, size(Iin, 3));
for color = 1 : size(Iin, 3) % for each color channel
    switch featParams.method
        
        case 'mr8'
            Icell{color}.FI = applyMaximumResponseFilterBank(Iin(:,:,color), featParams.filterMeta);
        case 'patch_rotate'
            Icell{color}.I = Iin(:,:,color);
            Icell{color}.FI = fft_filt_2(Iin(:,:,color), featParams.F, 1);
        case 'patch_simple'
            Icell{color}.I = Iin(:,:,color);
        case 'sift'
            itemp = single(Iin(:,:,color));
            [Icell{color}.frames, Icell{color}.descrs] = ...
                vl_phow(itemp, 'Sizes', featParams.siftBinSize, 'Step', 4, 'Fast', 'True');
        case 'mean_int'
            Icell{color}.I = Iin(:,:,color);
        
    end
end
Icell{1}.nrows = size(Iin, 1);
Icell{1}.ncols = size(Iin, 2);


end