function samples = textureSamples(dataDir, content, sampleList, featParams)

switch featParams.method
    case 'patch_simple'
        patchSizeGrab = featParams.patchSize;
    case 'patch_rotate'
        patchSizeGrab = featParams.patchBig;
    otherwise
        patchSizeGrab = 0;
end

currentImageInd = 0;

startTime = clock();

scale = featParams.scale;

for sItt = 1 : numel(sampleList.labels)
    
    processMonitor(startTime, clock(), numel(sampleList.labels), sItt, 100);
    
    if (sampleList.fromfile(sItt) ~= currentImageInd)
        
        I = textureLoadImage(fullfile(dataDir, content(sampleList.fromfile(sItt)).imgFile), featParams);
        currentImageInd = sampleList.fromfile(sItt);
        
    end
    
    row = round(sampleList.rowCol(sItt, 1) * 1/2^(scale - 1));
    col = round(sampleList.rowCol(sItt, 2) * 1/2^(scale - 1));
    %%% check that we won't go outside the image when grabbing the
    %%% patch.
    modifiedPosition = false;
    if (row - patchSizeGrab <= 0); row = patchSizeGrab + 1; modifiedPosition = true; end
    if (col - patchSizeGrab <= 0); col = patchSizeGrab + 1; modifiedPosition = true; end
    if (row + patchSizeGrab > I{1}.nrows); row = I{1}.nrows - patchSizeGrab; modifiedPosition = true; end
    if (col + patchSizeGrab > I{1}.ncols); col = I{1}.ncols - patchSizeGrab; modifiedPosition = true; end
    if (modifiedPosition)
        logger('Modified position for sample %d (row:%d, col:%d)', sItt, row, col);
    end
    %%% done checking that.
    patchRows = row - patchSizeGrab : row + patchSizeGrab;
    patchCols = col - patchSizeGrab : col + patchSizeGrab;
    for color = 1 : numel(I)
        
        patchCode = textureGrabOneSample(I{color}, patchRows, patchCols, featParams);
        if (sItt == 1 && color == 1) %first iteration
            samples = zeros(numel(sampleList.labels), numel(patchCode) * numel(I));
        end
        samples(sItt, (color - 1) * numel(patchCode) + 1 : color * numel(patchCode)) = patchCode;
        
    end
        
end

end
