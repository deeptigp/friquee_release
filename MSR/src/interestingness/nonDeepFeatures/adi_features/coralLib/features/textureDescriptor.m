function desc = textureDescriptor(dataDir, content, sampleList, ...
    featParams, encodeParams, contrastParams, D)


scale = featParams.scale;
intArea = featParams.intArea;
maxSamplesPerIntArea = featParams.maxSamplesPerIntArea;

startTime = clock();
currentImageInd = 0;

logger('Extracting descriptors');
for sItt = 1 : numel(sampleList.labels)
    
    
    if (sampleList.fromfile(sItt) ~= currentImageInd)
        
        I = textureLoadImage(fullfile(dataDir, content(sampleList.fromfile(sItt)).imgFile), featParams);
        currentImageInd = sampleList.fromfile(sItt);
        
    end
    
    row = round(sampleList.rowCol(sItt, 1) * 1/2^(scale - 1));
    col = round(sampleList.rowCol(sItt, 2) * 1/2^(scale - 1));
    
    % get samples
    tic; 
    for iaItt = 1 : numel(intArea)
        samples = patchDesctiptor(I, row, col, intArea(iaItt), maxSamplesPerIntArea, featParams);
        collectTime = toc;
        
        % contrast normalize
        samples = coralSetContrast(samples, contrastParams);
        
        % whiten
        samples = makeWhite(samples, D.wp.mu, D.wp.S);
        
        tic;
        thisDescriptor = encodeWithDictionary(samples, D.dict, encodeParams);
        
        if sItt == 1
            desc = zeros(length(sampleList.labels), numel(thisDescriptor) * numel(intArea));
        end
        
        desc(sItt, (iaItt - 1) * numel(thisDescriptor) + 1 : iaItt * numel(thisDescriptor)) = thisDescriptor;
    end
    processMonitor(startTime, clock(), numel(sampleList.labels), sItt, 30);
    if ~mod(sItt, 30); logger('Sample (point %d), collected in %.3f seconds and mapped in %.3f seconds', sItt, collectTime, toc); end
    
end


end

function samples = patchDesctiptor(I, row, col, intArea, maxSamplesPerIntArea, featParams)

switch featParams.method
    case 'patch_simple'
        patchSizeGrab = featParams.patchSize;
    case 'patch_rotate'
        patchSizeGrab = featParams.patchBig;
    otherwise
        patchSizeGrab = 0;
end

intArea = ceil(intArea); % make sure this is an integer.

intAreaDim = (intArea*2 + 1)^2;

% Create offsets.
rows = row - intArea : row + intArea;
cols = col - intArea : col + intArea;

% Make sure they are within limits.
rows = rows(rows > patchSizeGrab & ...
    rows <= I{1}.nrows - patchSizeGrab);
cols = cols(cols > patchSizeGrab & ...
    cols <= I{1}.ncols - patchSizeGrab);

% Figure out the max nbr steps in each direction.
nSteps = round(sqrt(min(maxSamplesPerIntArea, intAreaDim)));

% If nSteps is smaller than numel(rows), randomly subselect.
rows = randSelect(rows, nSteps);
cols = randSelect(cols, nSteps);


pos = 0;
% now, get all samples
for rowItt = 1 : numel(rows)
    for colItt = 1 : numel(cols)
        pos = pos + 1;
        patchRows = rows(rowItt) - patchSizeGrab : rows(rowItt) + patchSizeGrab;
        patchCols = cols(colItt) - patchSizeGrab : cols(colItt) + patchSizeGrab;
        for color = 1 : numel(I)
            
            patchCode = textureGrabOneSample(I{color}, patchRows, patchCols, featParams);
            if (rowItt == 1 && colItt == 1 && color == 1) %first iteration
                samples = zeros(numel(rows) * numel(cols), numel(patchCode) * numel(I));
            end
            samples(pos, (color - 1) * numel(patchCode) + 1 : color * numel(patchCode)) = patchCode;
            
        end
    end
end

end