function FIC = applyMaximumResponseFilterBank(I, filterMeta)

F = filterMeta.F;
Fids = filterMeta.Fids;
nbrOriented = filterMeta.nbrOriented * 2;
nbrCircular = filterMeta.nbrCircular * 2;
nbrFilters = nbrOriented + nbrCircular;

FI = fft_filt_2(I, F, 1);

FIC = zeros(size(FI, 1), size(FI, 2), nbrFilters);
for r = rowVector(unique(Fids))
       ind = Fids == r;
     if (sum(ind) > 1) 
         thisFI = abs(FI(:,:,ind));
         FIC(:,:,r) = max(thisFI, [], 3);
     else
         FIC(:,:,r) = FI(:,:,ind);       
     end
end

end