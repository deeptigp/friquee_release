function D = makeDictionary(samples, labels, dictLearn)
% function D = makeDictionary(samples, dictLearn)
%
% samples is a nSamles * nDims matrix
% labels is a nSamples * 1 array
% dictLearn.method indicate what type of dictionary

logger(sprintf('Making dictionary with method: %s', dictLearn.method));
params = dictLearn.params;

switch dictLearn.method
    case 'gmm'
        D.model = mexGmmTrainSP(single(samples'), single(params.K));
        
    case 'sc'
        D = [];
        for labelItt = rowVector((unique(labels)))
            ts = samples(labels == labelItt, :);
            
            D = [D mexTrainDL(ts', params)];
        end
        D = D';
        
    case 'kmeans'
        D = [];
        for labelItt = rowVector(unique(labels))
            ts = samples(labels == labelItt, :);
            [~, thisDict] = kmeans2(ts, params.K);
            
            % add dummy entries if missing.
            thisDict = [thisDict; zeros(params.K - size(thisDict, 1), size(thisDict, 2))];
            D = [D; thisDict];
        end
        D = bsxfun(@rdivide, D, sqrt(sum(D.^2, 2)) + 1e-20);
        
    case 'kmeans_allclass'
        [~, D] = kmeans2(samples, params.K);
        D = bsxfun(@rdivide, D, sqrt(sum(D.^2, 2)) + 1e-20);
        
    case 'svmrbf'
        svmParams = solverParams('libsvm');
        gridParams = svmParams.gridParams;
        errorStruct.handle = @coralErrorFcn;
        errorStruct.params.type = 'simple';
        solverStruct.handle = @libsvmWrapper;
        solverStruct.params.train = svmParams.options;
        solverStruct.params.weights = ones(1, max(labels));
        
        trainData.features = samples;
        trainData.labels = labels;
        trainData.fromfile = (1 : numel(labels))';
        fileNbrs = unique(trainData.fromfile);
        cvtrain = splitData(trainData, fileNbrs(1:2:end));
        cvtest = splitData(trainData, fileNbrs(2:2:end));
        
        [optParams D.stats] = hyperParamWrapper(cvtrain, cvtest, gridParams, errorStruct, solverStruct);
        
        solverOptions.train = setSolverCommonOptions(solverStruct.params.train, optParams);
        optStr = makeSolverOptionString(solverStruct.params.train, solverStruct.params.weights);
        
        D.model = svmtrain(trainData.labels, trainData.features, optStr);
        
    case 'svmlin_cluster'
        
        % Find cluster associations for each class
        for labelItt = rowVector(unique(labels))
            ts = samples(labels == labelItt, :);
            idx{labelItt} = kmeans2(ts, params.K);
        end
        D = [];
        for labelItt = rowVector(unique(labels))
            ts = samples(labels == labelItt, :);
            for clusterItt = 1 : params.K
                logger('Processing label: %d, cluster: %d', labelItt, clusterItt);
                positives = ts(idx{labelItt} == clusterItt, :);
                if isempty(positives) %this can happen. Sometimes not all k cluster centers are used
                    positives = randSelect(ts, 100);
                end
                negatives = samples(labels ~= labelItt, :);
                this_features = [positives; negatives];
                this_labels = [ones(size(positives, 1), 1); 2 * ones(size(negatives, 1), 1)];
                model = train(this_labels, sparse(this_features), '-c 1 -q -w1 .5 -w2 .01');
                D = [D; model.w];
            end
        end
        
    case 'svmlin_cluster_select'
        % create (a lot of) clusters
        
        C = [];
        clabels = [];
        for labelItt = rowVector(unique(labels))
            ts = samples(labels == labelItt, :);
            [idx{labelItt} Ctemp] = kmeans2(ts, params.Kbig);
            nC = size(Ctemp, 1);
            C = [C; Ctemp];
            clabels = [clabels; labelItt * ones(nC, 1)];
        end
        % find the most 'remote' clusters for each class, and train a
        % classifier using these clusters.
        K = pdist2( C, C, 'sqeuclidean');
        D = [];
        for labelItt = rowVector(unique(labels))
            distToOtherClusters = sum(K(clabels == labelItt, clabels ~= labelItt), 2);
            [~, clusterSortInd] = sort(distToOtherClusters, 'descend');
            
            ts = samples(labels == labelItt, :);
            for clusterItt = 1 : params.K
                
                positives = ts(idx{labelItt} == clusterSortInd(clusterItt), :);
                logger('Label: %d, cluster: %d (%d), nSamples: %d', labelItt, clusterItt, clusterSortInd(clusterItt), size(positives, 1));
                negatives = samples(labels ~= labelItt, :);
                negatives = negatives(randSelect(size(negatives, 1), 1000), :);
                this_features = [positives; negatives];
                this_labels = [ones(size(positives, 1), 1); 2 * ones(size(negatives, 1), 1)];
                model = train(this_labels, sparse(this_features), '-c 1 -q -w1 .5 -w2 .01');
                D = [D; model.w];
                
            end
        end
        
    case 'svmlin_randselect'
        D = [];
        for labelItt = rowVector(unique(labels))
            
            for clusterItt = 1 : params.K
                ts = samples(labels == labelItt, :);
                positives = ts(randSelect(size(ts, 1), 1), :);
                logger('Label: %d, cluster: %d (%d)', labelItt, clusterItt);
                
                negatives = samples(labels ~= labelItt, :);
                negatives = negatives(randSelect(size(negatives, 1), 1000), :);
                this_features = [positives; negatives];
                this_labels = [ones(size(positives, 1), 1); 2 * ones(size(negatives, 1), 1)];
                model = train(this_labels, sparse(this_features), '-c 1 -q -w1 .5 -w2 .01');
                D = [D; model.w];
                
            end
        end
        
    case 'svmlin'
        
        
        trainData.features = samples;
        trainData.labels = labels;
        trainData.fromfile = (1 : numel(labels))';
        trainData = selectSamples(trainData, inf); %sort by label, so the label ordering is correct.
        
        [~, model] = TPtrainSimple(trainData, trainData, gridParams, errorStruct, solverStruct);
        D.model = model;
        
    case 'svmlin_cluster_serge'
        
        % Map the data to a more fine - grained label structure
        clusterLabelMap = [];
        newLabels = [];
        newSamples = [];
        labelCounter = 0;
        for labelItt = rowVector(unique(labels))
            ts = samples(labels == labelItt, :);
            clusterIds = kmeans2(ts, params.K);
            for i = 1 : max(clusterIds)
                clusterLabelMap(labelCounter + i, 1) = labelItt;
            end
            newLabels = [newLabels; clusterIds + labelCounter];
            labelCounter = labelCounter + max(clusterIds);
            newSamples = [newSamples; ts];
        end
        
        % Create the trainData struct
        trainData.features = newSamples;
        trainData.labels = newLabels;
        trainData.fromfile = (1 : numel(labels))';
        trainData = selectSamples(trainData, inf); %sort by label, so the label ordering is correct.
        
        % Do training (with proper hyper parameter sweep).
        [~, model] = TPtrainSimple(trainData, trainData, gridParams, errorStruct, solverStruct);
        D.model = model;
        
end

end