function desc = filterDescriptors(dataDir, content, sampleList, featureParams, D)
% function dataOut = featuresClassic(cs, fileNbr)

nbrTextons = size(D, 1);
patchSize = featureParams.patchSize;
desc = zeros(numel(sampleList.labels), nbrTextons * numel(patchSize));
encodeParams.method = 'sqeuclidean';

currentImageInd = 0;
startTime = clock();
for sampleItt = 1 : numel(sampleList.labels)
    
    processMonitor(startTime, clock(), numel(sampleList.labels), sampleItt, 30);
    
    if (sampleList.fromfile(sampleItt) ~= currentImageInd)
        tic;
        I = imread(fullfile(dataDir, content(sampleList.fromfile(sampleItt)).imgFile));
        I = (imresize(I, 1/featureParams.resizeFactor));
        
        % filter image
        FIallChannels = coralApplyFilterWrapper(I, featureParams, featureParams.filterMeta, size(D, 2));
        
        % create texton map
        [nbrRows nbrCols nbrDims] = size(FIallChannels);
        samples = reshape(FIallChannels, nbrRows * nbrCols, nbrDims);
        textonMap = encodeWithDictionary(samples, D, encodeParams);
        textonMap = reshape(textonMap, nbrRows, nbrCols, 1);
        
        currentImageInd = sampleList.fromfile(sampleItt);
        logger('New image loaded in %.3f seconds.', toc);
    end
    
    % pool textonMap to features.
    rowCol = round((sampleList.rowCol(sampleItt, :)) ./ featureParams.resizeFactor);
    desc(sampleItt, :) = getHistFromTextonMap(textonMap, rowCol, patchSize, nbrTextons);
    
end