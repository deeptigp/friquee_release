function patchCode = textureGrabOneSample(I, patchRows, patchCols, featParams)

switch featParams.method
    case 'patch_simple'
        patchDim = (featParams.patchSize*2 + 1)^2;
        patch = I.I(patchRows, patchCols);
        patchCode = reshape(patch, 1, patchDim);
    
    case 'patch_rotate'
        patchDim = (featParams.patchTarget*2 + 1)^2;
        FIpatch = I.FI(patchRows, patchCols);
        Ipatch = I.I(patchRows, patchCols);
        
        patch = rotInvPatchPrefiltered(FIpatch, Ipatch, featParams.patchTarget, featParams.nFilterScales);
        patchCode = reshape(patch, 1, patchDim);
        
    case 'mr8'
        patchDim = featParams.filterOutputDim;
        patch = I.FI(patchRows, patchCols, :);
        patchCode = reshape(patch(:), 1, patchDim);
        
    case 'sift'
        patchDim = 128;
        %first, we need to find the closest row, col.
        siftrows = I.frames(2, :);
        siftcols = I.frames(1, :);
        rows = siftrows - patchRows; %patchRows is a scalar.
        cols = siftcols - patchCols; %patchCols is a scalar.
        d2 = rows.^2 + cols.^2;
        [~, ind] = find(d2 == min(d2), 1);
        patchCode = reshape(I.descrs(:, ind), 1, patchDim);
        % Set NaN's to zero. Very rare.
        patchCode(isnan(patchCode)) = .1;
        
    case 'mean_int'
        patchCode = mean(mean(I.I(patchRows, patchCols))); %output is a scalar
        
end

end