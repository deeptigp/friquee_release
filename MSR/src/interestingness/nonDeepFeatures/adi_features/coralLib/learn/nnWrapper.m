function res = nnWrapper(trainData, testData)



NNind = zeros(size(testData.features, 1), 1);

% divide in 100 parts
P = unique(round(linspace(0, size(testData.features, 1), 100)));

for i = 2 : length(P)
    thisChunk = testData.features(P(i-1) + 1 : P(i), :);
    [NNind(P(i-1) + 1 : P(i), :), ~] = vgg_nearest_neighbour_chi(thisChunk', trainData.features');
end


estLabels = trainData.labels(NNind);
acc = nnz(estLabels == testData.labels) / numel(estLabels);

res.estLabels = estLabels;
res.accuracy = acc;
res.CM = confMatrix(testData.labels, res.estLabels, max([trainData.labels; testData.labels]));


end