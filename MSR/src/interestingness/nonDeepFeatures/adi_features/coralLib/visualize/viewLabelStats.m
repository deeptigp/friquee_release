function viewLabelStats(labelCount, allLabels)

%%% visualize annotations
[~, ind] = sort(labelCount, 'descend');
labelCount = labelCount(ind);
allLabels = allLabels(ind);
% keyboard
d = bar(labelCount);
dd = get(d, 'Parent');
set(dd, 'XTick', 1:numel(labelCount));
set(dd, 'XTickLabel', strrep(allLabels, '_', ' '));
xticklabel_rotate;
ylabel('count');
% for i = 1:length(labelCount)
%     
%     fprintf(1, '%s; %.4f\n', allLabels{i}, 100*labelCount(i) / sum(labelCount));
%     
% end
end