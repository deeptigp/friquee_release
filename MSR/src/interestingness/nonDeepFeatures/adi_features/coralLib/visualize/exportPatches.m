function exportPatches(content, dataDir, labelParams, patchSize, outputDir)

for i = 1 : length(labelParams.cats)
    mkdir(fullfile(outputDir, labelParams.cats{i}));
end

for mainItt = 1 : numel(content);
    fileNbr = mainItt;
    
    % read images.
    I = imread(fullfile(dataDir, content(fileNbr).imgFile));
    
    % read labels
    labelMatrix = readCoralLabelFile(fullfile(dataDir, content(fileNbr).labelFile), labelParams);
    labelMatrix = pruneLabelMatrix(labelMatrix, size(I), patchSize);
    
    for sample = 1 : length(labelMatrix)
        row = labelMatrix(sample, 1);
        col = labelMatrix(sample, 2);
        label = labelMatrix(sample, 3);
        patch = I(row - patchSize : row + patchSize, col - patchSize : col + patchSize, :);
        
        thisDir = fullfile(outputDir, labelParams.cats{label});
        thisFile = fullfile(thisDir, strcat(num2str(fileNbr), '_', num2str(sample), '.jpg'));
        
        imwrite(patch, thisFile, 'jpg');
        
    end
    
    
    for i = 1 : length(labelParams.cats)
        thisDir = fullfile(outputDir, labelParams.cats{i});
        createImgDisplayHtmlCode(thisDir, [patchSize*2 + 1, patchSize*2 + 1], 300);
    end
end

end