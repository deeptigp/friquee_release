function samples = coralSetContrast(samples, params)

switch params.method
    
    case 'unit'
        samples = bsxfun(@rdivide, bsxfun(@minus, samples, mean(samples,2)), sqrt(var(samples,[],2) + params.varAdd));
    case 'weber'
        L = sqrt(sum(samples.^2, 2));
        L = L + 1e-10;
        samples = samples .* repmat(log(1 + L./.03) ./ L, [1 size(samples, 2)]);
    case 'imagerank'
        [~, lucid] = sort(samples);
        [~, samples] = sort(lucid);
    case 'none'
        
end
    
    
end
