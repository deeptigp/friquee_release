function [mu, S] = getWhite(inData, whiteParams)
% function [whiteData, m, v, S] = whitening(inData, whiteParams)
% Returns parameter for data whitening.
%
% INPUT inData: rows are observations, columns are features.
% INPUT whiteParams.level = {white, unbiased, none}
% INPUT whiteParams.epsilon = small add to the diagonal
%
% Copyright Oscar Beijbom, Sep 26 2012.

ndim = size(inData, 2);
mu = zeros(1, ndim);
S = eye(ndim);

if strcmp(whiteParams.level, 'none')
    return
end

mu = mean(inData, 1);
if strcmp(whiteParams.level, 'unbiased')
    return
end

inData = bsxfun(@minus, inData, mu);

[V,D] = eig(cov(inData));

S = V * diag(sqrt(1./(diag(D) + whiteParams.epsilon))) * V';


end