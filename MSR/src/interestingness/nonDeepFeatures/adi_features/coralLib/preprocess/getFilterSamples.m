function samples = getFilterSamples(dataDir, content, sampleList, featureParams)


nSamples = numel(sampleList.labels);
samples = zeros(nSamples, featureParams.filterOutputDim);

currentImageInd = 0;
startTime = clock();
for sItt = 1 : numel(sampleList.labels)
    
    processMonitor(startTime, clock(), numel(sampleList.labels), sItt, 100);
    
    if (sampleList.fromfile(sItt) ~= currentImageInd)
        tic;
        I = imread(fullfile(dataDir, content(sampleList.fromfile(sItt)).imgFile));
        I = (imresize(I, 1 / featureParams.resizeFactor));
        
        FIallChannels = coralApplyFilterWrapper(I, featureParams, featureParams.filterMeta, featureParams.filterOutputDim);
        
        currentImageInd = sampleList.fromfile(sItt);
        logger('New image loaded in %.3f seconds.', toc);
    end
    
    row = round(sampleList.rowCol(sItt, 1) / featureParams.resizeFactor);
    col = round(sampleList.rowCol(sItt, 2) / featureParams.resizeFactor);
    temp = FIallChannels(row, col, :);
    samples(sItt, :) = temp(:)';
    
    
end

end