function I = coralSetColor(I, colorParams)

switch colorParams.cspace
    case 'hsv'
        I = rgb2hsv(I);
    case 'lab'
        I = applycform(I, makecform('srgb2lab'));
    case 'rgb'
        
    case 'gray'
        I = rgb2gray(I);
    case 'original'
        
end

I = I(:,:, colorParams.channels);


end