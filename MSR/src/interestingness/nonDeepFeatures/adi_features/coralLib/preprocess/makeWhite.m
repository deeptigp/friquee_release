function [data, mu, S] = makeWhite(data, mu, S)
% function [whiteData, m, v, S] = makeWhite(inData, m, S)
% Whitens the input data.

% INPUT inData: rows are observations, columns are features.
% INPUT mu: mean value vector
% INPUT S: whitening matrix

% Copyright Oscar Beijbom, Sep 26 2012.
data = bsxfun(@minus, data, mu) * S;


end