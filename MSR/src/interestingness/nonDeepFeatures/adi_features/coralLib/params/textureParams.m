function featParams = textureParams(method, database)

switch method
    
    case 'patch_simple'
        
        switch database
            case 'lter'
                patchSize = 8;
            case 'UIUC'
                patchSize = 8;
            case 'curet'
                patchSize = 5;
            case 'kth'
                patchSize = 5; % TODO(mmoghimi)?
            case 'umd'
                patchSize = 5; % TODO(mmoghimi)?
        end
        featParams.patchSize = patchSize;
        
    case 'patch_rotate'
        
        switch database
            case 'lter'
                patchTarget = 8;
            case 'UIUC'
                patchTarget = 8;
            case 'curet'
                patchTarget = 5;
            case 'kth'
                patchTarget = 5; % TODO(mmoghimi)?
            case 'umd'
                patchTarget = 5; % TODO(mmoghimi)?
        end
        patchBig = ceil(patchTarget * sqrt(2));
        
        F = makeRFSfiltersFine(1, 1);
        F = F(:,:, 1 : end/2);
        featParams.F = F;
        featParams.nFilterScales = 1;
        featParams.patchTarget = patchTarget;
        featParams.patchBig = patchBig;
        
    case 'mr8'
        
        switch database
            
            case 'lter'
                
                filterParams.stds = [1 3 8 3];
                filterParams.oriented = [1 1 1 0];
                
            case {'UIUC', 'curet', 'kth', 'umd', 'six'}
                
                filterParams.stds = [1 2 4 10];
                filterParams.oriented = [1 1 1 0];
                
        end
        featParams.filterMeta = getFilterMeta(filterParams);
        featParams.supportRegion = 24;
        featParams.filterOutputDim = 2 * length(filterParams.stds);
        
    case 'sift'
        
        featParams.siftBinSize = 4;
        
    otherwise
        disp('wrong input')
        
end

switch database
    
    case {'lter'}
        color.cspace = 'lab';
        color.channels = 1:3;
        intArea = 120;
        preprocess.type = 'eachColorchannelStretch';
        preprocess.low = 0.01;
        preprocess.high = 0.99;
        
    case {'UIUC', 'curet', 'kth', 'umd'}
        color.cspace = 'original';
        color.channels = 1;
        intArea = 1000;
        preprocess.type = 'unit';
        
    case {'six'}
        color.cspace = 'gray';
        color.channels = 1;
        intArea = 60;
        preprocess.type = 'eachColorchannelStretch';
        preprocess.low = 0.01;
        preprocess.high = 0.99;
        
end
featParams.maxSamplesPerIntArea = 10000;
featParams.color = color;
featParams.method = method;
featParams.intArea = intArea;
featParams.preprocess = preprocess;

end



%%% HELPER FUNCTIONS %%%
function filterMeta = getFilterMeta(filterParams)

F = makeRFSfilters(filterParams.stds, filterParams.oriented, 49);

filterMeta.Fids = [];
for i = 1 : sum(filterParams.oriented)  * 2
    filterMeta.Fids = [filterMeta.Fids ; ones(6,1) * i];
end
for i = sum(filterParams.oriented)  * 2 + 1: length(filterParams.oriented) * 2
    filterMeta.Fids = [filterMeta.Fids ; ones(1,1) * i];
end
filterMeta.nbrOriented = sum(filterParams.oriented);
filterMeta.nbrCircular = sum(~filterParams.oriented);

% Fcolstack = zeros(size(F, 3), 49^2);
% for i = 1 : size(F, 3)
%     Fcolstack(i, :) = reshape(F(:,:,i), numel(F(:, :, i)), 1)';
% end
filterMeta.F = F;


end
