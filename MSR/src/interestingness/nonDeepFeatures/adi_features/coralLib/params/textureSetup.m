function [dict feat] = textureSetup(dataBase, method, dictLearnMethod, dictEncodeMethod, scale, contrastMethod)

featParams = textureParams(method, dataBase);
featParams.scale = scale;

[dictLearn, dictEncode] = dictParams(dictLearnMethod, dictEncodeMethod, dataBase);

whiteParams.level = 'none';
whiteParams.epsilon = 0.1;

contrastParams.method = contrastMethod;
contrastParams.varAdd = 10;

[dataDir, content, labelParams] = databaseParams(dataBase);

%%% SETUP DICTIONARY PARAM STRUCT %%%

dictTotalSamples = 5e5;
switch dataBase
    case 'lter'
        maxSamplesPerFile = 50;
        fileNbrs = [1 : 3 : 671 2 : 3 : 671];
        sampleList = coralSampleList(dataDir, content, fileNbrs, labelParams, inf, maxSamplesPerFile);
        sampleList = orderSamplesByImage(selectSamples(sampleList, round(dictTotalSamples/9)));
        
    case 'UIUC'
        fileNbrs = 1 : 2 : 1000;
        sampleList = textureSampleList(fileNbrs, content, [480 640], 10, 50);
        sampleList = orderSamplesByImage(selectSamples(sampleList, round(dictTotalSamples/25)));
        
    case 'curet'
        fileNbrs = 1 : 2 : 5612;
        sampleList = textureSampleList(fileNbrs, content, [200 200], 10, 20);
        sampleList = orderSamplesByImage(selectSamples(sampleList, round(dictTotalSamples/61)));

    case 'kth'
        fileNbrs = 1 : 2 : 810;
        sampleList = textureSampleList(fileNbrs, content, [200 200], 10, 20);
        sampleList = orderSamplesByImage(selectSamples(sampleList, round(dictTotalSamples/10)));

    case 'umd'
        fileNbrs = 1 : 4 : 1000;
        sampleList = textureSampleList(fileNbrs, content, [960 1280], 10, 20);
        sampleList = orderSamplesByImage(selectSamples(sampleList, round(dictTotalSamples/61)));

    
end

dict.sampleList = sampleList;
dict.dataDir = dataDir;
dict.content = content;
dict.featParams = featParams;
dict.contrastParams = contrastParams;
dict.dictLearn = dictLearn;
dict.whiteParams = whiteParams;

%%% SETUP FEAT PARAM STRUCT %%%
switch dataBase
    case 'lter'
        samplesPerClass = 400;
        maxSamplesPerFile = 100;
        fileNbrs = [1 : 3 : 671 2 : 3 : 671];
        sampleList = coralSampleList(dataDir, content, fileNbrs, labelParams, inf, maxSamplesPerFile);
        sampleList = orderSamplesByImage(selectSamples(sampleList, samplesPerClass));
        
    case 'UIUC'
        fileNbrs = 1 : 2 : 1000;
        sampleList = textureSampleListTT(fileNbrs, content, [480 640]);
        
    case 'curet'
        fileNbrs = 1 : 2 : 5612;
        sampleList = textureSampleListTT(fileNbrs, content, [200 200]);

    case 'kth'
        fileNbrs = 1 : 2 : 810;
        sampleList = textureSampleListTT(fileNbrs, content, [200 200]);
end

feat(1).sampleList = sampleList;
feat(1).dataDir = dataDir;
feat(1).content = content;
feat(1).featParams = featParams;
feat(1).contrastParams = contrastParams;
feat(1).dictEncode = dictEncode;
feat(1).whiteParams = whiteParams;


switch dataBase
    case 'lter'
        fileNbrs = 3 : 3 : 671;
        sampleList = coralSampleList(dataDir, content, fileNbrs, labelParams, inf, maxSamplesPerFile);
        sampleList = orderSamplesByImage(selectSamples(sampleList, samplesPerClass));
    case 'UIUC'
        fileNbrs = 2 : 2 : 1000;
        sampleList = textureSampleListTT(fileNbrs, content, [480 640]);
    case 'curet'
        fileNbrs = 2 : 2 : 5612;
        sampleList = textureSampleListTT(fileNbrs, content, [200 200]);
    case 'kth'
        fileNbrs = 2 : 2 : 810;
        sampleList = textureSampleListTT(fileNbrs, content, [200 200]);
end
feat(2) = feat(1);
feat(2).sampleList = sampleList;

end
