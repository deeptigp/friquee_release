function [dataDir, content, labelParams] = databaseParams(dataset)


switch(dataset)
    
    case('lter')
        
        dataDir = fullfile(getdirs('datadir'), 'Coral/LTER-MCR/organised/');
        
        labelParams.cats = {'CCA', 'Turf', 'Macro', 'Sand', 'Acrop', 'Pavon', 'Monti', 'Pocill', 'Porit'};
        labelParams.id = [1 2 3 4 5 6 7 8 9 9 9 9];
        labelParams.catsOrg = {'CCA', 'Turf', 'Macro', 'Sand', 'Acrop', 'Pavon', 'Monti', 'Pocill', 'Porit', 'P. Irr', 'P. Rus', 'P mass'};
        
        c = makeCoralContent(dataDir, {'database', 'lter', 'habitat', 'pole', 'qu', 'date'});
        content = [c([c.year] == 2008); c([c.year] == 2009); c([c.year] == 2010); c([c.year] == 2011)];
        
    case('UIUC')
        
        dataDir = fullfile(getdirs('datadir'), 'UIUC_textures/organized');
        d = dir(dataDir);
        d = d(3:end);
        for i = 1 : 1000
            content(i).imgFile = d(i).name;
        end
        labelParams.cats = {'bark1', 'bark2', 'bark3', 'wood1', 'wood2', 'wood3', 'water', 'granite', 'marble', 'floor1', 'floor2', ...
            'pebbles', 'wall', 'brick1', 'brick2', 'glass1', 'glass2', 'carpet1', 'carpet2', 'upholstery', 'wallpaper', 'fur', 'knit', ...
            'cordouroy', 'plaid'};
        labelParams.id = 1 : 25;
        
    case('curet')
        
        dataDir = fullfile(getdirs('datadir'), 'curetcol/organized');
        d = dir(dataDir);
        d = d(3:end);
        for i = 1 : 5612
            content(i).imgFile = d(i).name;
        end
        labelParams.cats = cell(61, 1);
        labelParams.id = 1 : 61;
        
    case('kth')
        dataDir = fullfile(getdirs('datadir'), 'KTH_TIPS/organised');
        d = dir(dataDir);
        d = d(3:end);
        for i = 1 : 810
            content(i).imgFile = d(i).name;
        end
        labelParams.cats = cell(61, 1);
        labelParams.id = 1 : 61;
        
    case('six')
        dataDir = fullfile(getdirs('datadir'), 'Coral/sixchannel_Moorea/organised');
        locationKeys = {'database', 'dive', 'img', 'date'};
        content = makeCoralContent(dataDir, locationKeys);
        
        labelParams.cats = {'Turf', 'Macro', 'CCA', 'Bare-Subst', 'Dead', 'Sand', 'Porit',  'Pocill', 'Pavon', 'Acrop', 'Monti', 'HC_other', 'Other'};
        labelParams.id = 1 : length(labelParams.cats);
        labelParams.catsOrg = labelParams.cats;
        
    case('six_small')
        dataDir = fullfile(getdirs('datadir'), 'Coral/sixchannel_Moorea/organised');
        locationKeys = {'database', 'dive', 'img', 'date'};
        content = makeCoralContent(dataDir, locationKeys);
        
        labelParams.cats = {'Turf', 'Macro', 'CCA', 'Sand', 'Porit', 'Pocill'};
        labelParams.id = 1 : length(labelParams.cats);
        labelParams.catsOrg = labelParams.cats;
        
    case('six_hardcoral')
        dataDir = fullfile(getdirs('datadir'), 'Coral/sixchannel_Moorea/organised');
        locationKeys = {'database', 'dive', 'img', 'date'};
        content = makeCoralContent(dataDir, locationKeys);
        
        labelParams.cats = {'Turf', 'Macro', 'CCA', 'Sand', 'Coral'};
        labelParams.id = [1 2 5 3 5 5 4 5 5 5];
        labelParams.catsOrg = {'Turf', 'Macro', 'Porit', 'CCA', 'HC_other', 'Pocill', 'Sand', 'Pavon', 'Acrop', 'Monti'};
        
    case('six_reannotation')
        dataDir = fullfile(getdirs('datadir'), 'Coral/sixchannel_Moorea/organised');
        locationKeys = {'database', 'dive', 'img', 'date'};
        content = makeCoralContent(dataDir, locationKeys);
        
        labelParams.cats = {'Pocill', 'Porit', 'Hard', 'CCA', 'Turf', 'Macro', 'Sand', 'Other'};
        labelParams.id = 1:numel(labelParams.cats);
        labelParams.catsOrg = labelParams.cats;
        
end