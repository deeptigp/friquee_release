function [dictLearn, dictEncode] = dictParams(learnMethod, encodeMethod, database)


% set a default value of nbr clusters per class depending on the dataset.
switch(database)
    case 'UIUC'
        params.K = 1000;
    case 'curet'
        params.K = 40;
    case 'kth'
        params.K = 1000;
    case {'lter', 'six'}
        params.K = 10;
end

% for these learning methods, additional parameters are needed.
switch learnMethod
    case 'sc'
        
        params.mode = 0;
        params.lambda = 1;
        params.iter = -100;
        params.K = 100;
        
    case 'svmlin_cluster_select';
        params.K = 10;
        params.Kbig = 100;
        
end
dictLearn.method = learnMethod;
dictLearn.params = params;
clear params
switch encodeMethod
    
    case 'nonlin-fixed'
        params.alpha = .5;
        
    case 'svmlin_sumdecvals_sigmoid'
        params.bias = 0;
        params.scale = -5;
        
    case 'svmlin_sumdecvals_threshhold'
        params.alpha = .5;
        
    case 'fisher'
        params.grad_weights = false;
        params.grad_means = true;
        params.grad_variances = true;
        params.alpha = single(0.5);
        params.pnorm = single(1);
    otherwise
        params = [];
        
        
end


dictEncode.method = encodeMethod;
dictEncode.params = params;



end