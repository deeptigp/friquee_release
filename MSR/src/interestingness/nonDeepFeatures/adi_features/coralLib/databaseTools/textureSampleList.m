function sampleList = textureSampleList(fileNbrs, content, imSize, patchSize, samplesPerDim)

% reset the random point generator
defaultStream = RandStream.getDefaultStream();
reset(defaultStream);

nSamples = samplesPerDim.^2 * numel(fileNbrs);
sampleList = struct('fromfile', zeros(nSamples, 1), 'rowCol', zeros(nSamples, 2), 'labels', zeros(nSamples, 1), 'pointNbr', zeros(nSamples, 1));

rows = patchSize + 1 + randSample(imSize(1) - 2 * patchSize - 1, samplesPerDim);
cols = patchSize + 1 + randSample(imSize(2) - 2 * patchSize - 1, samplesPerDim);
pos = 0;

for fileItt = 1 : numel(fileNbrs)
    fileNbr = fileNbrs(fileItt);
    
    label = str2double(content(fileNbr).imgFile(2:3));
    pointNbr = 0;
    for rItt = 1 : numel(rows)
        for cItt = 1 : numel(cols)
            
            pos = pos + 1;
            pointNbr = pointNbr + 1;
            sampleList.fromfile(pos) = fileNbr;
            sampleList.rowCol(pos, :) = [rows(rItt) cols(cItt)];
            sampleList.labels(pos) = label;
            sampleList.pointNbr(pos) = pointNbr;
            
        end
    end
end


end

