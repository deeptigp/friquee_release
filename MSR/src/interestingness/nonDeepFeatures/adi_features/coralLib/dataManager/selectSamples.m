function [sampleList ssfactor]= selectSamples(sampleList, maxSamplesPerClass)


labels = unique(sampleList.labels);

allind = [];
ssfactor = zeros(1, numel(labels));
for labelItt = rowVector(labels)

    ind = find(sampleList.labels == labelItt);
        
    ind = randSelect(ind, maxSamplesPerClass);

    allind = [allind; ind];
    
    ssfactor(labelItt) = max(1, numel(ind) / maxSamplesPerClass);

end

for f = rowVector(fields(sampleList))
   
    sampleList.(f{1}) = sampleList.(f{1})(allind, :);
    
end


end