function [sampleList sampleCounter] = coralSampleList(dataDir, content, fileNbrs, labelParams, targetNbrSamples, maxSamplesPerFile)

% reset the random point generator
defaultStream = RandStream.getDefaultStream();
reset(defaultStream);

% read the first file to know nbr of points per file.
labelMatrixTemp = readCoralLabelFile(fullfile(dataDir, content(1).labelFile), labelParams);

nSamplesMax = round(size(labelMatrixTemp, 1) * 1.1 * numel(content)); %add 10% extra as buffert.
sampleList = struct('fromfile', zeros(nSamplesMax, 1), 'rowCol', zeros(nSamplesMax, 2), 'labels', zeros(nSamplesMax, 1), 'pointNbr', zeros(nSamplesMax, 1));

pos = 0;
nclasses = max(labelParams.id);
sampleCounter = zeros(1, nclasses);
fileNbrs = fileNbrs(randperm(numel(fileNbrs)));

for fileItt = 1 : numel(fileNbrs)
    fileNbr = fileNbrs(fileItt);

    labelMatrix = readCoralLabelFile(fullfile(dataDir, content(fileNbr).labelFile), labelParams);
    
    counterPerFile =  zeros(1, nclasses);
    for labelItt = 1 : size(labelMatrix, 1)
        if(counterPerFile(labelMatrix(labelItt, 3)) <= maxSamplesPerFile) %enables a threshold on the nbr samples from each image.
            pos = pos + 1;
            sampleList.rowCol(pos, :) = labelMatrix(labelItt, 1:2);
            sampleList.labels(pos) = labelMatrix(labelItt, 3);
            sampleList.pointNbr(pos) = labelItt;
            sampleList.fromfile(pos) = fileNbr;
            sampleCounter(labelMatrix(labelItt, 3)) = sampleCounter(labelMatrix(labelItt, 3)) + 1;
            counterPerFile(labelMatrix(labelItt, 3)) = counterPerFile(labelMatrix(labelItt, 3)) + 1;
        end
    end
    
    if (sum(sampleCounter >= targetNbrSamples) == numel(sampleCounter))
        break
    end
    
    
end

for f = rowVector(fields(sampleList))
    sampleList.(f{1}) = sampleList.(f{1})(1:pos, :);
end


end

