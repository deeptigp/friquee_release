function sampleList = coralRandomSampleList(fileNbrs, imArea, samplesPerDim)
% function sampleList = coralRandomSampleList(fileNbrs, content, imArea,
% samplesPerDim)
%
%
% imArea is [minRow, maxRow, minCol, maxCol]

% reset the random point generator
defaultStream = RandStream.getDefaultStream();
reset(defaultStream);

nSamples = samplesPerDim.^2 * numel(fileNbrs);
sampleList = struct('fromfile', zeros(nSamples, 1), 'rowCol', zeros(nSamples, 2), 'labels', zeros(nSamples, 1), 'pointNbr', zeros(nSamples, 1));

rows = randSelect(imArea(1) : imArea(2), samplesPerDim);
cols = randSelect(imArea(3) : imArea(4), samplesPerDim);
pos = 0;

for fileItt = 1 : numel(fileNbrs)
    fileNbr = fileNbrs(fileItt);
    
    label = 1;
    pointNbr = 0;
    for rItt = 1 : numel(rows)
        for cItt = 1 : numel(cols)
            
            pos = pos + 1;
            pointNbr = pointNbr + 1;
            sampleList.fromfile(pos) = fileNbr;
            sampleList.rowCol(pos, :) = [rows(rItt) cols(cItt)];
            sampleList.labels(pos) = label;
            sampleList.pointNbr(pos) = pointNbr;
            
        end
    end
end


end

