function [ssfactor stats] = getSVMssfactor(data, targetNbrSamplesPerClass)

classes = unique(data.labels);
nbrClasses = length(classes);

% Prepare the train data and model weights.
% find subsamples factors for the train data.
for itt = 1 : nbrClasses
    thisClass = classes(itt);
    nbrTotalSamples(itt) = sum(data.labels == thisClass);
    ssfactor(itt) = max(1, nbrTotalSamples(itt) / targetNbrSamplesPerClass);
    nbrTrainSamples(itt) = nbrTotalSamples(itt) / ssfactor(itt);
end

for f = {'nbrTotalSamples', 'ssfactor', 'nbrTrainSamples', 'classes'}
    stats.(f{1}) = eval(f{1});
end

end