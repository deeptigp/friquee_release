function sampleList = orderSamplesByImage(sampleList)

[~, ind] = sort(sampleList.fromfile);

for f = rowVector(fields(sampleList))
   
    sampleList.(f{1}) = sampleList.(f{1})(ind, :);
    
end

end