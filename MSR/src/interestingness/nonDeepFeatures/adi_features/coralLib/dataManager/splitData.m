function [out, ids] = splitData(data, fileIds, onlyIdsFlag)
% [out ids] = splitData(data, fileIds, onlyIdsFlag)
%
% function splitData splits a data struct containing labels, features,
% etc. using the "fromfile" field, so that data points from the same file,
% will always end up in the same split. E.G usage:
%
% fileIds = unique(myData.fromfile);
% trainData = splitData(myData, fileIds(1:2:end));
%
% If INPUT onlyIdsFlag == 1; splitData will only return the ids, not the
% new subsamples datastruct. This is useful for large memory situations.
%
%
if nargin < 3
    onlyIdsFlag = false;
end

ids = false(size(data.fromfile));

for i = 1 : length(fileIds)
    id = fileIds(i);
    ids = ids | data.fromfile == id;
end

if(onlyIdsFlag)
    out = [];
else
    allFields = rowVector(fields(data));
    for f = allFields
        out.(f{1}) = data.(f{1})(ids, :);
    end
end

end