function cs = setupFixedFields(cs)
% a kind of generic, setup file that sets standard values and creates
% dictionaries.


cs.params.featureParams(1).filterOutputDim = 2 * length(cs.params.featureParams(1).filterParams.stds) * length(cs.params.featureParams(1).color.channels);
cs.stats.nbrClasses = length(cs.params.labelParams.cats);
cs.stats.nbrDims = cs.stats.nbrClasses * cs.params.featureParams(1).nbrTextons;

featureParams = cs.params.featureParams(1);
filterMeta.F = makeRFSfilters(featureParams.filterParams.stds, featureParams.filterParams.oriented);

filterMeta.Fids = [];
for i = 1 : sum(featureParams.filterParams.oriented)  * 2
    filterMeta.Fids = [filterMeta.Fids ; ones(6,1) * i];
end
for i = sum(featureParams.filterParams.oriented)  * 2 + 1: length(featureParams.filterParams.oriented) * 2
    filterMeta.Fids = [filterMeta.Fids ; ones(1,1) * i];
end
filterMeta.nbrOriented = sum(featureParams.filterParams.oriented);
filterMeta.nbrCircular = sum(~featureParams.filterParams.oriented);

cs.featurePrep{1}.filterMeta = filterMeta;


end