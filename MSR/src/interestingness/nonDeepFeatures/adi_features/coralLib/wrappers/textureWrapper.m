function ds = textureWrapper(ds, varargin)

global logfid
logfid = fopen(fullfile(getdirs('testdatadir'), ds.fileinfo.date, ds.fileinfo.dir, 'log.txt'), 'a');

defaultStream = RandStream.getDefaultStream();
reset(defaultStream);

feat = [];
stdtrain = 0;

[varnames, varvals] = var2varvallist(feat, stdtrain);
[feat, stdtrain] = varargin_helper(varnames, varvals, varargin{:});

for fItt = 1 : numel(feat)
    
    fp = feat(fItt);
    if fp.dict.do
        D = dictWrapper(fp.dataDir, fp.content, fp.dict.sampleList, ... 
            fp.featParams, fp.contrastParams, fp.dict.whiteParams,  ...
            fp.dict.dictLearn);
    else
        D = fp.dict.D;
    end
    
    desc = textureDescriptor(fp.dataDir, fp.content, fp.sampleList, ...
        fp.featParams, fp.dictEncode, fp.contrastParams, D);
    
    ds.feat(fItt).p = fp;
    ds.feat(fItt).desc = desc;
    ds.feat(fItt).D = D;
    ds = saveDataStruct(ds);
end


if(stdtrain)
    
    data = ds.feat(1).p.sampleList;
    
    data.features = [];
    for i = 1 : numel(ds.feat)
        data.features = [data.features ds.feat(i).desc];
    end
    
    ds.res = train(data);
    
end
ds = saveDataStruct(ds);

end

function res = train(data)

[solverStruct, errorStruct, gridParams] = solverParams('liblin');
files = unique(data.fromfile);
trainData = splitData(data, files([1 : 3 : end 2 : 3 : end]));
testData = splitData(data, files(3 : 3 : end));
res = trainSVMlocal(trainData, testData, gridParams, errorStruct, solverStruct);

end


function D = dictWrapper(dataDir, content, sampleList, featParams, contrastParams, whiteParams, dictLearn)


% get samples
samples = textureSamples(dataDir, content, sampleList, featParams);

% contrast normalize
samples = coralSetContrast(samples, contrastParams);

% find whitening parameters
[wp.mu, wp.S] = getWhite(samples, whiteParams);

% whiten
samples = makeWhite(samples, wp.mu, wp.S);

% make the dictionary
D.dict = makeDictionary(samples, sampleList.labels, dictLearn);
D.wp = wp;


end
