function [p] = config_sift(c)
p.grid_spacing = 4; % distance between grid centers
p.patch_sizes = [8 16 24];

p.pyramid_levels = 4;
p.llcknn = 3;
p.maxsize = 500;
p.pooling_method = 'rotInv';

% dictionary parameters
p.dictionary_size = 1024;
p.num_images = 2000;
p.descPerImage = 1000;
p.num_desc = 5e5;

p.train_file = '%s/train_sift_%d.mat';
p.test_file = '%s/test_sift_%d.mat';
p.dictionary_file = '%s/dictionary_sift_%s_%d.mat';

p.dictionary_method = 'kmeans';
p.encode_method = 'llc';
