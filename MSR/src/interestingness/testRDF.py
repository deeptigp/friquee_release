import numpy as np
import os
import subprocess
import sys
import scipy.io as sio
import numpy as np
import time
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt

def readMatFile(fName, key):
	""" This method reads a mat file and converts to an ndarray and returns the array
	fName: Name of the mat file
	key: Name of the structure that is stored in the matfile.
	"""

	mat_contents = sio.loadmat(fName)
	retArray = mat_contents[key]
	return retArray

def generateTrainTestData(features, labels, trainInd, testInd, splitNum):
	# the -1 can be removed if we use python to generate the train/test split files.

	trainInd = trainInd[:,splitNum-1]
	testInd = testInd[:,splitNum-1]

	# This can be removed once we remove the dependency on the mat files.. MATLAB is 1 indexesd while python is 0-indexed
	trainInd = trainInd - 1
	testInd = testInd - 1

	# Separate out train and test features
	trainX = features[trainInd,:]
	testX = features[testInd,:]

	# First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
	trainY = labels[:,trainInd]
	trainY = trainY.transpose()
	trainY = np.reshape(trainY,(len(trainY)))

	# First transpose it to convert to [num_samples, 1] then reshape it to make the shape [num_samples]
	testY = labels[:,testInd]
	testY = testY.transpose()
	testY = np.reshape(testY,(len(testY)))

	return (trainX, trainY, testX, testY)

def trainDecisionForests(trainX, trainY, testX, testY,nEst):
	dtReg = RandomForestClassifier(n_estimators=nEst,max_depth=500)


	dtReg.fit(trainX, trainY)

	start_time = time.time()
	predY=  dtReg.predict(testX)
	print("%s seconds to fit RDF " %(time.time() - start_time))

	predYProbs = dtReg.predict_proba(testX)


	# Evaluate on the test data
	rcParams = dtReg.get_params()
	print rcParams
	score  = dtReg.score(testX, testY)

	return (score,predY,predYProbs)

def analyzeRDFPerf(testY, predY, predYProbs):
	n_classes = 2
	precision = dict()
	recall = dict()
	average_precision = dict()
	plt.clf()
	i = 0
	#For class 1
	precision[i], recall[i], _ = precision_recall_curve(testY, predYProbs[:, 1])
	average_precision[i] = average_precision_score(testY, predYProbs[:, 1])

	print average_precision[0]


	plt.plot(recall[i], precision[i],
		label='Precision-recall curve of class {0} (area = {1:0.2f})'
			''.format(i+1, average_precision[0]))

	plt.xlim([0.0, 1.0])
	plt.ylim([0.0, 1.05])
	plt.xlabel('Recall')
	plt.ylabel('Precision')
	plt.title('Precision recall curve for RDF class = 1, pool5 features')
	plt.legend(loc="lower right")
	plt.show()


if __name__ == '__main__':
	labels = readMatFile('Results/kFold_567.mat','kFold_567')
	features = readMatFile('Data/pool5Features.mat','pool5Features')
	trainInd = readMatFile('Data/TrainingNIter.mat','TrainingNIter')
	testInd = readMatFile('Data/TestingNIter.mat','TestingNIter')

	labels = labels['derivedGTLabels']
	labels = labels.flatten()
	labels = labels[0]
	labels = np.int64(labels == 1)

	trainX, trainY, testX, testY = generateTrainTestData(features,labels,trainInd,testInd,1)

	score, predY, predYProbs = trainDecisionForests(trainX, trainY, testX, testY,30)

	print score, sum(predY)

	analyzeRDFPerf(testY, predY, predYProbs)