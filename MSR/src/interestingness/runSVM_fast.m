function [medACC, testY, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(trainX, trainY, testX, testY, layer)
    % Create a model
    gamma = '0.0020';%2^-9;
    if(strcmp(layer,'pool5'))
        C = '2';
    elseif(strcmp(layer,'fc6'))
        C = '0.5';
    elseif(strcmp(layer,'fc7') || strcmp(layer,'conv4'))
        C = '0.25';
    end
    trainOpts = ['-t 0 -s 0 -b 0 -g ',gamma ' -c ',C];
    
    trainedModel = svmtrain(trainY,double(trainX),trainOpts);
    
    tStart = tic;
    [Y_hat, medACC , ~] = svmpredict (testY, double(testX), trainedModel, '-b 0 -q');
    runTimeForAllImgs = toc(tStart)
end