%% This method runs an SVR on the set of datapoints that got classified to have the current layer as the label. This is just to evaluate if the binary classifier is doing a good job.. For ex: If 20 data points got the label as pool5, we use pool5Features to predict their interestingness scores in this method.
function predictedResults = evaluateClassifier_copy2(classifierResults, layer)
    load('Data/pool5Features.mat');
    load('Data/fc6Features.mat');
    load('Data/fc7Features.mat');
    load('Data/layer4Features.mat');
    load('Data/layer3Features.mat');

    load('Data/interestingness_scores.mat');


    labels = interestingness_score;

    predictedResults = struct;

    %% Evaluation of the goodness of this prediction

    % Lets take the 80-20 train test splits.. For all the images where fc5 was
    % the predicted class, use class 1, fc6 features for class 2 and fc7 for
    % class 3.

    %%
    class1Indices = classifierResults.testIndx(classifierResults.Y_hat==1);
    trainIndx = classifierResults.trainIndx;

    % Lets train an SVRs
    if(strcmp(layer,'layer3'))
        disp('layer3')
        [eLCC3,eRHO3, ePred3, eRMSE3, psnrVal3, runTimeForAllImgs3, runTimePerImg3] = temp_linearSVR(layer3Features(trainIndx,:),labels(trainIndx)',layer3Features(class1Indices,:),labels(class1Indices)','layer3');
        predictedResults.eLCC3 = eLCC3;
        predictedResults.eRHO3 = eRHO3;
        predictedResults.ePred3 = ePred3;
        predictedResults.rmse3  = eRMSE3;
        predictedResults.psnrVal3  = psnrVal3;
        predictedResults.runTimeForAllImgs3 = runTimeForAllImgs3;
        predictedResults.runTimePerImg3 = runTimePerImg3;
        
    elseif(strcmp(layer,'layer4'))
        disp('layer4')
        [eLCC4,eRHO4, ePred4, eRMSE4, psnrVal4, runTimeForAllImgs4, runTimePerImg4] = temp_linearSVR(layer4Features(trainIndx,:),labels(trainIndx)',layer4Features(class1Indices,:),labels(class1Indices)','layer4');
        predictedResults.eLCC4 = eLCC4;
        predictedResults.eRHO4 = eRHO4;
        predictedResults.ePred4 = ePred4;
        predictedResults.rmse4  = eRMSE4;
        predictedResults.psnrVal4  = psnrVal4;
        predictedResults.runTimeForAllImgs4 = runTimeForAllImgs4;
        predictedResults.runTimePerImg4 = runTimePerImg4;
     
    elseif(strcmp(layer,'pool5'))
        disp('pool5')
        [eLCC5,eRHO5, ePred5, eRMSE5, psnrVal5, runTimeForAllImgs5, runTimePerImg5] = temp_linearSVR(pool5Features(trainIndx,:),labels(trainIndx)',pool5Features(class1Indices,:),labels(class1Indices)','pool5');
        predictedResults.eLCC5 = eLCC5;
        predictedResults.eRHO5 = eRHO5;
        predictedResults.ePred5 = ePred5;
        predictedResults.rmse5  = eRMSE5;
        predictedResults.psnrVal5  = psnrVal5;
        predictedResults.runTimeForAllImgs5 = runTimeForAllImgs5;
        predictedResults.runTimePerImg5 = runTimePerImg5;
  
    elseif(strcmp(layer,'fc6'))
        disp('fc6')
        [eLCC6,eRHO6, ePred6, eRMSE6, psnrVal6, runTimeForAllImgs6, runTimePerImg6] = temp_linearSVR(fc6Features(trainIndx,:),labels(trainIndx)',fc6Features(class1Indices,:),labels(class1Indices)','fc6');
        predictedResults.eLCC6 = eLCC6;
        predictedResults.eRHO6 = eRHO6;
        predictedResults.ePred6 = ePred6;
        predictedResults.rmse6  = eRMSE6;
        predictedResults.psnrVal6  = psnrVal6;
        predictedResults.runTimeForAllImgs6 = runTimeForAllImgs6;
        predictedResults.runTimePerImg6 = runTimePerImg6;
  
    elseif(strcmp(layer,'fc7'))
        disp('fc7')
        [eLCC7,eRHO7, ePred7, eRMSE7, psnrVal7, runTimeForAllImgs7, runTimePerImg7] = temp_linearSVR(fc7Features(trainIndx,:),labels(trainIndx)',fc7Features(class1Indices,:),labels(class1Indices)','fc7');
        predictedResults.eLCC7 = eLCC7;
        predictedResults.eRHO7 = eRHO7;
        predictedResults.ePred7 = ePred7;
        predictedResults.rmse7  = eRMSE7;
        predictedResults.psnrVal7  = psnrVal7;
        predictedResults.runTimeForAllImgs7 = runTimeForAllImgs7;
        predictedResults.runTimePerImg7 = runTimePerImg7;
  
    end

    predictedResults.class1Indices = class1Indices;
    predictedResults.eGT = labels(class1Indices);
   % save(strcat('Results/predictedBinaryResults_',layer,'.mat'),'predictedResults');
end