%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM regressor for NIter numbere of times.
function [lcc, rho, runTimePerImg, Y_hat] = temp_linearSVR2(trainX, trainY, testX, testY)
    addpath('..');
    fid = fopen('train_ind2.txt','w');

    for itr_im = 1:size(trainX,1)
        fprintf(fid,'%d ',trainY(itr_im,1));
        for itr_feat =1:size(trainX,2)
            fprintf(fid,'%d:%f ',itr_feat,trainX(itr_im,itr_feat));
        end
        fprintf(fid,'\n');
    end

    fclose(fid);

    % Execute it.
    system('./svm-scale -l -1 -u 1 -s range2 train_ind2.txt > train_scale2');

    % Construct the file ( input ) suitably for testing the SVM
    fid = fopen('test_ind2.txt','w');
    for itr_im = 1:size(testX,1)
        fprintf(fid,'%d ',testY(itr_im,1));
        for itr_param = 1:size(testX,2)
            fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);

    % Scale the data.
    system('./svm-scale  -r range2 test_ind2.txt > test_ind2_scaled');

    %% t: Kernel type. %s: svm type. g: gamma c: cost.
    %  system('svm-train  -s 0 -t 2 -b 1 -q train_scale1 model1');

    cmd = strcat('./svm-train  -t 2 -s 4 -b 1',' -q train_scale2 model2');
    
    system(cmd);
    
    tStart = tic; 
    
    system('./svm-predict  -b 1 test_ind2_scaled model2 output2.txt > dump2');
    
    runTimePerImg = toc(tStart);
    
    runTimePerImg = runTimePerImg/length(testY)
    
    disp(strcat('Prediction Time for 1 image ',num2str(runTimePerImg)));
    
    load output2.txt;
    % Predicted scores
    Y_hat=output2;

    rho = corr(Y_hat,testY,'type','Spearman');
    lcc = corr(Y_hat,testY,'type','Pearson');

    [lcc rho]
end