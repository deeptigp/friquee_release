%% Initializations
function classLabels = kFold_strategy2(kFold_567)
thresh = 0.03;

classLabels = zeros(1,length(kFold_567.derivedGTLabels));

del56 = kFold_567.absDiffAll(:,1) - kFold_567.absDiffAll(:,2);

del57 = kFold_567.absDiffAll(:,1) - kFold_567.absDiffAll(:,3);


class1 = (del56 <= thresh & del57 <=thresh);

class0 = (del56 > thresh | del57 > thresh);

classLabels(class1) = 1;

notFC5Indexes = find(class0);

%% for fc6

del65 = kFold_567.absDiffAll(notFC5Indexes,2) - kFold_567.absDiffAll(notFC5Indexes,1);

del67 = kFold_567.absDiffAll(notFC5Indexes,2) - kFold_567.absDiffAll(notFC5Indexes,3);

class1 = (del65 <= thresh & del67 <=thresh);

class0 = (del65 > thresh | del67 > thresh);

classLabels(notFC5Indexes(class1)) = 2;

%% fc7

notFC6Indexes = (notFC5Indexes(class0));

del75 = kFold_567.absDiffAll(notFC6Indexes,3) - kFold_567.absDiffAll(notFC6Indexes,1);

del76 = kFold_567.absDiffAll(notFC6Indexes,3) - kFold_567.absDiffAll(notFC6Indexes,2);

class1 = (del75 <= thresh & del76 <=thresh);

class0 = (del75 > thresh | del76 > thresh);

classLabels(notFC6Indexes(class1)) = 3;

end