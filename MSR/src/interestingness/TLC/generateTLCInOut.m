load('Data/TrainingIter.mat');
load('Data/TestingIter.mat');
load('Data/fc7Features.mat');
load('Data/dataset_mem_scores.mat');
splitIndx = 1;

feat = fc7Features;

trainIndx = TrainingIter(:,splitIndx);
testIndx = TestingIter(:,splitIndx);

trainFeat = feat(trainIndx,:);
trainY = interestingness_score(trainIndx);

testFeat = feat(testIndx,:);
testY = interestingness_score(testIndx);

fid = fopen('tlc_train.txt','w');

for itr_im = 1:size(trainFeat,1)
    fprintf(fid,'%f\t',trainY(itr_im));
    for itr_feat =1:size(trainFeat,2)
        fprintf(fid,'%f\t',trainFeat(itr_im,itr_feat));
    end
    fprintf(fid,'\n');
end
fclose(fid);


% fid = fopen('tlc_test.txt','w');
% 
% for itr_im = 1:size(testFeat,1)
%     fprintf(fid,'%f\t',testY(itr_im));
%     for itr_feat =1:size(testFeat,2)
%         fprintf(fid,'%f\t',testFeat(itr_im,itr_feat));
%     end
%     fprintf(fid,'\n');
% end
% fclose(fid);

%% === Compute Correlations ===
fid = fopen('tlcout\0.inst.txt','rt');
tmp = textscan(fid, '%d %f %f %f %f', 'Headerlines', 1);
[corr(tmp{1,2},tmp{1,3},'type','Spearman') corr(tmp{1,2},tmp{1,3})]
fclose(fid); 