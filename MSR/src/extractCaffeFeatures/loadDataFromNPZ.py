import numpy as np
import scipy.io as sio
import os

def getFolderNames(folderName):
	subFolderNames = [name for name in os.listdir(folderName) if os.path.isdir(os.path.join(folderName, name)) ]
	return(subFolderNames)

if __name__ == '__main__':
	fc7Features = []
	fc6Features = []
	pool5Features = []
	groundTruthLabels = []

	#loadNPZDir = '/media/deepti/I/data/ImageData/caffeFeatures/'
	#loadNPZDir = '/home/deepti/research/MSR/data/caffeFeatures/'
	loadNPZDir = '/media/deepti/J/MSR/imNetVal/vggNetCaffeFeats/'

	#saveMatDir = '/home/deepti/research/MSR/src/fineGrained/Data/'

	saveMatDir = '/media/deepti/J/MSR/imNetVal/Data/'
#	images_folder = '/media/deepti/SamsungSSD/ImageNet/'


#	subImageFolders = getFolderNames(images_folder)

	totalNumImages = 0
	numImagesPerClass = []

	for i in xrange(1,11):
		print i
		## Loading class 1
		#fName = loadNPZDir+'caffeFeats_imNetVal50C_4-7.npz'
		fName = loadNPZDir+'vggNetFeats_imNetVal50C_4-7_'+str(i)+'.npz'
		#fName = loadNPZDir+'caffeFeats_class_'+subImageFolders[i]+'_4-7.npz' # Specify the appropriate file nam here
		print fName
		data = np.load(fName)
		numImages = data['fc7'].shape[0]
		if(i == 1):
			fc7Features = data['fc7']
			fc6Features = data['fc6']
			pool5Features = data['pool5']
			#conv4Features = data['conv4']
			numImagesPerClass = [numImages]
			groundTruthLabels = np.ones((numImages,1))
		else:
			fc7Features = np.concatenate((fc7Features,data['fc7']), axis=0)
			fc6Features = np.concatenate((fc6Features,data['fc6']), axis=0)
			pool5Features = np.concatenate((pool5Features,data['pool5']), axis=0)
			#conv4Features = np.concatenate((conv4Features,data['conv4']), axis=0)
			numImagesPerClass = np.concatenate(numImagesPerClass, numImages)
			a1 = np.ones((numImages,1))*i
			#groundTruthLabels = np.concatenate((groundTruthLabels, a1),axis = 0)

		totalNumImages = totalNumImages + numImages

#	sio.savemat((saveMatDir+'dogs_fc7Features_1-25.mat'),{'fc7Features':fc7Features})
#	sio.savemat((saveMatDir+'dogs_fc6Features_1-25.mat'),{'fc6Features':fc6Features})
#	sio.savemat((saveMatDir+'dogs_pool5Features_1-25.mat'),{'pool5Features':pool5Features})
#	sio.savemat((saveMatDir+'dogs_conv4Features_1-10.mat'),{'conv4Features':conv4Features})

	sio.savemat((saveMatDir+'VGGNet_fc7Features_1-50.mat'),{'fc7Features':fc7Features})
	sio.savemat((saveMatDir+'VGGNet_fc6Features_1-50.mat'),{'fc6Features':fc6Features})
	sio.savemat((saveMatDir+'VGGNet_pool5Features_1-50.mat'),{'pool5Features':pool5Features})
