import numpy as np
import sys
import caffe
import os
import glob
import scipy.io as sio
import time


def reshapeFeats(X):
	dim = X.shape
	newRow = dim[0]
	newCol = reduce(lambda x, y: x*y, dim[1:])
	X = np.reshape(X,(newRow, newCol))
	return X


if __name__ == '__main__':

	## Initializations.

	caffe_root = '/home/deepti/research/caffe/'
	matFilePath = '/home/deepti/research/MSR/src/interestingness/Data/'
	saveDir = '/media/deepti/I/data/ImageData/caffeFeatures/'

	images_folder = '/media/deepti/I/data/ImageData/memorabilityImages/'

	sys.path.insert(0, caffe_root + 'python')

	caffe.set_mode_cpu()

	net = caffe.Net(caffe_root + 'models/bvlc_reference_caffenet/deploy.prototxt',
					caffe_root + 'models/bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel',
					caffe.TEST)

	# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_transpose('data', (2,0,1))
	transformer.set_mean('data', np.load(caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1)) # mean pixel
	transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
	transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB


	print("Loading folder: %s" % images_folder)

	inputFiles = []

	# For memorability images.
	# If we want to read all the images...
	# for i in xrange(numFiles):
	# 	inputFiles.append(images_folder+"{0:04d}".format(i+1)+".jpg")

	# inputs =[transformer.preprocess('data', caffe.io.load_image(im_f))
	# 				 for im_f in inputFiles]

	# numImages = len(inputs)

	# For memorability images
	# If we want to read only images in either Training or TestingNIter.mat files.

	mat_contents = sio.loadmat(matFilePath+'TestingNIter.mat')

	testImageIndxs = mat_contents['TestingNIter']

	#Fetching only the first column..

	testImageIndxs = testImageIndxs[:,0]

	for i in testImageIndxs:
		inputFiles.append(images_folder+"{0:04d}".format(i+1)+".jpg")

	start_time = time.time()

	inputs =[transformer.preprocess('data', caffe.io.load_image(im_f))
					 for im_f in inputFiles]

	numImages = len(inputs)

	print numImages

	net.blobs['data'].reshape(numImages,3,227,227)

	net.blobs['data'].data[...] = inputs

	out = net.forward()

	pool5 = net.blobs['pool5'].data

	print("--- %s seconds ---" % (time.time() - start_time))

			# fc7 = net.blobs['fc7'].data

			# pool5 = reshapeFeats(net.blobs['pool5'].data)

			# conv5 = reshapeFeats(net.blobs['conv5'].data)

			# conv4 = reshapeFeats(net.blobs['conv4'].data)

			# conv3 = reshapeFeats(net.blobs['conv3'].data)

			# conv2 = reshapeFeats(net.blobs['conv2'].data)

			# pool2 = reshapeFeats(net.blobs['pool2'].data)

			# pool1 = reshapeFeats(net.blobs['pool1'].data)

			# conv1 = reshapeFeats(net.blobs['conv1'].data)


			# np.savez(saveDir+'caffeFeats_class'+`folderIndx`+'_4-7.npz', conv4=conv4,conv5=conv5,pool5=pool5,fc6=fc6,fc7=fc7)

			# np.savez(saveDir+'caffeFeats_class'+`folderIndx`+'_1-3.npz', conv1=conv1,pool1=pool1,pool2=pool2,conv2=conv2,conv3=conv3)

		# Loading data
		#data = np.load('caffeFeats4-7.npz')
		#fc7 = data['fc7'] and so on.
