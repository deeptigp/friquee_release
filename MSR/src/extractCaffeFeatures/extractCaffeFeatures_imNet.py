import numpy as np
import sys
import caffe
import os
import glob
import scipy.io as sio


def reshapeFeats(X):
	dim = X.shape
	newRow = dim[0]
	newCol = reduce(lambda x, y: x*y, dim[1:])
	X = np.reshape(X,(newRow, newCol))
	return X

def getFolderNames(folderName):
	subFolderNames = [name for name in os.listdir(folderName) if os.path.isdir(os.path.join(folderName, name)) ]
	return(subFolderNames)

def getImageFileNames(folderName):
	files = [os.path.join(folderName,f) for f in os.listdir(folderName) if os.path.join(folderName,f)]
	return files

if __name__ == '__main__':

	## Initializations.
	folderName = '/media/deepti/SamsungSSD/ImageNet/'
	caffe_root = '/home/deepti/research/caffe/'
	matFilePath = '/home/deepti/research/MSR/src/interestingness/Data/'
	#saveDir = '/media/deepti/I/data/ImageData/caffeFeatures/'
	saveDir = '/home/deepti/research/MSR/data/caffeFeatures/'
	#images_folder = '/media/deepti/I/data/ImageData/memorabilityImages/'

	#images_folder = '/media/deepti/I/data/ImageData/ImageNet/Images/'

	images_folder = folderName
	#folderIndx = 3

	sys.path.insert(0, caffe_root + 'python')

	caffe.set_mode_cpu()

	net = caffe.Net(caffe_root + 'models/bvlc_reference_caffenet/deploy.prototxt',
					caffe_root + 'models/bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel',
					caffe.TEST)

	# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_transpose('data', (2,0,1))
	transformer.set_mean('data', np.load(caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1)) # mean pixel
	transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
	transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB

	subImageFolders = getFolderNames(images_folder) # gets the list of all the sub folders.

	for folderIndx in xrange(1,2):
			folderIndx
			currImageFolder = images_folder+subImageFolders[folderIndx]+'/'
			#print("Loading folder: %s" % images_folder+`folderIndx`)
			print("Loading folder: %s" % images_folder+subImageFolders[folderIndx])

			# For random imageNetClasses
			#mat_contents = sio.loadmat(matFilePath+'imNetTrainTest_'+`folderIndx`+'.mat')
			#inputFiles = mat_contents['imgNames']
			#inputFiles = [s.encode('ascii','ignore').strip() for s in inputFiles]
			inputFiles = getImageFileNames(currImageFolder)

			inputs =[transformer.preprocess('data', caffe.io.load_image(im_f))
							 for im_f in inputFiles]
			numImages = len(inputs)

			print numImages
			net.blobs['data'].reshape(numImages,3,227,227)

			net.blobs['data'].data[...] = inputs

			out = net.forward()

			fc6 = net.blobs['fc6'].data

			fc7 = net.blobs['fc7'].data

			pool5 = reshapeFeats(net.blobs['pool5'].data)

			conv4 = reshapeFeats(net.blobs['conv4'].data)

			# conv3 = reshapeFeats(net.blobs['conv3'].data)

			# conv2 = reshapeFeats(net.blobs['conv2'].data)

			# pool2 = reshapeFeats(net.blobs['pool2'].data)

			# pool1 = reshapeFeats(net.blobs['pool1'].data)

			# conv1 = reshapeFeats(net.blobs['conv1'].data)


			np.savez(saveDir+'caffeFeats_class_'+subImageFolders[folderIndx]+'_4-7.npz', conv4=conv4, pool5=pool5,fc6=fc6,fc7=fc7)

			#np.savez(saveDir+'caffeFeats_class'+`folderIndx`+'_1-3.npz', conv1=conv1,pool1=pool1,pool2=pool2,conv2=conv2,conv3=conv3)

		# Loading data
	#	data = np.load('caffeFeats4-7.npz')
	#	fc7 = data['fc7'] and so on.
