import numpy as np
import sys
import caffe
import os
import glob
import scipy.io as sio


def reshapeFeats(X):
	dim = X.shape
	newRow = dim[0]
	newCol = reduce(lambda x, y: x*y, dim[1:])
	X = np.reshape(X,(newRow, newCol))
	return X


if __name__ == '__main__':

	## Initializations.

	caffe_root = '/home/deepti/research/caffe/'
	matFilePath = '/home/deepti/research/MSR/src/interestingness/Data/'
	saveDir = '/media/deepti/I/data/ImageData/caffeFeatures/'

	images_folder = '/media/deepti/I/data/ImageData/memorabilityImages/'
	#folderIndx = 3
	numFiles = 2222
	inputFiles = []

	sys.path.insert(0, caffe_root + 'python')

	caffe.set_mode_cpu()

	net = caffe.Net(caffe_root + 'models/bvlc_reference_caffenet/deploy.prototxt',
					caffe_root + 'models/bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel',
					caffe.TEST)

	# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_transpose('data', (2,0,1))
	transformer.set_mean('data', np.load(caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1)) # mean pixel
	transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
	transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB

	print("Loading folder: %s" % images_folder)

	# For memorability images.
	for i in xrange(numFiles):
		inputFiles.append(images_folder+"{0:04d}".format(i+1)+".jpg")

	inputs =[transformer.preprocess('data', caffe.io.load_image(im_f))
						for im_f in inputFiles]
	numImages = len(inputs)

	print numImages
	net.blobs['data'].reshape(numImages,3,227,227)

	net.blobs['data'].data[...] = inputs

	out = net.forward()

	predictedClassNames = []

	# load labels
	imagenet_labels_filename = caffe_root + 'data/ilsvrc12/synset_words.txt'

	labels = np.loadtxt(imagenet_labels_filename, str, delimiter='\t')

	for i in xrange(numImages):
		topK = out['prob'][i].argmax()
		print labels[topK]
		predictedClassNames.append(labels[topK])