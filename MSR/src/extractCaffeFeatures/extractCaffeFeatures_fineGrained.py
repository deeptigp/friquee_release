import numpy as np
import sys
import caffe
import os
import glob
import scipy.io as sio


def reshapeFeats(X):
	dim = X.shape
	newRow = dim[0]
	newCol = reduce(lambda x, y: x*y, dim[1:])
	X = np.reshape(X,(newRow, newCol))
	return X

def getFolderNames(folderName):
	subFolderNames = [name for name in os.listdir(folderName) if os.path.isdir(os.path.join(folderName, name)) ]
	return(subFolderNames)

def getImageFileNames(folderName):
	files = [os.path.join(folderName,f) for f in os.listdir(folderName) if os.path.join(folderName,f)]
	return files

if __name__ == '__main__':

	## Initializations.
	folderName = '/media/deepti/SamsungSSD/ImageNet/FineGrained/'
	caffe_root = '/home/deepti/research/caffe/'
	saveDir = '/home/deepti/research/MSR/data/caffeFeatures/'

	images_folder = folderName

	sys.path.insert(0, caffe_root + 'python')

	caffe.set_mode_cpu()

	net = caffe.Net(caffe_root + 'models/bvlc_reference_caffenet/deploy.prototxt',
					caffe_root + 'models/bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel',
					caffe.TEST)


	# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_transpose('data', (2,0,1))
	transformer.set_mean('data', np.load(caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1)) # mean pixel
	transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
	transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB

	#subImageFolders = getFolderNames(images_folder) # gets the list of all the sub folders.

	matFile = sio.loadmat('/home/deepti/research/MSR/src/fineGrained/Data/dogSubClassData_10C.mat')
	matFile  = matFile['dogSubClassData_10C']
	imageList = matFile['imgData']
	imageList = imageList.flatten().flatten()
	imageList = imageList[0]

	inputFiles = []
	print len(imageList)

	for imgIndx in xrange(0,len(imageList)):
		imgName = folderName + imageList[imgIndx][0][0]
		inputFiles.append(imgName)


	inputs =[transformer.preprocess('data', caffe.io.load_image(im_f))
							 for im_f in inputFiles]

	numImages = len(inputs)

	print numImages

	net.blobs['data'].reshape(numImages,3,227,227)

	net.blobs['data'].data[...] = inputs

	out = net.forward()

	fc6 = net.blobs['fc6'].data

	fc7 = net.blobs['fc7'].data

	pool5 = reshapeFeats(net.blobs['pool5'].data)

	conv4 = reshapeFeats(net.blobs['conv4'].data)

	np.savez(saveDir+'caffeFeats_dogs50C_4-7.npz', conv4=conv4, pool5=pool5,fc6=fc6,fc7=fc7)

		# Loading data
	#	data = np.load('caffeFeats4-7.npz')
	#	fc7 = data['fc7'] and so on.
