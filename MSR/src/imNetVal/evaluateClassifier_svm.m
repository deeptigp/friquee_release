%% This method runs an SVR on the set of datapoints that got classified to have the current layer as the label. This is just to evaluate if the binary classifier is doing a good job.. For ex: If 20 data points got the label as pool5, we use pool5Features to predict their interestingness scores in this method.
function predictedResults = evaluateClassifier_svm(classifierResults, layer,trainTestSplit, data)
   DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
    load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));

    labels = imNetVal_groundTruthLabels_1_50;
    
    predictedResults = struct;

    class1Indices = classifierResults.testIndx(classifierResults.Y_hat==1);
    trainIndx = classifierResults.trainIndx;

    trainX = data(trainIndx,:);
    
    testX = data(class1Indices,:);
    
    trainY = labels(trainIndx);
    testY = labels(class1Indices);

    size(testY)
    
    % Lets train SVCs
    if(strcmp(layer,'pool5'))
        disp('pool5')
        [medACC, dumb, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(trainX,trainY, testX, testY,'pool5',trainTestSplit);
        predictedResults.medACC = medACC;
        predictedResults.ePred5 = Y_hat;
        predictedResults.runTimeForAllImgs5 = runTimeForAllImgs;
  
    elseif(strcmp(layer,'fc6'))
        disp('fc6')
        [medACC, dumb, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(trainX,trainY, testX, testY,'fc6',trainTestSplit);
        predictedResults.medACC = medACC;
        predictedResults.ePred6 = Y_hat;
        predictedResults.runTimeForAllImgs6 = runTimeForAllImgs;
  
    elseif(strcmp(layer,'fc7'))
        disp('fc7')
        [medACC, dumb, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(trainX, trainY, testX, testY,'fc7',trainTestSplit);
        predictedResults.medACC = medACC;
        predictedResults.ePred7 = Y_hat;
        predictedResults.runTimeForAllImgs7 = runTimeForAllImgs;
    end

    predictedResults.class1Indices = class1Indices;
    predictedResults.eGT = labels(class1Indices);
end