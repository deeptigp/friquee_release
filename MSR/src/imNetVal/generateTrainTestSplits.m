%numImages  = 32500; % for a random set of 5 classes from imNet
%numImages = 2208; % 10 class of dogs
numImages = 2500; % 25 class of dogs
trainSplit = 0.8;
numClasses = 50;

numTrainImgs = round(trainSplit*numImages);
    
TrainingNIter = [];
TestingNIter = [];

for n = 1:10
    indx = randperm(numImages);

    trainIndx = indx(1:numTrainImgs);
    testIndx = indx(1+numTrainImgs:end);
    
    TrainingNIter = [TrainingNIter trainIndx'];
    TestingNIter = [TestingNIter testIndx'];
end

save(strcat('Data/TrainingNIter_',num2str(numClasses),'Class.mat'),'TrainingNIter');
save(strcat('Data/TestingNIter_',num2str(numClasses),'Class.mat'),'TestingNIter');

% 
% [medACC7, testY, Y_hat7, runTimeForAllImgs7] = executeSVM(fc7Features,imNetVal50ClassLabels, 1, 'Data/TrainingNIter_50Class.mat', 'Data/TestingNIter_50Class.mat', 'fc7');
% 
% [medACC6, testY, Y_hat6, runTimeForAllImgs6] = executeSVM(fc6Features,imNetVal50ClassLabels, 1, 'Data/TrainingNIter_50Class.mat', 'Data/TestingNIter_50Class.mat', 'fc6');
% 
% [medACC5, testY, Y_hat5, runTimeForAllImgs5] = executeSVM(pool5Features,imNetVal50ClassLabels, 1, 'Data/TrainingNIter_50Class.mat', 'Data/TestingNIter_50Class.mat', 'pool5');