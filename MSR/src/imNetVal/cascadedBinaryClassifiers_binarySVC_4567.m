% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7
   DATA_PATH = '/media/deepti/J/MSR/fineGrained/Data/';
    load(strcat(DATA_PATH,'dogs_groundTruthLabels_1-10.mat'));

    labels = groundTruthLabels;

    load('Results/kFold_567_1_10_k5.mat');

    load(strcat(DATA_PATH,'dogs_conv4Features_compress_1-10.mat'));

    load(strcat(DATA_PATH,'dogs_pool5Features_1-10.mat'));
    
    load(strcat(DATA_PATH,'dogs_fc6Features_1-10.mat'));
    
    load(strcat(DATA_PATH,'dogs_fc7Features_1-10.mat'));
    
    load(strcat(DATA_PATH,'TrainingNIter_10Class.mat'));
    load(strcat(DATA_PATH,'TestingNIter_10Class.mat'));
    
    trainTestSplit = 1;
    
    trainIndx = TrainingNIter(:,trainTestSplit);
    testIndx = TestingNIter(:,trainTestSplit);

    layer4Features = conv4Features_compressed;
    
    cascadeClassifierResults_aggre_layer4 = struct;
    cascadeClassifierResults_aggre_pool5 = struct;
    cascadeClassifierResults_aggre_fc6 = struct;
    cascadeClassifierResults_aggre_fc7 = struct;
    
    % Generate a random train/test split.
    numImages = length(labels);

    % Get the derived class labels.
    GTLabels = kFold_4567.derivedLayerLabels;
    
    %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);
    
    
    %% Get the label indexes.
    class1LabelIndxs = find(GTLabels == 1);
    class2LabelIndxs = find(GTLabels == 2);
    class3LabelIndxs = find(GTLabels == 3);
    class4LabelIndxs = find(GTLabels == 4); % Layer4
    
    binaryLabels1 = zeros(1,numImages);
    binaryLabels2 = zeros(1,numImages);
    binaryLabels3 = zeros(1,numImages);
    binaryLabels4 = zeros(1,numImages); % Layer4
    
    
    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1;
    binaryLabels2(class2LabelIndxs) = 1;
    binaryLabels3(class3LabelIndxs) = 1;
    binaryLabels4(class4LabelIndxs) = 1; %Layer4
    
    %% train with layer4, those that say no-layer4, test them with pool5.
   [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg] = binarySVM_fast(layer4Features(trainIndx,:),binaryLabels4(trainIndx)',layer4Features(testIndx,:),binaryLabels4(testIndx)');
 
    cascadeClassifierResults_aggre_layer4.trainIndx = trainIndx;
    cascadeClassifierResults_aggre_layer4.testIndx = testIndx;
    cascadeClassifierResults_aggre_layer4.Y_hat = Y_hat;
    cascadeClassifierResults_aggre_layer4.accuracy = accuracy;
    cascadeClassifierResults_aggre_layer4.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre_layer4.runTimePerImg = runTimePerImg;
    
    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre_layer4.mat'),'cascadeClassifierResults_aggre_layer4');
    
    cascadePredictedResults_aggre_layer4 = evaluateClassifier_fast(cascadeClassifierResults_aggre_layer4, 'conv4',trainTestSplit);
    
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre_layer4.mat'),'cascadePredictedResults_aggre_layer4');
    
    aggregatePredictedScores(cascadePredictedResults_aggre_layer4.class1Indices) = cascadePredictedResults_aggre_layer4.ePred4;
    
    notFC4Indxs = cascadeClassifierResults_aggre_layer4.testIndx(cascadeClassifierResults_aggre_layer4.Y_hat==0); 
    
    %% train with fc5, those that say no-fc5, test them with fc6.
    [accuracy, Y_hat, runTimeForAllImgs, runTimePerImg] = binarySVM_fast(pool5Features(trainIndx,:),binaryLabels1(trainIndx)',pool5Features(notFC4Indxs,:),binaryLabels1(notFC4Indxs)');

    cascadeClassifierResults_aggre_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre_pool5.testIndx = notFC4Indxs;
    cascadeClassifierResults_aggre_pool5.Y_hat = Y_hat;
    cascadeClassifierResults_aggre_pool5.accuracy = accuracy;
    cascadeClassifierResults_aggre_pool5.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre_pool5.runTimePerImg = runTimePerImg;
    
    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre_pool5.mat'),'cascadeClassifierResults_aggre_pool5');
    
    cascadePredictedResults_aggre_pool5 = evaluateClassifier_fast(cascadeClassifierResults_aggre_pool5, 'pool5',trainTestSplit);
    
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre_pool5.mat'),'cascadePredictedResults_aggre_pool5');
    aggregatePredictedScores(cascadePredictedResults_aggre_pool5.class1Indices) = cascadePredictedResults_aggre_pool5.ePred5;
    
    %% train with fc5, those that say no-fc5, test them with fc6.
    
    notFC5Indxs = cascadeClassifierResults_aggre_pool5.testIndx(cascadeClassifierResults_aggre_pool5.Y_hat==0); 
    
    [accuracy, Y_hat, runTimeForAllImgs, runTimePerImg] = binarySVM_fast(fc6Features(trainIndx,:),binaryLabels2(trainIndx)',fc6Features(notFC5Indxs,:),binaryLabels2(notFC5Indxs)');
    
    cascadeClassifierResults_aggre_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre_fc6.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre_fc6.runTimePerImg = runTimePerImg;
    
    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre_fc6.mat'),'cascadeClassifierResults_aggre_fc6');
    
    cascadePredictedResults_aggre_fc6 = evaluateClassifier_fast(cascadeClassifierResults_aggre_fc6, 'fc6',trainTestSplit);
    
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre_fc6.mat'),'cascadePredictedResults_aggre_fc6');
    aggregatePredictedScores(cascadePredictedResults_aggre_fc6.class1Indices) = cascadePredictedResults_aggre_fc6.ePred6;
    
    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre_fc6.testIndx(cascadeClassifierResults_aggre_fc6.Y_hat==0); 
    
        
    Y_hat = ones(size(notFC6Indxs));

    cascadeClassifierResults_aggre_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre_fc7.Y_hat = Y_hat;
    cascadePredictedResults_aggre_fc7 = evaluateClassifier_fast(cascadeClassifierResults_aggre_fc7, 'fc7',trainTestSplit);
    
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre_fc7.mat'),'cascadePredictedResults_aggre_fc7');
    aggregatePredictedScores(cascadePredictedResults_aggre_fc7.class1Indices) = cascadePredictedResults_aggre_fc7.ePred7;
    
    nonZeroPredictedScores = aggregatePredictedScores(testIndx);
    aggreACC = mean(nonZeroPredictedScores ==labels(testIndx));

    aggregateResults.predictedScores = aggregatePredictedScores;
    aggregateResults.aggreACC = aggreACC;
    
    
    save(strcat('Results/aggregateResults/aggregatePredictedScores_fc4567.mat'),'aggregateResults');
    
    fprintf('Spearman and Pearson correlation between the aggregated predictions and GT predictions %f %f\n',aggreACC); 