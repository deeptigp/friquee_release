%function [accur, rPrec, rRecall,randomClassLabels] =  randClassLabels(gTLabels)

numTestSamples = 500;
%numTestSamples = length(gTLabels);

indx = randperm(numTestSamples, numTestSamples/2);

randomClassLabels = zeros(numTestSamples,1);

randomClassLabels(indx) = 1;

%accur = mean(gTLabels == randomClassLabels);

%[rPrec, rRecall] = computePrecRecall(gTLabels, randomClassLabels);