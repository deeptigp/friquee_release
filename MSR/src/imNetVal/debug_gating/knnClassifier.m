addpath('../');
DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
trainTestSplit = 1;
%% Loading features and labels.
load(strcat(DATA_PATH,'imNetVal_pool5Features_1_50.mat'));
%load(strcat(DATA_PATH,'imNetVal_pool5_compressed_1_50.mat'));

load(strcat('../Data/TrainingNIter_50Class.mat'));
load(strcat('../Data/TestingNIter_50Class.mat'));

load('../Results/kFold_567_1_alex.mat');

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

numImages = size(pool5Features,1);
%numImages = size(pool5_compressed,1);

GTLabels = ones(numImages,1)*-1;

layerLabels = zeros(numImages,1);
trainBinaryLabels1 = zeros(1,numImages);
testBinaryLabels1 = zeros(1,numImages);

gold1 = load(strcat('../Results/goldLayerLabels_alex_',num2str(trainTestSplit)','.mat'));
gold1 = gold1.goldTestLayerLabels;

trainingLayerLabels = kFold_567.derivedLayerLabels;

GTLabels(trainIndx) = trainingLayerLabels; % This has -1 for test data, 1 for pool5, 2 for fc6, and 3 for fc7.
GTLabels(testIndx) = gold1; 

class1LabelIndxs = find(GTLabels== 1);

binaryLabels1 = zeros(1,numImages);

%% Set the class labels for train and test data.
binaryLabels1(class1LabelIndxs) = 1; % pool5 labels

%% Loading PCA features %%
load('../../DeeptiCode/Data/pcaTrain.mat');
load('../../DeeptiCode/Data/pcaTest.mat');

trainNX = pcaTrain;
testNX = pcaTest;


%% train with fc5, those that say no-fc5, test them with fc6.
% trainNX = pool5Features(trainIndx,:);
% testNX = pool5Features(testIndx,:);

[trainNX, testNX] = featureNormalize(trainNX, testNX);

allPrec = [];
allRecall = [];
allAccur = [];
trueClassLabels = binaryLabels1(testIndx);
neigh = 8;
%for neigh = 1:21
    Mdl = fitcknn(trainNX,binaryLabels1(trainIndx),'NumNeighbors',neigh,'NSMethod','exhaustive', 'Distance','cosine');
    
%     loss = resubLoss(Mdl)
% 
%     rng(10); % For reproducibility
%     CVMdl = crossval(Mdl,'KFold',5);
%     kloss = kfoldLoss(CVMdl)
%     
%     allLoss = [allLoss loss];
%     
%     allKLoss = [allKLoss kloss];
    
    predictedClass = predict(Mdl,testNX);
    
    [mPrec, mRecall] = computePrecRecall(trueClassLabels, predictedClass)
    
    allPrec =[allPrec mPrec];
    allRecall =[allRecall mRecall];
    
    allAccur = [allAccur mean(trueClassLabels'== predictedClass)];
    
%end


%
