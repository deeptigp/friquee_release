DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
trainTestSplit = 1;
%% Loading features and labels.
load(strcat(DATA_PATH,'imNetVal_pool5Features_1_50.mat'));

load(strcat('../Data/TrainingNIter_50Class.mat'));
load(strcat('../Data/TestingNIter_50Class.mat'));

load('../Results/kFold_567_1_alex.mat');

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

numImages = size(pool5Features,1);

GTLabels = ones(numImages,1)*-1;

predTestLabels = zeros(length(testIndx),1); % put the predicted labels here..

layerLabels = zeros(numImages,1);
trainBinaryLabels1 = zeros(1,numImages);
testBinaryLabels1 = zeros(1,numImages);

gold1 = load(strcat('../Results/goldLayerLabels_alex_',num2str(trainTestSplit)','.mat'));
gold1 = gold1.goldTestLayerLabels';

trainingLayerLabels = kFold_567.derivedLayerLabels;

GTLabels(trainIndx) = trainingLayerLabels; % This has -1 for test data, 1 for pool5, 2 for fc6, and 3 for fc7.
GTLabels(testIndx) = gold1;

trueTestLabels1 = (gold1 == 1); % test data points with pool5 as the true class labels.

%% train with fc5, those that say no-fc5, test them with fc6.
trainNX = pool5Features(trainIndx,:);
testNX = pool5Features(testIndx,:);

for indx = 1:length(testIndx)
    indx
    % Fetch all the data points, except the current point that we are
    % testing in trainNX
    testFeat = testNX(indx,:);
    trainFeat = [trainNX ; testNX(1:indx-1,:) ; testNX(indx+1:end,:)];
    trainLabels = [trainingLayerLabels ; gold1(1:indx-1,:) ; gold1(indx+1:end,:)];
    %size(trainFeat) 
    %size(trainLabels)
    [IDX,D] = knnsearch(trainFeat, testFeat,'NSMethod','exhaustive','Distance','spearman');

    predTestLabels(indx) = trainLabels(IDX);
end

predLabels1 = (predTestLabels == 1);

%% Get accuracy and prec-recall

accuracy1 = mean(predLabels1 == trueTestLabels1)

[prec1, rec1] = computePrecRecall(trueTestLabels1, predLabels1)