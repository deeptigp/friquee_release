[binaryLabels1,testIndx] = derivedDataLabels(trainTestSplit);

testY = binaryLabels1(testIndx);

minDV5 = min(probEstimates);
maxDV5 = max(probEstimates);

jumpVal5 = 0.5;

layer5Thresh = [minDV5 : jumpVal5 :maxDV5];

numRows  = length(layer5Thresh);

allPrec = [];
allRecall = [];
allAcc = [];

for indx5 = 1:numRows
    
    thresh5 = layer5Thresh(indx5);
    
    newY_hat5(probEstimates <= thresh5) = 0;
    newY_hat5(probEstimates > thresh5) = 1;
    
    allAcc = [allAcc mean(newY_hat5 == testY)];
    [mPrec, mRecall] = computePrecRecall_binary(testY, newY_hat5);
    allPrec = [allPrec mPrec];
    allRecall = [allRecall mRecall];
end