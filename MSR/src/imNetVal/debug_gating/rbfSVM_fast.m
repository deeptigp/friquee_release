function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg] = rbfSVM_fast(trainX, trainY, testX, testY)
    [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg, allPrec, allRecall] = runSVM(trainX, trainY, testX, testY);
end