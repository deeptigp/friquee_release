DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
trainTestSplit = 1;
%% Loading features and labels.
load(strcat(DATA_PATH,'imNetVal_pool5Features_1_50.mat'));
%load(strcat(DATA_PATH,'imNetVal_pool5_compressed_1_50.mat'));

load(strcat('../Data/TrainingNIter_50Class.mat'));
load(strcat('../Data/TestingNIter_50Class.mat'));

load('../Results/kFold_567_1_alex.mat');

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

numImages = size(pool5Features,1);
%numImages = size(pool5_compressed,1);

GTLabels = ones(numImages,1)*-1;

layerLabels = zeros(numImages,1);
trainBinaryLabels1 = zeros(1,numImages);
testBinaryLabels1 = zeros(1,numImages);

gold1 = load(strcat('../Results/goldLayerLabels_alex_',num2str(trainTestSplit)','.mat'));
gold1 = gold1.goldTestLayerLabels;

trainingLayerLabels = kFold_567.derivedLayerLabels;

GTLabels(trainIndx) = trainingLayerLabels; % This has -1 for test data, 1 for pool5, 2 for fc6, and 3 for fc7.

class1LabelIndxs = find(GTLabels== 1);

trueTestLabels1 = (gold1 == 1)'; % test data points with pool5 as the true class labels.
%trueTestLabels2 = (gold1 == 2)';

%trueTestLabels1 = (kFold_567.derivedLayerLabels == 1);

%% Set the class labels for train and test data.
trainBinaryLabels1(class1LabelIndxs) = 1; % train pool5 labels

%% train with fc5, those that say no-fc5, test them with fc6.
trainNX = pool5Features(trainIndx,:);
testNX = pool5Features(testIndx,:);

tStart = tic;
[IDX,D] = knnsearch(trainNX,testNX,'NSMethod','exhaustive','Distance','cosine');
runningTime = toc(tStart)

predTestLabels = kFold_567.derivedLayerLabels(IDX);

%% Get accuracy and prec-recall

predLabels1 = (predTestLabels == 1);
%predLabels2 = (predTestLabels == 2);

accuracy1 = mean(predLabels1 == trueTestLabels1)
%accuracy2 = mean(predLabels2 == trueTestLabels2)


[prec1, rec1] = computePrecRecall(trueTestLabels1, predLabels1)
%[prec2, rec2] = computePrecRecall(trueTestLabels2, predLabels2)