function [accur, rPrec, rRecall,randomClassLabels] =  randMultiClassLabels(gTLabels)

numTestSamples = length(gTLabels);
randomClassLabels = ones(numTestSamples,1);

partition = round(numTestSamples/3);

indx2 = randperm(numTestSamples, 2*partition);

randomClassLabels(indx2(1:partition)) = 2;

randomClassLabels(indx2(partition+1:end)) = 3;

accur = mean(gTLabels == randomClassLabels);

[rPrec, rRecall] = computePrecRecall_multi(gTLabels, randomClassLabels);