function [binaryLabels1, testIndx] = derivedDataLabels(trainTestSplit)
addpath('..');
    DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';

    load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));
    labels = imNetVal_groundTruthLabels_1_50;

    load(strcat('../Data/TrainingNIter_50Class.mat'));
    load(strcat('../Data/TestingNIter_50Class.mat'));

    load('../Results/kFold_567_1_alex.mat');

    trainIndx = TrainingNIter(:,trainTestSplit);
    testIndx = TestingNIter(:,trainTestSplit);

    %% All initializations..

    numImages = length(labels);

    gold1 = load(strcat('../Results/goldLayerLabels_alex_',num2str(trainTestSplit)','.mat'));
    gold1 = gold1.goldTestLayerLabels;

    %1. Step1 - Get the oracle layer labels for the training data.
    GTLabels = ones(numImages,1)*-1;

    trainingLayerLabels = kFold_567.derivedLayerLabels;

    GTLabels(trainIndx) = trainingLayerLabels; % This has -1 for test data, 1 for pool5, 2 for fc6, and 3 for fc7.
    GTLabels(testIndx) = gold1;

    class1LabelIndxs = find(GTLabels== 1);

    binaryLabels1 = zeros(1,numImages);

    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
end