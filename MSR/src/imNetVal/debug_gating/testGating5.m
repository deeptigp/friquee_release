addpath('..');
addpath('/home/deepti/research/MSR/src/include/');

DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
trainTestSplit = 1;
load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));
load(strcat(DATA_PATH,'imNetVal_pool5Features_1_50.mat'));

load(strcat('../Data/TrainingNIter_50Class.mat'));
load(strcat('../Data/TestingNIter_50Class.mat'));

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

trainNX = pool5Features(trainIndx,:);
testNX = pool5Features(testIndx,:);


binaryLabels1 = derivedDataLabels(trainTestSplit);

%% train with fc5, those that say no-fc5, test them with fc6.
allAcc1 = [];
allPrec1 = [];
allRecall1 = [];


%for com = 100:100:1000
% load('../../DeeptiCode/Data/pcaTrain.mat');
% load('../../DeeptiCode/Data/pcaTest.mat');
% 
% trainNX = pcaTrain;
% testNX = pcaTest;

% gtTestLabels = binaryLabels1(testIndx)';
% 
% 
% [accur, rPrec, rRecall,randomClassLabels] =  randClassLabels(gtTestLabels);


%    [allAcc1, testY, Y_hat, runTimeForAllImgs, runTimePerImg, allPrec1, allRecall1] = runSVM(trainNX,binaryLabels1(trainIndx)',testNX, binaryLabels1(testIndx)');
%     allAcc =[allAcc acc];
%     allPrec = [allPrec prec];
%     allRecall = [allRecall recall];
%end

[accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg, mPrec, mRecall, probEstimates] = binarySVM_fast(trainNX,binaryLabels1(trainIndx)',testNX, binaryLabels1(testIndx)');
