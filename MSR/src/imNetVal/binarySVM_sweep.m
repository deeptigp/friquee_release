function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg, probEstimates] = binarySVM_sweep(trainX, trainY, testX, testY, layer)
    % Create a model
    C = '2'; gamma = '1';

    trainOpts = ['-t 0 -s 0 -b 1 -g ',gamma ' -c ',C];

    trainedModel = svmtrain(trainY,double(trainX),trainOpts);

    tStart = tic;

    [Y_hat, medACC , probEstimates] = svmpredict (testY, double(testX), trainedModel, '-b 0 -q');
    runTimeForAllImgs = toc(tStart);

    runTimePerImg = runTimeForAllImgs/length(testY);
   % [mPrec, mRecall] = computePrecRecall(testY, Y_hat);
end