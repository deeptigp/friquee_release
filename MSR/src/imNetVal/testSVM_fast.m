function [medACC, testY, Y_hat, runTimeForAllImgs] = testSVM_fast(testX, testY, layer, trainTestSplit)
    DIRPATH = 'models/10C/';
    load([DIRPATH,'trainedModel_',num2str(trainTestSplit),layer,'_linear.mat']);
    tStart = tic;
    [Y_hat, medACC , ~] = svmpredict (testY, double(testX), trainedModel, '-b 0 -q');
    runTimeForAllImgs = toc(tStart)
end