DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
load(strcat(DATA_PATH,'imNetVal_pool5_compressed_1_50.mat'));

load(strcat(DATA_PATH,'imNetVal_fc6Features_1_50.mat'));

load(strcat(DATA_PATH,'imNetVal_fc7Features_1_50.mat'));

load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));

labels = imNetVal_groundTruthLabels_1_50;

numImages = size(pool5Features,1);

kFold = 5;

foldIndices = crossvalind('Kfold', numImages, kFold);

predictedImNetClassLabels = zeros(numImages,3); % obtained from pool5, fc6, and fc7.

derivedLayerLabels = zeros(numImages,1);

kFold_567 = struct;

for k = 1:kFold
    k
    testIndx = find(foldIndices==k);
    trainIndx = find(foldIndices ~=k);
    
    testLabels = labels(testIndx);
    trainLabels = labels(trainIndx);
    
    [size(testIndx) size(trainIndx)]
    
    [acc5, gt5, pred5] = runSVM_fast(pool5Features(trainIndx,:),trainLabels,pool5Features(testIndx,:),testLabels,'pool5');
    
    [acc6, gt6, pred6] = runSVM_fast(fc6Features(trainIndx,:),trainLabels,fc6Features(testIndx,:),testLabels,'fc6');
    
    [acc7, gt7, pred7] = runSVM_fast(fc7Features(trainIndx,:),trainLabels,fc7Features(testIndx,:),testLabels,'fc7');
    
    predictedImNetClassLabels(testIndx,:) = [(pred5==gt5) (pred6==gt6) (pred7==gt7)];
end

%% Logic to assign class labels pool5 (1), fc6 (2), or fc7 (3)

fc5Indexes = find(predictedImNetClassLabels(:,1));

notFC5Indexes = find(predictedImNetClassLabels(:,1) == 0);

derivedLayerLabels(fc5Indexes) = 1;

fc6Indexes = notFC5Indexes(predictedImNetClassLabels(notFC5Indexes,2) == 1);

derivedLayerLabels(fc6Indexes) = 2;

notFC6Indexes = notFC5Indexes(predictedImNetClassLabels(notFC5Indexes,2) == 0);

fc7Indexes = notFC6Indexes(predictedImNetClassLabels(notFC6Indexes,3)==1);

derivedLayerLabels(fc7Indexes) = 3;

%% There still are a few data points which were all classified incorrectly by all three layers.. As of now, I am assigning them the label pool5
notFC7Indexes = notFC6Indexes(predictedImNetClassLabels(notFC6Indexes,3)==0);

derivedLayerLabels(notFC7Indexes) = 1;


kFold_567.foldIndices = foldIndices;
kFold_567.predictedImNetClassLabels = predictedImNetClassLabels;
kFold_567.derivedLayerLabels = derivedLayerLabels;
kFold_567.doesNotMatter = notFC7Indexes;
save(strcat('Results/kFold_567_1_50_k5.mat'),'kFold_567');

