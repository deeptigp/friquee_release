function [medACC, testY, Y_hat, runTimeForAllImgs, meanPrec, meanRec] = runSVM_fast(trainX, trainY, testX, testY, layer,trainTestSplit)
    % Create a model
    meanPrec = 0;
    meanRec  = 0;
    
    gamma = '0.0020';%2^-9;
    if(strcmp(layer,'pool5'))
        C = '2';
    elseif(strcmp(layer,'fc6'))
        C = '0.25';
    elseif(strcmp(layer,'fc7') || strcmp(layer,'conv4'))
        C = '0.25';
    end
    
    trainOpts = ['-t 0 -s 0 -b 0 -g ',gamma ' -c ',C];
    
    %[trainX, testX] = featScale(trainX, testX);
    
    
    trainedModel = svmtrain(trainY,double(trainX),trainOpts);
    
 %   save(['models/trainedModel_',num2str(trainTestSplit),layer,'_linear.mat'],'trainedModel');
    
    tStart = tic;
    [Y_hat, medACC , ~] = svmpredict (testY, double(testX), trainedModel, '-b 0 -q');
    runTimeForAllImgs = toc(tStart);
 %   [meanPrec, meanRec] = computePrecRecall(testY, Y_hat);
end