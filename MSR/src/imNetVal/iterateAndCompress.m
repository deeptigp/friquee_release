% This script varies the K values and compresses the features accordingly.
DIR_PATH = '/media/deepti/J/MSR/imNetVal/';
load(strcat(DIR_PATH,'Data/VGGNet_pool5Features_1-50.mat'));

%% == labels == %%
load(strcat(DIR_PATH,'Data/imNetVal_groundTruthLabels_1_50.mat'));

groundTruthLabels = imNetVal50ClassLabels;

kVals = [1000:1000: 25000];

perfValues2 = [];
runningTimes2= [];

for K = kVals
    fprintf('Compressing features to dimension %d \n', K);
    Y = compressData(pool5Features, K);
    [medACC5, testY, Y_hat5, runTimeForAllImgs5] = executeSVM(Y, groundTruthLabels, 2, 'Data/TrainingNIter_50Class.mat','Data/TestingNIter_50Class.mat', 'pool5');
    perfValues2 = [perfValues2 ; medACC5(1)];
    runningTimes2 = [runningTimes2; runTimeForAllImgs5];
end

plot(kVals, perfValues2,'rX-');

xlabel('Dimension to which the pool5 was compressed to');
ylabel('Mean accuracy');