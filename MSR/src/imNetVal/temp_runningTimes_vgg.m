featComputeTime = [ 1.39 0.13 0.02];
perLayerFeatComputeTime = [1.53 1.51 1.38 1.39]; % fc7-fc6-pool5
totalClassificationTime =0;

totalTestImgs = size(cascadeClassifierResults_aggre567_BSVC_pool5.testIndx,1);

numDataForBC = [size(cascadeClassifierResults_aggre567_BSVC_pool5.testIndx,1) size(cascadeClassifierResults_aggre567_BSVC_fc6.testIndx,1) size(cascadeClassifierResults_aggre567_BSVC_fc7.testIndx,1)];

totalClassificationTime = cascadeClassifierResults_aggre567_BSVC_pool5.runTimeForAllImgs + cascadeClassifierResults_aggre567_BSVC_fc6.runTimeForAllImgs;
if(exist('cascadePredictedResults_aggre567_BSVC_fc7'))
    totalRegressionTime = cascadePredictedResults_aggre567_BSVC_pool5.runTimeForAllImgs5 + cascadePredictedResults_aggre567_BSVC_fc6.runTimeForAllImgs6 + cascadePredictedResults_aggre567_BSVC_fc7.runTimeForAllImgs7;
else
    totalRegressionTime = cascadePredictedResults_aggre567_BSVC_pool5.runTimeForAllImgs5 + cascadePredictedResults_aggre567_BSVC_fc6.runTimeForAllImgs6;
end

totalFeatureComputeTime = sum(numDataForBC.*featComputeTime);

totalTestTime = totalClassificationTime + totalRegressionTime + totalFeatureComputeTime;

individualFeatCompTime = totalTestImgs.*perLayerFeatComputeTime;
    
[totalClassificationTime totalRegressionTime ]