%vggNetFeats_imNetVal50C_4-7_',str(iter),'.npz'
%% I'll wait for the files to get generated before writing down the code for loading them.

DIR_PATH = '/media/deepti/J/MSR/imNetVal/';
load(strcat(DIR_PATH,'Data/VGGNet_fc7Features_1-50.mat'));
load(strcat(DIR_PATH,'Data/VGGNet_fc6Features_1-50.mat'));
load(strcat(DIR_PATH,'Data/VGGNet_pool5Features_1-50.mat'));
load(strcat(DIR_PATH,'Data/VGGNet_pool5Features_compressed_1-50.mat'));

%% == labels == %%
load(strcat(DIR_PATH,'Data/imNetVal_groundTruthLabels_1_50.mat'));

groundTruthLabels = imNetVal50ClassLabels;
    

[medACC7, testY, Y_hat7, runTimeForAllImgs7] = executeSVM(fc7Features,groundTruthLabels, 1, 'Data/TrainingNIter_50Class.mat', 'Data/TestingNIter_50Class.mat', 'fc7')
[medACC6, testY, Y_hat6, runTimeForAllImgs6] = executeSVM(fc6Features,groundTruthLabels, 1, 'Data/TrainingNIter_50Class.mat', 'Data/TestingNIter_50Class.mat', 'fc6')
[medACC5, testY, Y_hat5, runTimeForAllImgs5] = executeSVM(pool5Features,groundTruthLabels, 1, 'Data/TrainingNIter_50Class.mat', 'Data/TestingNIter_50Class.mat', 'pool5')

%[medACC15, testY, Y_hat5, runTimeForAllImgs5] = executeSVM(pool5Features,groundTruthLabels, 1, 'Data/TrainingNIter_50Class.mat', 'Data/TestingNIter_50Class.mat', 'pool5')


perLayerResults = struct;

perLayerResults.medACC7 = medACC7;
perLayerResults.Y_hat7 = Y_hat7;
perLayerResults.runTimeForAllImgs7 = runTimeForAllImgs7;

perLayerResults.medACC6 = medACC6;
perLayerResults.Y_hat6 = Y_hat6;
perLayerResults.runTimeForAllImgs6 = runTimeForAllImgs6;


perLayerResults.medACC5 = medACC5;
perLayerResults.Y_hat5 = Y_hat5;
perLayerResults.runTimeForAllImgs5 = runTimeForAllImgs5;


