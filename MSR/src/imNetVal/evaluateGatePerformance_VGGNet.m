%%Performance of the gating functions..
function evaluateGatePerformance_VGGNet(trainTestSplit)
numImages = 2500;
DATA_PATH = '/media/deepti/J/MSR/fineGrained/Data/';

load(strcat('Data/TestingNIter_50Class.mat'));

testInd = (TestingNIter(:,trainTestSplit));

layerLabels = zeros(numImages,1);

gold1 = load(strcat('Results/goldLayerLabels_vgg_',num2str(trainTestSplit)','.mat'));
gold1 = gold1.goldTestLayerLabels;

layerLabels(testInd) = gold1;

class1LabelIndxs = find(layerLabels== 1);
class2LabelIndxs = find(layerLabels== 2);
class3LabelIndxs = find(layerLabels== 3);

binaryLabels1 = zeros(1,numImages);
binaryLabels2 = zeros(1,numImages);
binaryLabels3 = zeros(1,numImages);

%% Set the class labels for train and test data.
binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
binaryLabels2(class2LabelIndxs) = 1; % fc6 labels
binaryLabels3(class3LabelIndxs) = 1; % fc7 labels


cascadeClassifierResults_aggre567_BSVC_pool5 = load(strcat('Results/aggregateResults/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_pool5.mat'));
cascadeClassifierResults_aggre567_BSVC_pool5 = cascadeClassifierResults_aggre567_BSVC_pool5.cascadeClassifierResults_aggre567_BSVC_pool5;

cascadeClassifierResults_aggre567_BSVC_pool5.accuracy = mean(cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat == binaryLabels1(cascadeClassifierResults_aggre567_BSVC_pool5.testIndx)');
[prec, rec] = computePrecRecall(binaryLabels1(cascadeClassifierResults_aggre567_BSVC_pool5.testIndx)', cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat);

cascadeClassifierResults_aggre567_BSVC_pool5.mPrec = prec;
cascadeClassifierResults_aggre567_BSVC_pool5.mRecall = rec;


cascadeClassifierResults_aggre567_BSVC_fc6 = load('Results/aggregateResults/1/cascadeClassifierResults_aggre567_BSVC_fc6.mat');
cascadeClassifierResults_aggre567_BSVC_fc6 = cascadeClassifierResults_aggre567_BSVC_fc6.cascadeClassifierResults_aggre567_BSVC_fc6;

cascadeClassifierResults_aggre567_BSVC_fc6.accuracy = mean(cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat == binaryLabels1(cascadeClassifierResults_aggre567_BSVC_fc6.testIndx)');
[prec, rec] = computePrecRecall(binaryLabels1(cascadeClassifierResults_aggre567_BSVC_fc6.testIndx)', cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat);

cascadeClassifierResults_aggre567_BSVC_fc6.mPrec = prec;
cascadeClassifierResults_aggre567_BSVC_fc6.mRecall = rec;

save(strcat('Results/aggregateResults/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_pool5.mat'),'cascadeClassifierResults_aggre567_BSVC_pool5');
save(strcat('Results/aggregateResults/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_fc6.mat'),'cascadeClassifierResults_aggre567_BSVC_fc6');

end