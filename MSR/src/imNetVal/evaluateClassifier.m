%% This method runs an SVR on the set of datapoints that got classified to have the current layer as the label. This is just to evaluate if the binary classifier is doing a good job.. For ex: If 20 data points got the label as pool5, we use pool5Features to predict their interestingness scores in this method.
function predictedResults = evaluateClassifier(classifierResults, layer,trainTestSplit)
DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));

labels = imNetVal_groundTruthLabels_1_50;

predictedResults = struct;

%% Evaluation of the goodness of this prediction

% Lets take the 80-20 train test splits.. For all the images where fc5 was
% the predicted class, use class 1, fc6 features for class 2 and fc7 for
% class 3.

%%
class1Indices = classifierResults.testIndx(classifierResults.Y_hat==1);
trainIndx = classifierResults.trainIndx;



% Lets train SVCs
if(strcmp(layer,'pool5'))
    disp('pool5')
    load(strcat(DATA_PATH,'imNetVal_pool5Features_1_50.mat'));
    %load(strcat(DATA_PATH,'dogs_pool5Features_1-25.mat'));
    [medACC, testY, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(pool5Features(trainIndx,:),labels(trainIndx),pool5Features(class1Indices,:),labels(class1Indices),'pool5',trainTestSplit);
    predictedResults.medACC = medACC;
    predictedResults.ePred5 = Y_hat;
    predictedResults.runTimeForAllImgs5 = runTimeForAllImgs;
    
elseif(strcmp(layer,'fc6'))
    disp('fc6')
    load(strcat(DATA_PATH,'imNetVal_fc6Features_1_50.mat'));
    %load(strcat(DATA_PATH,'dogs_fc6Features_1-25.mat'));
    [medACC, testY, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(fc6Features(trainIndx,:),labels(trainIndx), fc6Features(class1Indices,:),labels(class1Indices),'fc6',trainTestSplit);
    predictedResults.medACC = medACC;
    predictedResults.ePred6 = Y_hat;
    predictedResults.runTimeForAllImgs6 = runTimeForAllImgs;
    
elseif(strcmp(layer,'fc7'))
    disp('fc7')
    load(strcat(DATA_PATH,'imNetVal_fc7Features_1_50.mat'));
    
    %load(strcat(DATA_PATH,'dogs_fc7Features_1-25.mat'));
    [medACC, testY, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(fc7Features(trainIndx,:),labels(trainIndx), fc7Features(class1Indices,:),labels(class1Indices),'fc7',trainTestSplit);
    predictedResults.medACC = medACC;
    predictedResults.ePred7 = Y_hat;
    predictedResults.runTimeForAllImgs7 = runTimeForAllImgs;
end

predictedResults.class1Indices = class1Indices;
predictedResults.eGT = labels(class1Indices);
end