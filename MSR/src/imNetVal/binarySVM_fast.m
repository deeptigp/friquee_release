function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg, mPrec, mRecall, probEstimates] = binarySVM_fast(trainX, trainY, testX, testY, layer)
    trainOpts = ['-t 0 -s 0 -b 0 '];

    trainedModel = svmtrain(trainY,double(trainX),trainOpts);

    tStart = tic;

    [Y_hat, medACC , probEstimates] = svmpredict (testY, double(testX), trainedModel, '-b 0');
    runTimeForAllImgs = toc(tStart);

    runTimePerImg = runTimeForAllImgs/length(testY);
    [mPrec, mRecall] = computePrecRecall_binary(testY, Y_hat);
end