function randArray = generateRandArray(trainTestSplit)
   trainTestSplit = 1;
   load(strcat('Results/goldLayerLabels_vgg_',num2str(trainTestSplit),'.mat'));
   num5 = sum(goldTestLayerLabels == 1);
   num6 = sum(goldTestLayerLabels == 2);
   num7 = sum(goldTestLayerLabels == 3);
   
   numTestImages = num5+num6+num7;
   
   randArray = zeros(1,numTestImages);
   
   indx = randperm(numTestImages);
   
   randArray(indx(1:num5)) = 1;
   
   randArray(indx(num5+1:num5+num6)) = 2;
   
   randArray(indx(num5+num6+1:end)) = 3;
end