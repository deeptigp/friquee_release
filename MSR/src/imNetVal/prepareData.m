dirPath = '/media/deepti/SamsungSSD/ImageNet/FineGrained/release/';

load(strcat(dirPath,'dogImgData.mat'));

% Lets try loading only 10 classes to start with..

dogClassLabels = dogImgData.dogClassLabels;

startClassLabel  = 380%dogClassLabels(1)+10;

lastClassLabel = 404;%startClassLabel + 39;

indxs = ((dogClassLabels>=startClassLabel)  & (dogClassLabels <=lastClassLabel));

imgData = dogImgData.dog_images(indxs);

dogSubClassLabels = dogClassLabels(indxs);


dogSubClassData_25C  = struct;
dogSubClassData_25C.dogSubClassLabels = dogSubClassLabels;
dogSubClassData_25C.imgData = imgData;

save('Data/dogSubClassData_25C.mat','dogSubClassData_25C');

groundTruthLabels = dogSubClassLabels;
save('Data/dogs_groundTruthLabels_1-25.mat','groundTruthLabels');