%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7
function aggregateResults_BSVC = random_cascade_VGG(trainTestSplit)
%    trainTestSplit = 1;
    DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
    load(strcat(DATA_PATH,'VGGNet_fc7Features_1-50.mat'));
    load(strcat(DATA_PATH,'VGGNet_fc6Features_1-50.mat'));
    load(strcat(DATA_PATH,'VGGNet_pool5Features_compressed_1-50.mat'));

    %% == labels == %%
    load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));

    labels = imNetVal50ClassLabels;

    load(strcat('Data/TestingNIter_50Class.mat'));

    load(strcat('Data/TrainingNIter_50Class.mat'));

    testIndx = (TestingNIter(:,trainTestSplit));
    trainIndx = (TrainingNIter(:,trainTestSplit));


    %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);

    cascadeClassifierResults_aggre567_BSVC_pool5 = struct;
    cascadeClassifierResults_aggre567_BSVC_fc6 = struct;
    cascadeClassifierResults_aggre567_BSVC_fc7 = struct;

    % Generate a random train/test split.
    numImages = length(labels);


    binaryLabels1 = zeros(1,length(testIndx));
    indx = randperm(length(testIndx),floor(length(testIndx)/2));
    binaryLabels1(indx) = 1;
    
    sum(binaryLabels1)
    
    cascadeClassifierResults_aggre567_BSVC_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_pool5.testIndx = testIndx;
    cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat = binaryLabels1';
    cascadeClassifierResults_aggre567_BSVC_pool5.runTimeForAllImgs = 0;


    cascadePredictedResults_aggre567_BSVC_pool5 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_pool5, 'pool5',trainTestSplit, pool5Features_compressed);

    %% Aggregating the predictions of the data points that were predicted to belong to pool5 class..
    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_pool5.class1Indices) = cascadePredictedResults_aggre567_BSVC_pool5.ePred5;


    %% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_BSVC_pool5.testIndx(cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat==0);

    fprintf('Number of not fc5 images: %d',length(notFC5Indxs));

    binaryLabels2 = zeros(1,length(notFC5Indxs));
    indx = randperm(length(notFC5Indxs),floor(length(notFC5Indxs)/2));
    binaryLabels2(indx) = 1;
    
    cascadeClassifierResults_aggre567_BSVC_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat = binaryLabels2';
    cascadeClassifierResults_aggre567_BSVC_fc6.runTimeForAllImgs = 0;

    cascadePredictedResults_aggre567_BSVC_fc6 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc6, 'fc6',trainTestSplit, fc6Features);

    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc6.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc6.ePred6;

    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre567_BSVC_fc6.testIndx(cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat==0);

    fprintf('Number of not fc6 images: %d',length(notFC6Indxs));

    Y_hat = ones(size(notFC6Indxs));

    cascadeClassifierResults_aggre567_BSVC_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc7.Y_hat = Y_hat;

    cascadePredictedResults_aggre567_BSVC_fc7 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc7, 'fc7',trainTestSplit, fc7Features);

    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc7.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc7.ePred7;

    nonZeroPredictedScores = aggregatePredictedScores(testIndx);
    aggregateResults_BSVC.predictedScores = nonZeroPredictedScores;

    aggreACC = mean(nonZeroPredictedScores ==labels(testIndx));

    aggregateResults_BSVC.aggreACC = aggreACC;
    [agPrec, agRecall] = computePrecRecall(labels(testIndx),nonZeroPredictedScores);

    aggregateResults_BSVC.aggrPrec = agPrec;
    aggregateResults_BSVC.aggrRecall = agRecall;

    fprintf('Mean correlation between the aggregated predictions and GT predictions %f %f\n',aggreACC);

    temp_runningTimes_vgg;
    aggregateResults_BSVC.totalTestTime = totalTestTime;
    aggregateResults_BSVC.totalClassificationTime = totalClassificationTime;
    aggregateResults_BSVC.totalFeatureComputeTime = totalFeatureComputeTime;
    aggregateResults_BSVC.totalRegressionTime = totalRegressionTime;
% save(strcat('Results/aggregateResults/',num2str(trainTestSplit),'/aggregatePredictedScores_BSVC_fc567.mat'),'aggregateResults_BSVC');
