DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
%load(strcat(DATA_PATH,'imNetVal_pool5Features_1_50.mat'));
load(strcat(DATA_PATH,'VGGNet_pool5Features_1-50.mat'));
  load(strcat('Data/TestingNIter_50Class.mat'));
  runTimeForAllImgs = 0;
for split = 1:3
    testIndx = find(TestingNIter(:,split));
    X = pool5Features(testIndx,:);
    size(X)
    tStart = tic;
    Y = compressData(X,4096); 
    runTimeForAllImgs = runTimeForAllImgs + toc(tStart);
end

(runTimeForAllImgs/3)/500