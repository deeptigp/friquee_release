trainTestSplit = 1;
DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
load(strcat(DATA_PATH,'VGGNet_fc7Features_1-50.mat'));
load(strcat(DATA_PATH,'VGGNet_fc6Features_1-50.mat'));
load(strcat(DATA_PATH,'VGGNet_pool5Features_compressed_1-50.mat'));

%% == labels == %%
load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));
load(strcat('Data/TrainingNIter_50Class.mat'));
load(strcat('Data/TestingNIter_50Class.mat'));
labels = imNetVal50ClassLabels;

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

% Generate a random train/test split.
numImages = length(labels);

load('Results/kFold_567_1_vgg.mat');
% Get the derived class labels.
GTLabels = ones(numImages,1)*-1;
GTLabels(trainIndx) = kFold_567.derivedLayerLabels;

%% Get the label indexes.
class1LabelIndxs = find(GTLabels== 1);
class2LabelIndxs = find(GTLabels== 2);
class3LabelIndxs = find(GTLabels== 3);

binaryLabels1 = zeros(1,numImages);
binaryLabels2 = zeros(1,numImages);
binaryLabels3 = zeros(1,numImages);

%% Set the class labels for train and test data.
binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
binaryLabels2(class2LabelIndxs) = 1; % fc6 labels
binaryLabels3(class3LabelIndxs) = 1; % fc7 labels


% %% train with fc5, those that say no-fc5, test them with fc6.
trainNX = pool5Features_compressed(trainIndx,:);
testNX = pool5Features_compressed(testIndx,:);

trainNY = binaryLabels1(trainIndx)';
testNY = binaryLabels1(testIndx)';

[accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg, probEstimates5] = binarySVM_sweep(trainNX, trainNY, testNX, testNY);

cascadeClassifierResults_aggre567_BSVC_pool5 = struct;
cascadeClassifierResults_aggre567_BSVC_pool5.trainIndx = trainIndx;
cascadeClassifierResults_aggre567_BSVC_pool5.testIndx = testIndx;
cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat = Y_hat;
cascadeClassifierResults_aggre567_BSVC_pool5.accuracy = accuracy;
cascadeClassifierResults_aggre567_BSVC_pool5.runTimeForAllImgs = runTimeForAllImgs;

minDV5 = min(probEstimates5);
maxDV5 = max(probEstimates5);

jumpVal5 = 0.4;

minDV6 = -2.12;
maxDV6 = 4.59;
jumpVal6 = 0.6;

layer5Thresh = [minDV5 : jumpVal5 :maxDV5];

layer6Thresh = [minDV6 : jumpVal6 :maxDV6];

numRows = length(layer5Thresh);
numCols = length(layer6Thresh);

sumNewYhat = [];
allAccur = zeros(numRows, numCols);
allRuntimes = zeros(numRows, numCols);

numFC5Imgs = zeros(numRows, numCols);
numFC6Imgs = zeros(numRows, numCols);

findMinDV6 = 10000;
findMaxDV6 = -1000;

for indx5 = 1:numRows
    
    thresh5 = layer5Thresh(indx5);
    
    %% Fix the Y_hat's according to the thresholds
    
    newY_hat5(probEstimates5 <= thresh5) = 0;
    newY_hat5(probEstimates5 > thresh5) = 1;
    
    cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat = newY_hat5;
       
    cascadePredictedResults_aggre567_BSVC_pool5 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_pool5, 'pool5',trainTestSplit, pool5Features_compressed);
    
    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_pool5.class1Indices) = cascadePredictedResults_aggre567_BSVC_pool5.ePred5;
    
    numFC5Imgs(indx5,:) = length(cascadePredictedResults_aggre567_BSVC_pool5.ePred5);
    
    %% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_BSVC_pool5.testIndx(cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat==0);
    
    fprintf('Number of not fc5 images: %d',length(notFC5Indxs));
    
    
    trainNX = fc6Features(trainIndx,:);
    testNX = fc6Features(notFC5Indxs,:);
    
    trainNY = binaryLabels2(trainIndx)';
    testNY = binaryLabels2(notFC5Indxs)';
    
    [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg, probEstimates6] = binarySVM_sweep(trainNX, trainNY, testNX, testNY);
    cascadeClassifierResults_aggre567_BSVC_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_BSVC_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre567_BSVC_fc6.runTimeForAllImgs = runTimeForAllImgs;
    
    if(findMinDV6 > min(probEstimates6(:)))
        findMinDV6  = min(probEstimates6(:));
    end
    
    if(findMaxDV6 < max(probEstimates6(:)))
        findMaxDV6  = max(probEstimates6(:));
    end
    
    for indx6 = 1:numCols
        
        thresh6 = layer6Thresh(indx6);
        
        newY_hat6(probEstimates6 <= thresh6) = 0;
        newY_hat6(probEstimates6 > thresh6) = 1;
        
        numFC6Imgs(indx5,indx6) = sum(newY_hat6);
        
        cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat = newY_hat6;
        
        cascadePredictedResults_aggre567_BSVC_fc6 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc6, 'fc6',trainTestSplit, fc6Features);
        
        aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc6.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc6.ePred6;
        
        %% Those rejected by fc6 will be considered by fc7.
        notFC6Indxs = cascadeClassifierResults_aggre567_BSVC_fc6.testIndx(cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat==0);
        
        fprintf('Number of not fc6 images: %d\n',length(notFC6Indxs));
        
        Y_hat = ones(size(notFC6Indxs));
        
        cascadeClassifierResults_aggre567_BSVC_fc7.trainIndx = trainIndx;
        cascadeClassifierResults_aggre567_BSVC_fc7.testIndx = notFC6Indxs;
        cascadeClassifierResults_aggre567_BSVC_fc7.Y_hat = Y_hat;
        
        fprintf('Current threshold setting of layers 6 and 5 %f %f\n',layer6Thresh(indx6), layer5Thresh(indx5));
        
        if(~isempty(notFC6Indxs))
            cascadePredictedResults_aggre567_BSVC_fc7 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc7, 'fc7',trainTestSplit, fc7Features);
            
            aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc7.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc7.ePred7;
        end
        
        nonZeroPredictedScores = aggregatePredictedScores(testIndx);
        aggreACC = mean(nonZeroPredictedScores ==labels(testIndx)');
        
        aggregateResults_BSVC.predictedScores = nonZeroPredictedScores;
        aggregateResults_BSVC.aggreACC = aggreACC;
        
        
        fprintf('Mean correlation between the aggregated predictions and GT predictions %f %f\n',aggreACC);
        
        allAccur(indx5, indx6) = aggreACC;
        
        temp_runningTimes_vgg;
        
        allRuntimes(indx5, indx6) = totalTestTime;
        
    end
end


