function getOracleLayerLabels(isAlex,trainTestSplit)

DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
load(strcat('Data/TrainingNIter_50Class.mat'));
load(strcat('Data/TestingNIter_50Class.mat'));

if(isAlex)
    load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));
    labels = imNetVal_groundTruthLabels_1_50;
    
    load(strcat(DATA_PATH,'imNetVal_pool5_compressed_1_50.mat'));
    load(strcat(DATA_PATH,'imNetVal_fc6Features_1_50.mat'));
    load(strcat(DATA_PATH,'imNetVal_fc7Features_1_50.mat'));
    
    layer5Features = pool5_compressed;
    
else
    load(strcat(DATA_PATH,'VGGNet_fc7Features_1-50.mat'));
    load(strcat(DATA_PATH,'VGGNet_fc6Features_1-50.mat'));
    load(strcat(DATA_PATH,'VGGNet_pool5Features_compressed_1-50.mat'));
    
    load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));
    labels = imNetVal_groundTruthLabels_1_50;
    
    layer5Features = pool5Features_compressed;
end

% Get the current train/test split.
trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

perClassLabels = zeros(length(testIndx),3);


[acc5, gt5, pred5] = runSVM_fast(layer5Features(trainIndx,:),labels(trainIndx),layer5Features(testIndx,:),labels(testIndx),'pool5');

[acc6, gt6, pred6] = runSVM_fast(fc6Features(trainIndx,:),labels(trainIndx),fc6Features(testIndx,:),labels(testIndx),'fc6');

[acc7, gt7, pred7] = runSVM_fast(fc7Features(trainIndx,:),labels(trainIndx),fc7Features(testIndx,:),labels(testIndx),'fc7');

perClassLabels(:,:) = [(pred5==gt5) (pred6==gt6) (pred7==gt7)];


%% Logic to assign class labels pool5 (1), fc6 (2), or fc7 (3)

fc5Indexes = find(perClassLabels(:,1)==1);

goldTestLayerLabels(fc5Indexes) = 1;

fc6Indexes = find(perClassLabels(:,1)==0 & perClassLabels(:,2)== 1);

goldTestLayerLabels(fc6Indexes) = 2;

fc7Indexes = find(perClassLabels(:,1)==0 & perClassLabels(:,2)== 0 & perClassLabels(:,3)== 1);

goldTestLayerLabels(fc7Indexes) = 3;

%% There still are a few data points which were all classified incorrectly by all three layers.. As of now, I am assigning them the label pool5
dontMatter = find(perClassLabels(:,1)==0 & perClassLabels(:,2)== 0 & perClassLabels(:,3)== 0);

goldTestLayerLabels(dontMatter) = 1;

if(isAlex)
    save(strcat('Results/goldLayerLabels_alex_',num2str(trainTestSplit),'.mat'),'goldTestLayerLabels');
else
    save(strcat('Results/goldLayerLabels_vgg_',num2str(trainTestSplit),'.mat'),'goldTestLayerLabels');
end
end