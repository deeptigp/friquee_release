%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7
function cascade_SVM_SVM_AlexNet(trainTestSplit)
    DATA_PATH = '/media/deepti/J/MSR/imNetVal/Data/';
    load(strcat(DATA_PATH,'imNetVal_groundTruthLabels_1_50.mat'));
    labels = imNetVal_groundTruthLabels_1_50;


    load('Results/kFold_567_1_50_k5.mat');

    load(strcat(DATA_PATH,'imNetVal_pool5_compressed_1_50.mat'));

    load(strcat(DATA_PATH,'imNetVal_fc6Features_1_50.mat'));

    load(strcat(DATA_PATH,'imNetVal_fc7Features_1_50.mat'));

    load(strcat('Data/TrainingNIter_50Class.mat'));
    load(strcat('Data/TestingNIter_50Class.mat'));

    
    trainIndx = TrainingNIter(:,trainTestSplit);
    testIndx = TestingNIter(:,trainTestSplit);

    %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);

    cascadeClassifierResults_aggre567_BSVC_pool5 = struct;
    cascadeClassifierResults_aggre567_BSVC_fc6 = struct;
    cascadeClassifierResults_aggre567_BSVC_fc7 = struct;

    % Generate a random train/test split.
    numImages = length(labels);

    % Get the derived class labels.
    GTLabels = kFold_567.derivedLayerLabels;

    %% Get the label indexes.
    class1LabelIndxs = find(GTLabels== 1);
    class2LabelIndxs = find(GTLabels== 2);
    class3LabelIndxs = find(GTLabels== 3);

    binaryLabels1 = zeros(1,numImages);
    binaryLabels2 = zeros(1,numImages);
    binaryLabels3 = zeros(1,numImages);

    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
    binaryLabels2(class2LabelIndxs) = 1; % fc6 labels
    binaryLabels3(class3LabelIndxs) = 1; % fc7 labels


    %% train with fc5, those that say no-fc5, test them with fc6.
    trainNX = pool5_compressed(trainIndx,:);
    testNX = pool5_compressed(testIndx,:);

    [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg,mPrec, mRecall] = binarySVM_fast(trainNX, binaryLabels1(trainIndx)',testNX, binaryLabels1(testIndx)');

    cascadeClassifierResults_aggre567_BSVC_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_pool5.testIndx = testIndx;
    cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_BSVC_pool5.accuracy = accuracy;
    cascadeClassifierResults_aggre567_BSVC_pool5.runTimeForAllImgs = runTimeForAllImgs;
 cascadeClassifierResults_aggre567_BSVC_pool5.mPrec = mPrec;
    cascadeClassifierResults_aggre567_BSVC_pool5.mRecall = mRecall;


    save(strcat('Results/aggregateResults_alexNet/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_pool5.mat'),'cascadeClassifierResults_aggre567_BSVC_pool5');

    cascadePredictedResults_aggre567_BSVC_pool5 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_pool5, 'pool5',trainTestSplit, pool5_compressed);

    save(strcat('Results/aggregateResults_alexNet/',num2str(trainTestSplit),'/cascadePredictedResults_aggre567_BSVC_pool5.mat'),'cascadePredictedResults_aggre567_BSVC_pool5');

    %% Aggregating the predictions of the data points that were predicted to belong to pool5 class..
    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_pool5.class1Indices) = cascadePredictedResults_aggre567_BSVC_pool5.ePred5;


    %% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_BSVC_pool5.testIndx(cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat==0);

    fprintf('Number of not fc5 images: %d',length(notFC5Indxs));

    fc6Compressed = compressData(fc6Features, 3000);

    trainNX = fc6Compressed(trainIndx,:);
    testNX = fc6Compressed(notFC5Indxs,:);


    [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg,mPrec, mRecall] = binarySVM_fast(trainNX, binaryLabels2(trainIndx)', testNX, binaryLabels2(notFC5Indxs)');

    cascadeClassifierResults_aggre567_BSVC_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_BSVC_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre567_BSVC_fc6.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre567_BSVC_fc6.mPrec = mPrec;
    cascadeClassifierResults_aggre567_BSVC_fc6.mRecall = mRecall;

    save(strcat('Results/aggregateResults_alexNet/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_fc6.mat'),'cascadeClassifierResults_aggre567_BSVC_fc6');

    cascadePredictedResults_aggre567_BSVC_fc6 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc6, 'fc6',trainTestSplit, fc6Compressed);

    save(strcat('Results/aggregateResults_alexNet/',num2str(trainTestSplit),'/cascadePredictedResults_aggre567_BSVC_fc6.mat'),'cascadePredictedResults_aggre567_BSVC_fc6');

    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc6.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc6.ePred6;

    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre567_BSVC_fc6.testIndx(cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat==0);

    fprintf('Number of not fc6 images: %d',length(notFC6Indxs));

    Y_hat = ones(size(notFC6Indxs));

    cascadeClassifierResults_aggre567_BSVC_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc7.Y_hat = Y_hat;

    save(strcat('Results/aggregateResults_alexNet/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_fc7.mat'),'cascadeClassifierResults_aggre567_BSVC_fc7');

    cascadePredictedResults_aggre567_BSVC_fc7 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc7, 'fc7',trainTestSplit, fc7Features);

    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc7.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc7.ePred7;
    save(strcat('Results/aggregateResults_alexNet/',num2str(trainTestSplit),'/cascadePredictedResults_aggre567_BSVC_fc7.mat'),'cascadePredictedResults_aggre567_BSVC_fc7');


    nonZeroPredictedScores = aggregatePredictedScores(testIndx);
    aggreACC = mean(nonZeroPredictedScores ==labels(testIndx));

    aggregateResults_BSVC.predictedScores = nonZeroPredictedScores;
    aggregateResults_BSVC.aggreACC = aggreACC;
    [agPrec, agRecall] = computePrecRecall(labels(testIndx),nonZeroPredictedScores);

    aggregateResults_BSVC.aggrPrec = agPrec;
    aggregateResults_BSVC.aggrRecall = agRecall;
    
    temp_runningTimes_alexNet;
     aggregateResults_BSVC.totalTestTime = totalTestTime;
    aggregateResults_BSVC.totalClassificationTime = totalClassificationTime;
    aggregateResults_BSVC.totalFeatureComputeTime = totalFeatureComputeTime;
    aggregateResults_BSVC.totalRegressionTime = totalRegressionTime;
    
    save(strcat('Results/aggregateResults_alexNet/',num2str(trainTestSplit),'/aggregatePredictedScores_BSVC_fc567.mat'),'aggregateResults_BSVC');

    fprintf('Mean correlation between the aggregated predictions and GT predictions %f %f\n',aggreACC);
    
end