function I = extractCenterImage(I,desiredDim)
    [row,col,~] = size(I);
    xmin = round((row -desiredDim)/2)+1;
    ymin = round((col -desiredDim)/2)+1;
    
    I = I(xmin: xmin+desiredDim-1, ymin:ymin+desiredDim-1,:);
end