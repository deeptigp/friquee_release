% This script resizes every image to 256 X 256 as AlexNet takes in a 256X256 image.

%dirName = 'D:\ImageData\coco\images\train2014\';
dirName = '/media/deepti/SamsungSSD/ImageNet/FineGrained/';
saveDirName = '/media/deepti/SamsungSSD/ImageNet/FineGrained/train_images/dogImages_10Class/';

load('/home/deepti/research/MSR/src/fineGrained/Data/dogSubClassData.mat');

desiredDim = 256;

imgList = dogSubClassData.imgData;

for i=1766:length(imgList)
    i
    % get the dimensions of the image.
    a = imfinfo(strcat(dirName,imgList{i}));
    
    if(strcmp(a.ColorType,'truecolor')~=0)
        I = imread(strcat(dirName,imgList{i}));
    else
       % I1 = vl_imreadjpeg({strcat(dirName,imgList{i})});
      %  cform = makecform('cmyk2srgb');
      %  I = applycform(double(I1{1}),cform);
      I = imread(strcat(dirName,imgList{i}));
      I1 = zeros([size(I) 3],'uint8');
      I1(:,:,1) = I;
      I1(:,:,2) = I;
      I1(:,:,3) = I;
      I = I1;
    end
    
    % rescale the smallest size to 256 X 256 if needed.
    if(size(I,1) < desiredDim)
        I = imresize(I, [desiredDim size(I,2)]);
    end
    
    if(size(I,2) < desiredDim)
        I = imresize(I, [size(I,1) desiredDim]);
    end
    
   % Extract the center portion of the image.
    if(size(I,1) >= desiredDim && size(I,2) >= desiredDim) 
%        subplot(2,1,1)
%        imshow(I)
       I = extractCenterImage(I,desiredDim);
%        subplot(2,1,2)
%        imshow(I)
%        b=waitforbuttonpress;          
   end
   %save the images
   c = strsplit(imgList{i},'/')
   imgOut = strcat(saveDirName,c{3});
   imwrite(I,imgOut);
end
