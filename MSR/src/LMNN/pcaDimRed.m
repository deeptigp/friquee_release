load goldLayerLabels_pool5.mat        
load imNetVal_pool5Features_1_50.mat  

X = double(pool5Features); 
trainTestSplit = 1;

load(strcat('../imNetVal/Data/TrainingNIter_50Class.mat'));
load(strcat('../imNetVal/Data/TestingNIter_50Class.mat'));

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

trainX = X(trainIndx,:);
testX = X(testIndx,:);

dimRed = 1000;

%function [pcaTrain, pcaTest,Dact, Dred] = pcaDimRed(trainX, testX, dimRed)
    % first, 0-mean data
    trainX= bsxfun(@minus, trainX, mean(trainX,1));           
    testX  = bsxfun(@minus, testX, mean(trainX,1));           

    % Compute PCA
    covariancex = (trainX'*trainX)./(size(trainX,1)-1);                 
    %[V Dact] = eig(covariancex);  
    [V Dred] = eigs(covariancex, dimRed);
    pcaTrain = trainX*V;
    % here you should train your classifier on pcatrain and ytrain (correct labels)

    pcaTest = testX*V;
    % here you can test your classifier on pcatest using ytest (compare with correct labels)

%end
findBestL;

save('Data/pcaTrain.mat','pcaTrain');
save('Data/pcaTest.mat','pcaTest');

