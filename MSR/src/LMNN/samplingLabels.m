load('Data/goldLayerLabels_pool5.mat');
load('Data/imNetVal_pool5Features_1_50.mat');

Y = goldLayerLabels;

Y = (Y==1);

numNegSamples = sum(Y==0);

% We want to only get that many positive samples.

posIndx = find(Y == 1);
negIndx  = find(Y == 0);

randIndx = randperm(length(posIndx),numNegSamples);
posIndx = posIndx(randIndx);

posSamples = Y(posIndx);
negSamples = Y(negIndx);

dataIndices = [posIndx' negIndx'];
classLabels = double([posSamples' negSamples']);
features = pool5Features(dataIndices,:);

numImages = length(dataIndices);
numTrainImgs = round(0.8*numImages);

indx = randperm(numImages);

trainIndx = indx(1:numTrainImgs);
testIndx = indx(1+numTrainImgs:end);
    

sampledData = struct;

sampledData.features = features;
sampledData.dataIndices = dataIndices;
sampledData.classLabels = classLabels;
sampledData.testIndx = testIndx;
sampledData.trainIndx = trainIndx;

save('Data/sampledData.mat','sampledData');