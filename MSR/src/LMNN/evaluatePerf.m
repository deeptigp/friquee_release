predictedLabels = knnclDetails.lTe2;
trueLabels = Y(testIndx, :)';

accur = mean(predictedLabels == trueLabels);

[prec, rec] = computePrecRecall_binary(trueLabels,predictedLabels);