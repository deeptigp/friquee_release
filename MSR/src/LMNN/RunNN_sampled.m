addpath('lmnn3/');
setpaths3;
load('Data/sampledData.mat');

X = double(sampledData.features); 
Y = sampledData.classLabels'; 

numImages = size(X,1);

trainTestSplit = 1;

trainIndx = sampledData.trainIndx;
testIndx = sampledData.testIndx;

xTr = X(trainIndx, :)'; 
xTe = X(testIndx, :)'; 

yTr = Y(trainIndx)';
yTe = Y(testIndx, :)';


%% tune parameters
disp('Setting hyper parameters');
maxiter=1000; % will converge earlier
outdim = 25;

outDimRange = [5:5:50]; % Sweeping through different dimensions
outKRange = [3:2:15]; % Different values of K

numDim = length(outDimRange);
numK = length(outKRange);


allAccur = [];
allPrec = [];
allRec = [];
allTestErrEuc = [];
allTestErrLMNN = [];
for i = 1: numDim
    for j = 1:numK
        outdim = outDimRange(i);
        K = outKRange(j);
        [K outdim]
        
        %% train full muodel
        fprintf('Training final model...\n');
        [L,Details] = lmnnCG(xTr, yTr,K,'outdim',outdim, 'maxiter',maxiter);
        testerrEUC=knncl([],xTr,yTr,xTe,yTe,1,'train',1,'test',1);
        [testerrLMNN, knnclDetails]=knncl(L,xTr,yTr,xTe,yTe,1,'train',1,'test',1);
        fprintf('\n\nTesting error before LMNN: %2.2f%%\n',100.*testerrEUC);
        fprintf('Testing error after  LMNN: %2.2f%%\n',100.*testerrLMNN);

        evaluatePerf;

        [accur, prec, rec, testerrEUC(2), testerrLMNN(2)]

        allAccur(i,j) = accur;

        allPrec(i,j) = prec;

        allRec(i,j) = rec;

        allTestErrEuc(i,j) = testerrEUC(2);

        allTestErrLMNN(i,j) = testerrLMNN(2);
        
    end
end