%function l = findBestL(Dred,Dact)
    Da = diag(Dact);
    Da = sum(Da);

    Dr = diag(Dred);
    sumD = 0;
    
    l = dimRed;
    
    for l = 1:size(Dr)
        sumD = sumD + Dr(l);
        if (sumD/Da >= 0.9)
            break;
        end
    end
%end
