% DIR_PATH = '/media/deepti/J/MSR/fineGrained/';
% load(strcat(DIR_PATH,'Data/dogs_pool5Features_1-10.mat'));
% load(strcat(DIR_PATH,'Data/dogs_groundTruthLabels_1-10.mat'));

pool5Features_altDrop = dropout(pool5Features, 0);

[medACC5a, testY, Y_hat5a, runTimeForAllImgs5a] = executeSVM(pool5Features_altDrop, groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'), 'pool5');

%load(strcat(DIR_PATH,'Data/dogs_dropOutFC5_1-10.mat'));
pool5Features_randDrop = dropout(pool5Features, 1);

[medACC5r, testY, Y_hat5r, runTimeForAllImgs5r] = executeSVM(pool5Features_randDrop, groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'), 'pool5');

Y = compressData(pool5Features, 4096);
% [medACC5c1, testY, Y_hat5c1, runTimeForAllImgs5c1] = executeSVM(Y, groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'), 'pool5');
%    
% Y = compressData(pool5Features, 4608);
% [medACC5c2, testY, Y_hat5c2, runTimeForAllImgs5c2] = executeSVM(Y, groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'), 'pool5');
%     