function Y = dropout(feat, isRandom)
    featDim = size(feat,2);
    
    dim = featDim/2;
    
    %random dropout
    if(isRandom)
        retainIndxs = randperm(featDim, dim);
    else
        retainIndxs = [1:2:featDim];
    end
    
    Y = feat(:,retainIndxs);
end