function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg, mPrec, mRecall] = binarySVM_fast(trainX, trainY, testX, testY, layer)
    mPrec = 0;
    mRecall = 0;
    
    
    length(testY)
    
    sum(testY)
    
    trainOpts = ['-t 0 -s 0 -b 0 '];%-g ',gamma ' -c ',C]

    trainedModel = svmtrain(trainY,double(trainX),trainOpts);

    tStart = tic;

    [Y_hat, medACC , probEstimates] = svmpredict (testY, double(testX), trainedModel, '-b 0 -q');    
    
    runTimeForAllImgs = toc(tStart);

    runTimePerImg = runTimeForAllImgs/length(testY);
    
    %[mPrec, mRecall] = computePrecRecall(testY, Y_hat);
end