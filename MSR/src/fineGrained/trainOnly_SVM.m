function [medACC, testY, Y_hat, runTimeForAllImgs, meanPrec, meanRec] = trainOnly_SVM(trainX, trainY)
    
    meanPrec = 0;
    meanRec = 0;
        
    trainOpts = ['-t 0 -s 0 -b 0 '];
    
    trainedModel = svmtrain(trainY,double(trainX),trainOpts);
end