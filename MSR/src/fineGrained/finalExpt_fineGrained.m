%%%%% 1. Getting the individual perf. metrics
% DIR_PATH = '/media/deepti/J/MSR/fineGrained/';
% load(strcat(DIR_PATH,'Data/dogs_fc7Features_1-10.mat'));
% load(strcat(DIR_PATH,'Data/dogs_fc6Features_1-10.mat'));
% load(strcat(DIR_PATH,'Data/dogs_pool5Features_1-10.mat'));
% load(strcat(DIR_PATH,'Data/pool5Compress_4096.mat'));
% load('/media/deepti/J/MSR/fineGrained/Data/dogs_groundTruthLabels_1-10.mat');
% 
% for split = 1:3
% 
%     [medACC7, testY, Y_hat7, runTimeForAllImgs7, meanPrec7, meanRec7] = executeSVM(fc7Features,groundTruthLabels, split, '/media/deepti/J/MSR/fineGrained/Data/TrainingNIter_10Class.mat', '/media/deepti/J/MSR/fineGrained/Data/TestingNIter_10Class.mat', 'fc7');
%     [medACC6, testY, Y_hat6, runTimeForAllImgs6, meanPrec6, meanRec6] = executeSVM(fc6Features,groundTruthLabels, split, '/media/deepti/J/MSR/fineGrained/Data/TrainingNIter_10Class.mat', '/media/deepti/J/MSR/fineGrained/Data/TestingNIter_10Class.mat', 'fc6');
%     [medACC5, testY, Y_hat5, runTimeForAllImgs5, meanPrec5, meanRec5] = executeSVM(pool5Features,groundTruthLabels, split, '/media/deepti/J/MSR/fineGrained/Data/TrainingNIter_10Class.mat', '/media/deepti/J/MSR/fineGrained/Data/TestingNIter_10Class.mat', 'pool5');
%     [medACC5c, testY, Y_hat5c, runTimeForAllImgs5c, meanPrec5c, meanRec5c] = executeSVM(pool5Compress_4096,groundTruthLabels, split, '/media/deepti/J/MSR/fineGrained/Data/TrainingNIter_10Class.mat', '/media/deepti/J/MSR/fineGrained/Data/TestingNIter_10Class.mat', 'pool5');
% 
% 
%     perLayerResults_1_10_split = struct;
%     perLayerResults_1_10_split .medACC7 = medACC7;
%     perLayerResults_1_10_split .Y_hat7 = Y_hat7;
%     perLayerResults_1_10_split .runTimeForAllImgs7 = runTimeForAllImgs7;
%     perLayerResults_1_10_split .meanPrec7 = meanPrec7;
%     perLayerResults_1_10_split .meanRec7 = meanRec7;
% 
%     perLayerResults_1_10_split .medACC6 = medACC6;
%     perLayerResults_1_10_split .Y_hat6 = Y_hat6;
%     perLayerResults_1_10_split .runTimeForAllImgs6 = runTimeForAllImgs6;
%     perLayerResults_1_10_split .meanPrec6 = meanPrec6;
%     perLayerResults_1_10_split .meanRec6 = meanRec6;
% 
%     perLayerResults_1_10_split .medACC5 = medACC5;
%     perLayerResults_1_10_split .Y_hat5 = Y_hat5;
%     perLayerResults_1_10_split .runTimeForAllImgs5 = runTimeForAllImgs5;
%     perLayerResults_1_10_split .meanPrec5 = meanPrec5;
%     perLayerResults_1_10_split .meanRec5 = meanRec5;
% 
%     perLayerResults_1_10_split .medACC5c = medACC5c;
%     perLayerResults_1_10_split .Y_hat5c = Y_hat5c;
%     perLayerResults_1_10_split .runTimeForAllImgs5c = runTimeForAllImgs5c;
%     perLayerResults_1_10_split .meanPrec5c = meanPrec5c;
%     perLayerResults_1_10_split .meanRec5c = meanRec5c;
% 
% 
%     save(strcat('Results/perLayerResults_1_10_split',num2str(split),'.mat'),'perLayerResults_1_10_split');
% 
%   %  cascade_SVM_SVM(split);
% end


%%==COMPUTING THE COST AND THE ACCURACY THAT WE CAN OBTAIN ASSUMING OUR
%%CLASSIFIER IS PERFECT ===%%%

meanFeatTime = 0;
meanSVRTime  = 0;
meanTotalTime  = 0;
meanAccur = 0;
meanPrec = 0;
meanRecall = 0;
for split = 1:3
    aggregateResults{split} =  final_perfect_cascade(split);
    meanFeatTime  = meanFeatTime + aggregateResults{split}.totalFeatureComputeTime;
    meanSVRTime  = meanSVRTime + aggregateResults{split}.totalRegressionTime;
    meanTotalTime  = meanTotalTime + aggregateResults{split}.totalTestTime;
    meanAccur  = meanAccur + aggregateResults{split}.aggreACC;
    meanPrec = meanPrec + aggregateResults{split}.aggrPrec;
    meanRecall = meanRecall + aggregateResults{split}.aggrRecall;
end

meanFeatTime = meanFeatTime/3;
meanSVRTime = meanSVRTime/3;
meanTotalTime = meanTotalTime/3;
meanAccur  = meanAccur/3;
meanRecall = meanRecall/3;
meanPrec = meanPrec/3;

bestAchievable = struct;
bestAchievable.meanFeatTime = meanFeatTime;
bestAchievable.meanSVRTime = meanSVRTime;
bestAchievable.meanTotalTime = meanTotalTime;
bestAchievable.meanAccur = meanAccur;
bestAchievable.meanPrec = meanPrec;
bestAchievable.meanRecall = meanRecall;

save('Results/bestAchievable_fineGrained.mat','bestAchievable');