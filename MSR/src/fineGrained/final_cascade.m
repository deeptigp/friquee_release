function final_cascade(trainTestSplit)
    %trainTestSplit = 3;
    DATA_PATH = '/media/deepti/J/MSR/fineGrained/Data/';
    load(strcat(DATA_PATH,'dogs_groundTruthLabels_1-10.mat'));
    load(strcat(DATA_PATH,'pool5Compress_4096.mat'));

    load(strcat(DATA_PATH,'dogs_fc6Features_1-10.mat'));

    load(strcat(DATA_PATH,'dogs_fc7Features_1-10.mat'));

    load(strcat(DATA_PATH,'TrainingNIter_10Class.mat'));
    load(strcat(DATA_PATH,'TestingNIter_10Class.mat'));

    %% All initializations..
    
    labels = groundTruthLabels;
    
    numImages = length(labels);
    
        %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);

    cascadeClassifierResults_aggre567_BSVC_pool5 = struct;
    cascadeClassifierResults_aggre567_BSVC_fc6 = struct;
    cascadeClassifierResults_aggre567_BSVC_fc7 = struct;
   
    % Get the current train/test split.
    trainIndx = TrainingNIter(:,trainTestSplit);
    testIndx = TestingNIter(:,trainTestSplit);

    %1. Step1 - Get the oracle layer labels for the training data.
    GTLabels = ones(numImages,1)*-1;
    
    trainingLayerLabels = kFold_final(pool5Compress_4096(trainIndx,:), fc6Features(trainIndx,:), fc7Features(trainIndx,:), labels(trainIndx), trainTestSplit);
    
    GTLabels(trainIndx) = trainingLayerLabels; % This has -1 for test data, 1 for pool5, 2 for fc6, and 3 for fc7.
    
    class1LabelIndxs = find(GTLabels== 1);
    class2LabelIndxs = find(GTLabels== 2);
    class3LabelIndxs = find(GTLabels== 3);

    binaryLabels1 = zeros(1,numImages);
    binaryLabels2 = zeros(1,numImages);
    binaryLabels3 = zeros(1,numImages);

    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
    binaryLabels2(class2LabelIndxs) = 1; % fc6 labels
    binaryLabels3(class3LabelIndxs) = 1; % fc7 labels
    
   %% train with fc5, those that say no-fc5, test them with fc6.
    trainNX = pool5Compress_4096(trainIndx,:);
    testNX = pool5Compress_4096(testIndx,:);

   [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg, mPrec, mRecall] = binarySVM_fast(trainNX,binaryLabels1(trainIndx)',testNX, binaryLabels1(testIndx)');

     cascadeClassifierResults_aggre567_BSVC_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_pool5.testIndx = testIndx;
    cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_BSVC_pool5.accuracy = accuracy;
    cascadeClassifierResults_aggre567_BSVC_pool5.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre567_BSVC_pool5.mPrec = mPrec;
    cascadeClassifierResults_aggre567_BSVC_pool5.mRecall = mRecall;


    save(strcat('Results/aggregateResults/SVM-SVM/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_pool5.mat'),'cascadeClassifierResults_aggre567_BSVC_pool5');

    cascadePredictedResults_aggre567_BSVC_pool5 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_pool5, 'pool5',trainTestSplit, pool5Compress_4096);

    save(strcat('Results/aggregateResults/SVM-SVM/',num2str(trainTestSplit),'/cascadePredictedResults_aggre567_BSVC_pool5.mat'),'cascadePredictedResults_aggre567_BSVC_pool5');

    %% Aggregating the predictions of the data points that were predicted to belong to pool5 class..
    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_pool5.class1Indices) = cascadePredictedResults_aggre567_BSVC_pool5.ePred5;

%% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_BSVC_pool5.testIndx(cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat==0);

    fprintf('Number of not fc5 images: %d',length(notFC5Indxs));

    fc6Compressed = compressData(fc6Features, 3000);

    trainNX = fc6Compressed(trainIndx,:);
    testNX = fc6Compressed(notFC5Indxs,:);

    trainNY = binaryLabels2(trainIndx)';
    testNY = binaryLabels2(notFC5Indxs)';

    [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg, mPrec, mRecall] = binarySVM_fast(trainNX, trainNY, testNX, testNY);

    cascadeClassifierResults_aggre567_BSVC_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_BSVC_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre567_BSVC_fc6.runTimeForAllImgs = runTimeForAllImgs;
     cascadeClassifierResults_aggre567_BSVC_fc6.mPrec = mPrec;
    cascadeClassifierResults_aggre567_BSVC_fc6.mRecall = mRecall;



    save(strcat('Results/aggregateResults/SVM-SVM/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_fc6.mat'),'cascadeClassifierResults_aggre567_BSVC_fc6');

    cascadePredictedResults_aggre567_BSVC_fc6 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc6, 'fc6',trainTestSplit, fc6Features);

    save(strcat('Results/aggregateResults/SVM-SVM/',num2str(trainTestSplit),'/cascadePredictedResults_aggre567_BSVC_fc6.mat'),'cascadePredictedResults_aggre567_BSVC_fc6');

    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc6.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc6.ePred6;

    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre567_BSVC_fc6.testIndx(cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat==0);

    fprintf('Number of not fc6 images: %d',length(notFC6Indxs));

    Y_hat = ones(size(notFC6Indxs));

    cascadeClassifierResults_aggre567_BSVC_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc7.Y_hat = Y_hat;

    save(strcat('Results/aggregateResults/SVM-SVM/',num2str(trainTestSplit),'/cascadeClassifierResults_aggre567_BSVC_fc7.mat'),'cascadeClassifierResults_aggre567_BSVC_fc7');

    cascadePredictedResults_aggre567_BSVC_fc7 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc7, 'fc7',trainTestSplit, fc7Features);

    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc7.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc7.ePred7;
    save(strcat('Results/aggregateResults/SVM-SVM/',num2str(trainTestSplit),'/cascadePredictedResults_aggre567_BSVC_fc7.mat'),'cascadePredictedResults_aggre567_BSVC_fc7');


    nonZeroPredictedScores = aggregatePredictedScores(testIndx);
    aggreACC = mean(nonZeroPredictedScores ==labels(testIndx));

    aggregateResults_BSVC.predictedScores = nonZeroPredictedScores;
    aggregateResults_BSVC.aggreACC = aggreACC;
    [agPrec, agRecall] = computePrecRecall(labels(testIndx),nonZeroPredictedScores);

    aggregateResults_BSVC.aggrPrec = agPrec;
    aggregateResults_BSVC.aggrRecall = agRecall;
    
    fprintf('Mean accuracy between the aggregated predictions and GT predictions %f %f\n',aggreACC);

    temp_runningTimes;
    aggregateResults_BSVC.totalTestTime = totalTestTime;
    aggregateResults_BSVC.totalClassificationTime = totalClassificationTime;
    aggregateResults_BSVC.totalFeatureComputeTime = totalFeatureComputeTime;
    aggregateResults_BSVC.totalRegressionTime = totalRegressionTime;


    save(strcat('Results/aggregateResults/SVM-SVM/',num2str(trainTestSplit),'/aggregatePredictedScores_BSVC_fc567.mat'),'aggregateResults_BSVC');

