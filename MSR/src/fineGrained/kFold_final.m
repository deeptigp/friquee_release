function derivedLayerLabels = kFold_final(layer5Feat, layer6Feat, layer7Feat, labels, trainTestSplit)
    
numImages = size(layer5Feat,1);

predictedImNetClassLabels = zeros(numImages,3); % obtained from pool5, fc6, and fc7.

derivedLayerLabels = zeros(numImages,1);
kFold = 5;

foldIndices = crossvalind('Kfold', numImages, kFold);

kFold_567 = struct;

for k = 1:kFold
    k
    testIndx = find(foldIndices==k);
    trainIndx = find(foldIndices ~=k);
    
    testLabels = labels(testIndx);
    trainLabels = labels(trainIndx);
    
    [size(testIndx) size(trainIndx)]
    
    [acc5, gt5, pred5] = runSVM_fast(layer5Feat(trainIndx,:),trainLabels,layer5Feat(testIndx,:),testLabels,'pool5');

    [acc6, gt6, pred6] = runSVM_fast(layer6Feat(trainIndx,:),trainLabels,layer6Feat(testIndx,:),testLabels,'fc6');

    [acc7, gt7, pred7] = runSVM_fast(layer7Feat(trainIndx,:),trainLabels,layer7Feat(testIndx,:),testLabels,'fc7');

    predictedImNetClassLabels(testIndx,:) = [(pred5==gt5) (pred6==gt6) (pred7==gt7)];    
end

%% Logic to assign class labels pool5 (1), fc6 (2), or fc7 (3)

fc5Indexes = find(predictedImNetClassLabels(:,1)==1);

derivedLayerLabels(fc5Indexes) = 1;

fc6Indexes = find(predictedImNetClassLabels(:,1)==0 & predictedImNetClassLabels(:,2)== 1);

derivedLayerLabels(fc6Indexes) = 2;

fc7Indexes = find(predictedImNetClassLabels(:,1)==0 & predictedImNetClassLabels(:,2)== 0 & predictedImNetClassLabels(:,3)== 1);

derivedLayerLabels(fc7Indexes) = 3;

%% There still are a few data points which were all classified incorrectly by all three layers.. As of now, I am assigning them the label pool5
dontMatter = find(predictedImNetClassLabels(:,1)==0 & predictedImNetClassLabels(:,2)== 0 & predictedImNetClassLabels(:,3)== 0);

derivedLayerLabels(dontMatter) = 1;

kFold_567.foldIndices = foldIndices;
kFold_567.predictedImNetClassLabels = predictedImNetClassLabels;
kFold_567.derivedLayerLabels = derivedLayerLabels;
kFold_567.dontMatter = dontMatter;

save(strcat('Results/kFold_567_',num2str(trainTestSplit),'_final.mat'),'kFold_567');
end

