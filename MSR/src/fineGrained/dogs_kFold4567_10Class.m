DATA_PATH = '/media/deepti/J/MSR/fineGrained/';
load(strcat(DATA_PATH,'Data/dogs_fc6Features_1-10.mat'));
load(strcat(DATA_PATH,'Data/dogs_fc7Features_1-10.mat'));
load(strcat(DATA_PATH,'Data/dogs_pool5Features_1-10.mat'));
load(strcat(DATA_PATH,'Data/dogs_conv4Features_compress_1-10.mat'));
load(strcat(DATA_PATH,'Data/dogs_groundTruthLabels_1-10.mat'));

labels = groundTruthLabels;
conv4Features = conv4Features_compressed;
numImages = size(pool5Features,1);

kFold = 4;

foldIndices = crossvalind('Kfold', numImages, kFold);

predictedImNetClassLabels = zeros(numImages,4); % obtained from pool5, fc6, and fc7, and conv4

derivedLayerLabels = zeros(numImages,1);

kFold_4567 = struct;

for k = 1:kFold
    k
    testIndx = find(foldIndices==k);
    trainIndx = find(foldIndices ~=k);
    
    testLabels = labels(testIndx);
    trainLabels = labels(trainIndx);
    
    [size(testIndx) size(trainIndx)]
    
    [acc5, gt4, pred4] = runSVM_fast(conv4Features(trainIndx,:),trainLabels,conv4Features(testIndx,:),testLabels,'conv4');
    
    [acc5, gt5, pred5] = runSVM_fast(pool5Features(trainIndx,:),trainLabels,pool5Features(testIndx,:),testLabels,'pool5');

    [acc6, gt6, pred6] = runSVM_fast(fc6Features(trainIndx,:),trainLabels,fc6Features(testIndx,:),testLabels,'fc6');

    [acc7, gt7, pred7] = runSVM_fast(fc7Features(trainIndx,:),trainLabels,fc7Features(testIndx,:),testLabels,'fc7');

    predictedImNetClassLabels(testIndx,:) = [(pred5==gt5) (pred6==gt6) (pred7==gt7) (pred4==gt4)];    
end

%% Logic to assign class labels pool5 (1), fc6 (2), or fc7 (3)
%%conv4
conv4Indexes = find(predictedImNetClassLabels(:,4));

notConv4Indexes = find(predictedImNetClassLabels(:,4) == 0);

derivedLayerLabels(conv4Indexes) = 4;

%%fc5
fc5Indexes = notConv4Indexes(predictedImNetClassLabels(notConv4Indexes,1) == 1);

derivedLayerLabels(fc5Indexes) = 1;

notFC5Indexes = notConv4Indexes(predictedImNetClassLabels(notConv4Indexes,1) == 0);

fc6Indexes = notFC5Indexes(predictedImNetClassLabels(notFC5Indexes,2) == 1);

derivedLayerLabels(fc6Indexes) = 2;

notFC6Indexes = notFC5Indexes(predictedImNetClassLabels(notFC5Indexes,2) == 0);

fc7Indexes = notFC6Indexes(predictedImNetClassLabels(notFC6Indexes,3)==1);

derivedLayerLabels(fc7Indexes) = 3;

%% There still are a few data points which were all classified incorrectly by all three layers.. As of now, I am assigning them the label pool5
notFC7Indexes = notFC6Indexes(predictedImNetClassLabels(notFC6Indexes,3)==0);

derivedLayerLabels(notFC7Indexes) = 4;


kFold_4567.foldIndices = foldIndices;
kFold_4567.predictedImNetClassLabels = predictedImNetClassLabels;
kFold_4567.derivedLayerLabels = derivedLayerLabels;

save('Results/kFold_4567_10Class_k4.mat','kFold_4567');

