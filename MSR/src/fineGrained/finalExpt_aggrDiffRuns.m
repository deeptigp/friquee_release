perLayerFeatComputeTime = [0.1420 0.1172 0.0775 0.0777]; % fc7-fc6-pool5-pool5Compressed
individualFeatCompTime = 442*perLayerFeatComputeTime; 

s1 = load('Results/finalResults/perLayerResults_1_10_split1.mat');
s1 = s1.perLayerResults_1_10_split;
s2 = load('Results/finalResults/perLayerResults_1_10_split2.mat');
s2 = s2.perLayerResults_1_10_split;
s3 = load('Results/finalResults/perLayerResults_1_10_split3.mat');
s3 = s3.perLayerResults_1_10_split;

%% aggregate results 
a1 = load('Results/aggregateResults/SVM-SVM/1/aggregatePredictedScores_BSVC_fc567.mat');
a1 = a1.aggregateResults_BSVC;

a2 = load('Results/aggregateResults/SVM-SVM/2/aggregatePredictedScores_BSVC_fc567.mat');
a2 = a2.aggregateResults_BSVC;

a3 = load('Results/aggregateResults/SVM-SVM/3/aggregatePredictedScores_BSVC_fc567.mat');
a3 = a3.aggregateResults_BSVC;


meanAccur7 = mean([s1.medACC7(1) s2.medACC7(1) s3.medACC7(1)]);

meanAP7 = mean([s1.meanPrec7 s2.meanPrec7 s3.meanPrec7]);

meanAR7 = mean([s1.meanRec7 s2.meanRec7 s3.meanRec7]);

meanAccur6 = mean([s1.medACC6(1) s2.medACC6(1) s3.medACC6(1)]);

meanAP6 = mean([s1.meanPrec6 s2.meanPrec6 s3.meanPrec6]);

meanAR6 = mean([s1.meanRec6 s2.meanRec6 s3.meanRec6]);

meanAccur5 = mean([s1.medACC5(1) s2.medACC5(1) s3.medACC5(1)]);

meanAP5 = mean([s1.meanPrec5 s2.meanPrec5 s3.meanPrec5]);

meanAR5 = mean([s1.meanRec5 s2.meanRec5 s3.meanRec5]);

meanAccur5c = mean([s1.medACC5c(1) s2.medACC5c(1) s3.medACC5c(1)]);

meanAP5c = mean([s1.meanPrec5c s2.meanPrec5c s3.meanPrec5c]);

meanAR5c = mean([s1.meanRec5c s2.meanRec5c s3.meanRec5c]);


meanRunTime7 = mean([s1.runTimeForAllImgs7 s2.runTimeForAllImgs7 s3.runTimeForAllImgs7]);

meanRunTime6 = mean([s1.runTimeForAllImgs6 s2.runTimeForAllImgs6 s3.runTimeForAllImgs6]);

meanRunTime5 = mean([s1.runTimeForAllImgs5 s2.runTimeForAllImgs5 s3.runTimeForAllImgs5]);

meanRunTimeCompress5 = mean([s1.runTimeForAllImgs5c s2.runTimeForAllImgs5c s3.runTimeForAllImgs5c]);

meanTotalTime = individualFeatCompTime + [meanRunTime7 meanRunTime6 meanRunTime5 meanRunTimeCompress5];

%%--cascaded results -- %%

meanAccurCascade = mean([a1.aggreACC a2.aggreACC a3.aggreACC]);

mAPCascade = mean([a1.aggrPrec a2.aggrPrec a3.aggrPrec]);

mARCascade = mean([a1.aggrRecall a2.aggrRecall a3.aggrRecall]);


meanCTimeCascade = mean([a1.totalClassificationTime a2.totalClassificationTime a3.totalClassificationTime]);

meanRTimeCascade = mean([a1.totalRegressionTime a2.totalRegressionTime a3.totalRegressionTime]);

meanFCTimeCascade = mean([a1.totalFeatureComputeTime a2.totalFeatureComputeTime a3.totalFeatureComputeTime]);

meanTotalTestTimeCascade = mean([a1.totalTestTime  a2.totalTestTime a3.totalTestTime]);


%%Performance of the gating functions..
g51 = load('Results/aggregateResults/SVM-SVM//1/cascadeClassifierResults_aggre567_BSVC_pool5.mat');
g51 = g51.cascadeClassifierResults_aggre567_BSVC_pool5;
g52 = load('Results/aggregateResults/SVM-SVM//2/cascadeClassifierResults_aggre567_BSVC_pool5.mat');
g52 = g52.cascadeClassifierResults_aggre567_BSVC_pool5;
g53 = load('Results/aggregateResults/SVM-SVM//3/cascadeClassifierResults_aggre567_BSVC_pool5.mat');
g53 = g53.cascadeClassifierResults_aggre567_BSVC_pool5;

gf5Accur = mean([g51.accuracy(1) g52.accuracy(1) g53.accuracy(1)]);
gf5Prec = mean([g51.mPrec g52.mPrec g53.mPrec]);
gf5Recall = mean([g51.mRecall g52.mRecall g53.mRecall]);


g61 = load('Results/aggregateResults/SVM-SVM//1/cascadeClassifierResults_aggre567_BSVC_fc6.mat');
g61 = g61.cascadeClassifierResults_aggre567_BSVC_fc6;
g62 = load('Results/aggregateResults/SVM-SVM//2/cascadeClassifierResults_aggre567_BSVC_fc6.mat');
g62 = g62.cascadeClassifierResults_aggre567_BSVC_fc6;
g63 = load('Results/aggregateResults/SVM-SVM//3/cascadeClassifierResults_aggre567_BSVC_fc6.mat');
g63 = g63.cascadeClassifierResults_aggre567_BSVC_fc6;

gf6Accur = mean([g61.accuracy(1) g62.accuracy(1) g63.accuracy(1)]);
gf6Prec = mean([g61.mPrec g62.mPrec g63.mPrec]);
gf6Recall = mean([g61.mRecall g62.mRecall g63.mRecall]);


aggr3Splits = struct;

%%Gating funcs. performance
aggr3Splits.gf5Accur = gf5Accur;
aggr3Splits.gf5Prec = gf5Prec;
aggr3Splits.gf5Recall = gf5Recall;


aggr3Splits.gf6Accur = gf6Accur;
aggr3Splits.gf6Prec = gf6Prec;
aggr3Splits.gf6Recall = gf6Recall;


aggr3Splits.meanAccur7 = meanAccur7;
aggr3Splits.meanAP7 = meanAP7;
aggr3Splits.meanAR7 = meanAR7;

aggr3Splits.meanAccur6 = meanAccur6;
aggr3Splits.meanAP6 = meanAP6;
aggr3Splits.meanAR6 = meanAR6;

aggr3Splits.meanAccur5 = meanAccur5;
aggr3Splits.meanAP5 = meanAP5;
aggr3Splits.meanAR5 = meanAR5;

aggr3Splits.meanAccur5c = meanAccur5c;
aggr3Splits.meanAP5c = meanAP5c;
aggr3Splits.meanAR5c = meanAR5c;


aggr3Splits.meanRunTime7 = meanRunTime7;
aggr3Splits.meanRunTime6 = meanRunTime6;
aggr3Splits.meanRunTime5 = meanRunTime5;
aggr3Splits.meanRunTimeCompress5 = meanRunTimeCompress5;

aggr3Splits.meanTotalTime = meanTotalTime;

aggr3Splits.meanAccurCascade = meanAccurCascade;
aggr3Splits.mAPCascade = mAPCascade;
aggr3Splits.mARCascade = mARCascade;

aggr3Splits.meanCTimeCascade = meanCTimeCascade;
aggr3Splits.meanRTimeCascade = meanRTimeCascade;
aggr3Splits.meanFCTimeCascade = meanFCTimeCascade;
aggr3Splits.meanTotalTestTimeCascade = meanTotalTestTimeCascade;

save('Results/finalResults/aggr3Splits.mat','aggr3Splits');
