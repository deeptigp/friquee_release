function Y = compressData(feat, K)
    
    % dimensionality of the features
    fN = size(feat,2);
    
    % K is the factor of compression.
    A = zeros(K,fN);

    for i = 1:K
        A(i,:) = randn(1,fN);
    end

    Y = A*feat';
    Y = Y';
end