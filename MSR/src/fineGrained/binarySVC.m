%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM regressor for NIter numbere of times.
%function [accuracy, Y_hat] = binarySVC(trainX, trainY, testX, testY)
function [accur, Y_hat, runTimeForAllImgs, runTimePerImg] = binarySVC(trainX, trainY, testX, testY)
    addpath('..');
    
    C = 2; gamma = 1;
    
    accur = zeros(length(C), length(gamma));
    
    fid = fopen('train_ind_bc.txt','w');

    for itr_im = 1:size(trainX,1)
        fprintf(fid,'%d ',trainY(itr_im,1));
        for itr_feat =1:size(trainX,2)
            fprintf(fid,'%d:%f ',itr_feat,trainX(itr_im,itr_feat));
        end
        fprintf(fid,'\n');
    end

    fclose(fid);

    % Execute it.
    system('./svm-scale -l -1 -u 1 -s range_bc train_ind_bc.txt > train_scale_bc');

    % Construct the file ( input ) suitably for testing the SVM
    fid = fopen('test_ind_bc.txt','w');
    for itr_im = 1:size(testX,1)
        fprintf(fid,'%d ',testY(itr_im,1));
        for itr_param = 1:size(testX,2)
            fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);

    % Scale the data.
    system('./svm-scale  -r range_bc test_ind_bc.txt > test_ind_scaled_bc');

    %% t: Kernel type. %s: svm type. g: gamma c: cost.
    %  system('svm-train  -s 0 -t 2 -b 1 -q train_scale_bc model_bc');
    for cItr = 1:length(C)
        for gItr = 1:length(gamma)
            %% This is a polynomial kernel, C-SVC.  % t:0 is linear, t:1 is polynomial
            cmd = strcat('./svm-train  -t 0 -s 0 -b 1 -r 1 -g',{' '},num2str(gamma(gItr)),' -c',{' '},num2str(C(cItr)),' -q train_scale_bc model_bc');
            %cmd = strcat('./svm-train  -t 1 -s 0 -b 1 -r 1 -c',{' '},num2str(C(cItr)),' -q train_scale_bc model_bc');
            cmd{1}
            system(cmd{1});
            tStart = tic; 
            system('./svm-predict  -b 1 test_ind_scaled_bc model_bc output_bc.txt > dump_bc');
            runTimeForAllImgs = toc(tStart);
            runTimePerImg = runTimeForAllImgs/length(testY)
            disp(strcat('Prediction Time for',num2str(length(testY)), 'images = ', num2str(runTimeForAllImgs), 'and for 1 image is ',num2str(runTimePerImg)))
           % load output_bc.txt;
            
            % Predicted scores
           % Y_hat=output_bc;
            
           % sum(Y_hat)
          % accuracy = sum(Y_hat == testY)/length(testY)
          % accur(cItr,gItr) = accuracy;
        end
    end
  %  system('rm test_ind_scaled_bc model_bc output_bc.txt dump_bc train_scale_bc test_ind_bc.txt range_bc train_ind_bc.txt');
end