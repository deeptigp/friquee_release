%%== sample calls.
%%
%[accuracy, runTimeForAllImgs, predictedLabels] = executeRDF(pool5Features,groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat') , strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'));

%function [varyNumTrees, allAccur, allRunTimes] = executeRDF(data,labels, trainTestSplit, trainFile, testFile, layer)

function [accuracy, runTimeForAllImgs, predictedLabels] = executeRDF(data,labels, trainTestSplit, trainFile, testFile)
load(trainFile);
load(testFile);

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

trainY = labels(trainIndx)';
testY = labels(testIndx)';

trainFeats = data(trainIndx,:);
testFeats = data(testIndx,:);

[size(trainY) size(testY)]

[size(trainFeats) size(testFeats)]

numTrees = 300;

% I want to capture the running time per image.
allAccur = [];
allRunTimes = [];

[accuracy, predictedLabels, runTimeForAllImgs, scores, stdevs, BaggedEnsemble] = ensembleDecisionForests(trainFeats,trainY',testFeats,testY', numTrees);

% varyNumTrees = [100 : 100 : 1000];
% for numTrees = varyNumTrees
%     numTrees
%     [accuracy, predictedLabels, runTimeForAllImgs, scores, stdevs, BaggedEnsemble] = ensembleDecisionForests(trainFeats,trainY',testFeats,testY', numTrees);
%     accuracy
%     allAccur = [allAccur accuracy];
%     allRunTimes = [allRunTimes runTimeForAllImgs];
% end
% 
% fig = figure(1);
% plot(varyNumTrees, allAccur, 'r-x');
% xlabel('Num. of trees');
% ylabel('Mean accuracy');
% title('For fine-grained 10-class pool5 features');
% print(fig,'Results/plots/dogPool5-accur-RDF','-djpeg');
% 
% fig =  figure(2);
% plot(varyNumTrees, allRunTimes, 'b-o');
% xlabel('Num. of trees');
% ylabel('Testing time');
% title('For fine-grained 10-class pool5 features');
% print(fig,'Results/plots/dogPool5-testTime-RDF','-djpeg');
% end