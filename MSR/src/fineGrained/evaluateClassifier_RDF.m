%% This method runs an SVR on the set of datapoints that got classified to have the current layer as the label. This is just to evaluate if the binary classifier is doing a good job.. For ex: If 20 data points got the label as pool5, we use pool5Features to predict their interestingness scores in this method.
function predictedResults = evaluateClassifier_RDF(classifierResults, layer)
    DATA_PATH = '/media/deepti/J/MSR/fineGrained/Data/';
    load(strcat(DATA_PATH,'dogs_groundTruthLabels_1-10.mat'));

    labels = groundTruthLabels;

    predictedResults = struct;
    class1Indices = classifierResults.testIndx(classifierResults.Y_hat==1);
    trainIndx = classifierResults.trainIndx;

    % Lets train SVCs
    if(strcmp(layer,'pool5'))
        disp('pool5')
        load(strcat(DATA_PATH,'pool5Features_randDrop.mat'));
        [accuracy, predictedLabels, runTimeForAllImgs] = ensembleDecisionForests(pool5Features_randDrop(trainIndx,:), labels(trainIndx), pool5Features_randDrop(class1Indices,:),labels(class1Indices), 300);
        predictedResults.medACC = accuracy;
        predictedResults.ePred5 = predictedLabels;
        predictedResults.runTimeForAllImgs5 = runTimeForAllImgs;

    elseif(strcmp(layer,'fc6'))
        disp('fc6')
        load(strcat(DATA_PATH,'dogs_fc6Features_1-10.mat'));
        [accuracy, predictedLabels, runTimeForAllImgs] = ensembleDecisionForests(fc6Features(trainIndx,:), labels(trainIndx), fc6Features(class1Indices,:),labels(class1Indices), 300);
        predictedResults.medACC = accuracy;
        predictedResults.ePred6 = predictedLabels;
        predictedResults.runTimeForAllImgs6 = runTimeForAllImgs;

    elseif(strcmp(layer,'fc7'))
        disp('fc7')
        load(strcat(DATA_PATH,'dogs_fc7Features_1-10.mat'));
        [accuracy, predictedLabels, runTimeForAllImgs] = ensembleDecisionForests(fc7Features(trainIndx,:), labels(trainIndx), fc7Features(class1Indices,:),labels(class1Indices), 300);
        predictedResults.medACC = accuracy;
        predictedResults.ePred7 = predictedLabels;
        predictedResults.runTimeForAllImgs7 = runTimeForAllImgs;
    end

    predictedResults.class1Indices = class1Indices;
    predictedResults.eGT = labels(class1Indices);
end