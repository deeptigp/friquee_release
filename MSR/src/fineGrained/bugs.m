diffWithFC7 = find(Y_hat7 ~= nonZeroPredictedScores);

layerLabels = kFold_567.newDerivedLayerLabels(testIndx);

layerLabelsI = layerLabels(diffWithFC7);

gTruth  = labels(testIndx);


differingValues = nonZeroPredictedScores(diffWithFC7);

gDiff = gTruth(diffWithFC7);

Y6 = Y_hat6(diffWithFC7);