% This script varies the K values and compresses the features accordingly.
DIR_PATH = '/media/deepti/J/MSR/fineGrained/';
%load(strcat(DIR_PATH,'Data/dogs_pool5Features_1-10.mat'));
load(strcat(DIR_PATH,'Data/dogs_fc6Features_1-10.mat'));
load(strcat(DIR_PATH,'Data/dogs_groundTruthLabels_1-10.mat'));

kVals = [1000:1000: 4096];

perfValues = [];
runningTimes6= [];

for K = kVals
    fprintf('Compressing features to dimension %d \n', K);
    Y = compressData(fc6Features, K);
    [medACC6, testY, Y_hat6, runTimeForAllImgs6] = executeSVM(Y, groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'), 'fc6');
    perfValues = [perfValues ; medACC6(1)];
    runningTimes6 = [runningTimes6; runTimeForAllImgs6];
end

plot(kVals, perfValues,'rX-');

xlabel('Dimension to which the pool6 was compressed to');
ylabel('Mean accuracy');