%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7

%function aggregateResults_RDF = cascadedBinaryClassifiers_binarySVC_567()
    DATA_PATH = '/media/deepti/J/MSR/fineGrained/Data/';
    load(strcat(DATA_PATH,'dogs_groundTruthLabels_1-10.mat'));

    labels = groundTruthLabels;

    load('Results/kFold_567_1-10_k5_dropout.mat');

    load(strcat(DATA_PATH,'dogs_dropOutFC5_1-10.mat'));

    load(strcat(DATA_PATH,'dogs_fc6Features_1-10.mat'));
    
    load(strcat(DATA_PATH,'dogs_fc7Features_1-10.mat'));
    
    load(strcat(DATA_PATH,'TrainingNIter_10Class.mat'));
    load(strcat(DATA_PATH,'TestingNIter_10Class.mat'));

    trainTestSplit = 1;
    trainIndx = TrainingNIter(:,trainTestSplit);
    testIndx = TestingNIter(:,trainTestSplit);

    %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);

    cascadeClassifierResults_aggre567_BSVC_pool5 = struct;
    cascadeClassifierResults_aggre567_BSVC_fc6 = struct;
    cascadeClassifierResults_aggre567_BSVC_fc7 = struct;

    % Generate a random train/test split.
    numImages = length(labels);

    % Get the derived class labels.
    GTLabels = kFold_567.derivedLayerLabels;

    %% Get the label indexes.
    class1LabelIndxs = find(GTLabels== 1);
    class2LabelIndxs = find(GTLabels== 2);
    class3LabelIndxs = find(GTLabels== 3);

    binaryLabels1 = zeros(1,numImages);
    binaryLabels2 = zeros(1,numImages);
    binaryLabels3 = zeros(1,numImages);

    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
    binaryLabels2(class2LabelIndxs) = 1; % fc6 labels
    binaryLabels3(class3LabelIndxs) = 1; % fc7 labels


    %% train with fc5, those that say no-fc5, test them with fc6.
    trainNX = dropOutFC5(trainIndx,:);
    testNX = dropOutFC5(testIndx,:);
    
    [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg, probEstimates] = binarySVM_fast(trainNX,binaryLabels1(trainIndx)',testNX, binaryLabels1(testIndx)');
 
    cascadeClassifierResults_aggre567_BSVC_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_pool5.testIndx = testIndx;
    cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_BSVC_pool5.accuracy = accuracy;
    cascadeClassifierResults_aggre567_BSVC_pool5.runTimeForAllImgs = runTimeForAllImgs;


    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre567_BSVC_pool5.mat'),'cascadeClassifierResults_aggre567_BSVC_pool5');

    cascadePredictedResults_aggre567_BSVC_pool5 = evaluateClassifier(cascadeClassifierResults_aggre567_BSVC_pool5, 'pool5',trainTestSplit);

    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre567_BSVC_pool5.mat'),'cascadePredictedResults_aggre567_BSVC_pool5');

    %% Aggregating the predictions of the data points that were predicted to belong to pool5 class..
    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_pool5.class1Indices) = cascadePredictedResults_aggre567_BSVC_pool5.ePred5;


    %% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_BSVC_pool5.testIndx(cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat==0);

    fprintf('Number of not fc5 images: %d',length(notFC5Indxs));

    trainNX = fc6Features(trainIndx,:);
    testNX = fc6Features(notFC5Indxs,:);

    [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg] = binarySVM_fast(trainNX, binaryLabels2(trainIndx)', testNX, binaryLabels2(notFC5Indxs)');

    cascadeClassifierResults_aggre567_BSVC_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_BSVC_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre567_BSVC_fc6.runTimeForAllImgs = runTimeForAllImgs;


    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre567_BSVC_fc6.mat'),'cascadeClassifierResults_aggre567_BSVC_fc6');

    cascadePredictedResults_aggre567_BSVC_fc6 = evaluateClassifier(cascadeClassifierResults_aggre567_BSVC_fc6, 'fc6',trainTestSplit);

    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre567_BSVC_fc6.mat'),'cascadePredictedResults_aggre567_BSVC_fc6');

    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc6.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc6.ePred6;

    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre567_BSVC_fc6.testIndx(cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat==0);

    fprintf('Number of not fc6 images: %d',length(notFC6Indxs));

    Y_hat = ones(size(notFC6Indxs));

    cascadeClassifierResults_aggre567_BSVC_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc7.Y_hat = Y_hat;

    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre567_BSVC_fc7.mat'),'cascadeClassifierResults_aggre567_BSVC_fc7');

    cascadePredictedResults_aggre567_BSVC_fc7 = evaluateClassifier(cascadeClassifierResults_aggre567_BSVC_fc7, 'fc7',trainTestSplit);

    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc7.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc7.ePred7;
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre567_BSVC_fc7.mat'),'cascadePredictedResults_aggre567_BSVC_fc7');


    nonZeroPredictedScores = aggregatePredictedScores(testIndx);
    aggreACC = mean(nonZeroPredictedScores ==labels(testIndx));

    aggregateResults_BSVC.predictedScores = aggregatePredictedScores;
    aggregateResults_BSVC.aggreACC = aggreACC;

    save(strcat('Results/aggregateResults/aggregatePredictedScores_BSVC_fc567.mat'),'aggregateResults_BSVC');

    fprintf('Mean correlation between the aggregated predictions and GT predictions %f %f\n',aggreACC);
