%numImages  = 32500; % for a random set of 5 classes from imNet
% numImages = 2208; % 10 class of dogs
% numClasses = 10;
% kFold = 5;
% 
% foldIndices = crossvalind('Kfold', numImages, kFold);
% 
% TestingNIter = [];
% TrainingNIter = [];
% 
% for k = [1, 2, 3]
%     testIndx = find(foldIndices == k);
%     trainIndx = find(foldIndices ~=k);
%     
%     TestingNIter = [TestingNIter testIndx];
%     TrainingNIter = [TrainingNIter trainIndx];
% end
% 
% save(strcat('Data/TrainingNIter_',num2str(numClasses),'C_kFold.mat'),'TrainingNIter');
% save(strcat('Data/TestingNIter_',num2str(numClasses),'C_kFold.mat'),'TestingNIter');

%
% numTrainImgs = round(trainSplit*numImages);
%
% TrainingNIter = [];
% TestingNIter = [];
%
% for n = 1:10
%     indx = randperm(numImages);
%
%     trainIndx = indx(1:numTrainImgs);
%     testIndx = indx(1+numTrainImgs:end);
%
%     TrainingNIter = [TrainingNIter trainIndx'];
%     TestingNIter = [TestingNIter testIndx'];
% end

%save(strcat('Data/TrainingNIter_',num2str(numClasses),'Class.mat'),'TrainingNIter');
%save(strcat('Data/TestingNIter_',num2str(numClasses),'Class.mat'),'TestingNIter');