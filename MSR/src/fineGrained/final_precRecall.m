%%Individual layer's predictions.
DIR_PATH = '/media/deepti/J/MSR/fineGrained/Data/';

load(strcat(DIR_PATH,'TestingNIter_10Class.mat'));
load(strcat(DIR_PATH,'dogs_groundTruthLabels_1-10.mat'));

labels = groundTruthLabels;
numClasses = 10;


prec7 = [];
rec7 = [];
diagElems =[];
for split = 1:3
    
    testIndx = (TestingNIter(:,split));
    
    gLabels = labels(testIndx);
    %load the per layer files.
    load(strcat('Results/finalResults/perLayerResults_1_10_split',num2str(split),'.mat'));
    
    predY7 = perLayerResults_1_10_split.Y_hat7;
    predY6 = perLayerResults_1_10_split.Y_hat6;
    predY5 = perLayerResults_1_10_split.Y_hat5;
    predY5c = perLayerResults_1_10_split.Y_hat5c;
    
    cMat7 = confusionmat(gLabels, predY7);
    
    colSum = sum(cMat7); % denom of prec.
    rowSum = sum(cMat7,2)'; % denom of recall.
    
    for c = 1:numClasses
        diagElems = [diagElems cMat7(c,c)];
    end
    
    prec7 = [prec7 mean(diagElems./colSum)];
    rec7 = [rec7 mean(diagElems./rowSum)];

    meanPrec7 = mean(prec7);
    meanRec7 = mean(rec7);
end


