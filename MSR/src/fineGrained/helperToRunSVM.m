DIR_PATH = '/media/deepti/J/MSR/fineGrained/';
load(strcat(DIR_PATH,'Data/dogs_fc7Features_1-25.mat'));
load(strcat(DIR_PATH,'Data/dogs_fc6Features_1-25.mat'));
load(strcat(DIR_PATH,'Data/dogs_pool5Features_1-25.mat'));
load(strcat(DIR_PATH,'Data/dogs_groundTruthLabels_1-25.mat'));

[medACC7, testY, Y_hat7, runTimeForAllImgs7] = executeSVM(fc7Features,groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_25Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_25Class.mat'), 'fc7');
[medACC6, testY, Y_hat6, runTimeForAllImgs6] = executeSVM(fc6Features,groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_25Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_25Class.mat'), 'fc6');
[medACC5, testY, Y_hat5, runTimeForAllImgs5] = executeSVM(pool5Features,groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_25Class.mat'), strcat(DIR_PATH,'Data/TestingNIter_25Class.mat'), 'pool5');


DIR_PATH = '/media/deepti/J/MSR/fineGrained/';
load(strcat(DIR_PATH,'Data/dogs_fc7Features_1-10.mat'));
load(strcat(DIR_PATH,'Data/dogs_fc6Features_1-10.mat'));
load(strcat(DIR_PATH,'Data/dogs_pool5Features_1-10.mat'));
load(strcat(DIR_PATH,'Data/pool5Compress_4096.mat'));
load('/media/deepti/J/MSR/fineGrained/Data/dogs_groundTruthLabels_1-10.mat');


[accuracy, runTimeForAllImgs, predictedLabels] = executeRDF(pool5Features_altDrop,groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat') , strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'));
[accuracy, runTimeForAllImgs, predictedLabels] = executeRDF(pool5Compress_4096,groundTruthLabels, 1, strcat(DIR_PATH,'Data/TrainingNIter_10Class.mat') , strcat(DIR_PATH,'Data/TestingNIter_10Class.mat'));




perLayerResults_1_25_split1 = struct;
perLayerResults_1_25_split1 .medACC7 = medACC7;
perLayerResults_1_25_split1 .Y_hat7 = Y_hat7;
perLayerResults_1_25_split1 .runTimeForAllImgs7 = runTimeForAllImgs7;

perLayerResults_1_25_split1 .medACC6 = medACC6;
perLayerResults_1_25_split1 .Y_hat6 = Y_hat6;
perLayerResults_1_25_split1 .runTimeForAllImgs6 = runTimeForAllImgs6;

perLayerResults_1_25_split1 .medACC5 = medACC5;
perLayerResults_1_25_split1 .Y_hat5 = Y_hat5;
perLayerResults_1_25_split1 .runTimeForAllImgs5 = runTimeForAllImgs5;

save('Results/perLayerResults_1_25_split1.mat','perLayerResults_1_25_split1');
