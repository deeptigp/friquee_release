%% This method runs an SVR on the set of datapoints that got classified to have the current layer as the label. This is just to evaluate if the binary classifier is doing a good job.. For ex: If 20 data points got the label as pool5, we use pool5Features to predict their interestingness scores in this method.
function predictedResults = evaluateClassifier(classifierResults, layer,trainTestSplit)
    DATA_PATH = '/media/deepti/J/MSR/fineGrained/Data/';
    load(strcat(DATA_PATH,'dogs_groundTruthLabels_1-10.mat'));
    %load(strcat(DATA_PATH,'dogs_groundTruthLabels_1-25.mat'));

    labels = groundTruthLabels;
    
    predictedResults = struct;

    class1Indices = classifierResults.testIndx(classifierResults.Y_hat==1);
    trainIndx = classifierResults.trainIndx;
    
    % Lets train SVCs
    if(strcmp(layer,'pool5'))
        disp('pool5')
        load(strcat(DATA_PATH,'pool5Compress_4096.mat'));
        %load(strcat(DATA_PATH,'dogs_pool5Features_1-10.mat'));
        %load(strcat(DATA_PATH,'dogs_pool5Features_1-25.mat'));
        %[accuracy, predictedLabels, runTimeForAllImgs] = ensembleDecisionForests(pool5Features(trainIndx,:), labels(trainIndx), pool5Features(class1Indices,:),labels(class1Indices), 300);
        
        [medACC, testY, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(pool5Compress_4096(trainIndx,:),labels(trainIndx),pool5Compress_4096(class1Indices,:),labels(class1Indices),'pool5',trainTestSplit);
        
        predictedResults.medACC = medACC;
        predictedResults.ePred5 = Y_hat;
        predictedResults.runTimeForAllImgs5 = runTimeForAllImgs;
  
    elseif(strcmp(layer,'fc6'))
        disp('fc6')
        load(strcat(DATA_PATH,'dogs_fc6Features_1-10.mat'));
        %load(strcat(DATA_PATH,'dogs_fc6Features_1-25.mat'));
        %[accuracy, predictedLabels, runTimeForAllImgs] = ensembleDecisionForests(fc6Features(trainIndx,:), labels(trainIndx), fc6Features(class1Indices,:),labels(class1Indices), 300);
        [medACC, testY, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(fc6Features(trainIndx,:),labels(trainIndx), fc6Features(class1Indices,:),labels(class1Indices),'fc6',trainTestSplit);
        predictedResults.medACC = medACC;
        predictedResults.ePred6 = Y_hat;
        predictedResults.runTimeForAllImgs6 = runTimeForAllImgs;
  
    elseif(strcmp(layer,'fc7'))
        disp('fc7')
        load(strcat(DATA_PATH,'dogs_fc7Features_1-10.mat'));
        %[accuracy, predictedLabels, runTimeForAllImgs] = ensembleDecisionForests(fc7Features(trainIndx,:), labels(trainIndx), fc7Features(class1Indices,:),labels(class1Indices), 300);
        %load(strcat(DATA_PATH,'dogs_fc7Features_1-25.mat'));
        [medACC, testY, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(fc7Features(trainIndx,:),labels(trainIndx), fc7Features(class1Indices,:),labels(class1Indices),'fc7',trainTestSplit);
        predictedResults.medACC = medACC;
        predictedResults.ePred7 = Y_hat;
        predictedResults.runTimeForAllImgs7 = runTimeForAllImgs;
    end

    predictedResults.class1Indices = class1Indices;
    predictedResults.eGT = labels(class1Indices);
end