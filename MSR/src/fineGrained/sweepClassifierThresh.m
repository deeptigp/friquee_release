trainTestSplit = 1;
DATA_PATH = '/media/deepti/J/MSR/fineGrained/Data/';
load(strcat(DATA_PATH,'dogs_groundTruthLabels_1-10.mat'));

labels = groundTruthLabels;

load('Results/kFold_567_1_10_k5.mat');

%load(strcat(DATA_PATH,'dogs_pool5Features_1-10.mat'));
load(strcat(DATA_PATH,'pool5Compress_4096.mat'));
load(strcat(DATA_PATH,'dogs_fc6Features_1-10.mat'));

load(strcat(DATA_PATH,'dogs_fc7Features_1-10.mat'));

load(strcat(DATA_PATH,'TrainingNIter_10Class.mat'));
load(strcat(DATA_PATH,'TestingNIter_10Class.mat'));

trainIndx = TrainingNIter(:,trainTestSplit);
testIndx = TestingNIter(:,trainTestSplit);

% Generate a random train/test split.
numImages = length(labels);

% Get the derived class labels.
GTLabels = kFold_567.derivedLayerLabels;

%% Get the label indexes.
class1LabelIndxs = find(GTLabels== 1);
class2LabelIndxs = find(GTLabels== 2);
class3LabelIndxs = find(GTLabels== 3);

binaryLabels1 = zeros(1,numImages);
binaryLabels2 = zeros(1,numImages);
binaryLabels3 = zeros(1,numImages);

%% Set the class labels for train and test data.
binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
binaryLabels2(class2LabelIndxs) = 1; % fc6 labels
binaryLabels3(class3LabelIndxs) = 1; % fc7 labels


% %% train with fc5, those that say no-fc5, test them with fc6.
trainNX = pool5Compress_4096(trainIndx,:);
testNX = pool5Compress_4096(testIndx,:);

trainNY = binaryLabels1(trainIndx)';
testNY = binaryLabels1(testIndx)';

[accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg, probEstimates5] = binarySVM_fast(trainNX, trainNY, testNX, testNY);

cascadeClassifierResults_aggre567_BSVC_pool5 = struct;
cascadeClassifierResults_aggre567_BSVC_pool5.trainIndx = trainIndx;
cascadeClassifierResults_aggre567_BSVC_pool5.testIndx = testIndx;
cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat = Y_hat;
cascadeClassifierResults_aggre567_BSVC_pool5.accuracy = accuracy;
cascadeClassifierResults_aggre567_BSVC_pool5.runTimeForAllImgs = runTimeForAllImgs;

minDV5 = min(probEstimates5);
maxDV5 = max(probEstimates5);

jumpVal5 = 0.4;

minDV6 = -2.12;
maxDV6 = 4.59;
jumpVal6 = 0.6;

layer5Thresh = [minDV5 : jumpVal5 :maxDV5];

layer6Thresh = [minDV6 : jumpVal6 :maxDV6];

numRows = length(layer5Thresh);
numCols = length(layer6Thresh);

sumNewYhat = [];
allAccur = zeros(numRows, numCols);
allRuntimes = zeros(numRows, numCols);

for indx5 = 1:numRows
    
    thresh5 = layer5Thresh(indx5);
    
    %% Fix the Y_hat's according to the thresholds
    
    newY_hat5(probEstimates5 <= thresh5) = 0;
    newY_hat5(probEstimates5 > thresh5) = 1;
    
    cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat = newY_hat5;
       
    cascadePredictedResults_aggre567_BSVC_pool5 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_pool5, 'pool5',trainTestSplit, pool5Compress_4096);
    
    aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_pool5.class1Indices) = cascadePredictedResults_aggre567_BSVC_pool5.ePred5;
    
    
    %% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_BSVC_pool5.testIndx(cascadeClassifierResults_aggre567_BSVC_pool5.Y_hat==0);
    
    fprintf('Number of not fc5 images: %d',length(notFC5Indxs));
    
    fc6Compressed = compressData(fc6Features, 2000);
    
    trainNX = fc6Compressed(trainIndx,:);
    testNX = fc6Compressed(notFC5Indxs,:);
    
    trainNY = binaryLabels2(trainIndx)';
    testNY = binaryLabels2(notFC5Indxs)';
    
    [accuracy, testY, Y_hat, runTimeForAllImgs, runTimePerImg, probEstimates6] = binarySVM_fast(trainNX, trainNY, testNX, testNY);
    cascadeClassifierResults_aggre567_BSVC_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_BSVC_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_BSVC_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre567_BSVC_fc6.runTimeForAllImgs = runTimeForAllImgs;
    
    
    for indx6 = 1:numCols
        
        thresh6 = layer6Thresh(indx6);
        
        newY_hat6(probEstimates6 <= thresh6) = 0;
        newY_hat6(probEstimates6 > thresh6) = 1;
        
        cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat = newY_hat6;
        
        cascadePredictedResults_aggre567_BSVC_fc6 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc6, 'fc6',trainTestSplit, fc6Compressed);
        
        aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc6.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc6.ePred6;
        
        %% Those rejected by fc6 will be considered by fc7.
        notFC6Indxs = cascadeClassifierResults_aggre567_BSVC_fc6.testIndx(cascadeClassifierResults_aggre567_BSVC_fc6.Y_hat==0);
        
        fprintf('Number of not fc6 images: %d\n',length(notFC6Indxs));
        
        Y_hat = ones(size(notFC6Indxs));
        
        cascadeClassifierResults_aggre567_BSVC_fc7.trainIndx = trainIndx;
        cascadeClassifierResults_aggre567_BSVC_fc7.testIndx = notFC6Indxs;
        cascadeClassifierResults_aggre567_BSVC_fc7.Y_hat = Y_hat;
        
        fprintf('Current threshold setting of layers 6 and 5 %f %f\n',layer6Thresh(indx6), layer5Thresh(indx5));
        
        if(~isempty(notFC6Indxs))
            cascadePredictedResults_aggre567_BSVC_fc7 = evaluateClassifier_svm(cascadeClassifierResults_aggre567_BSVC_fc7, 'fc7',trainTestSplit, fc7Features);
            
            aggregatePredictedScores(cascadePredictedResults_aggre567_BSVC_fc7.class1Indices) = cascadePredictedResults_aggre567_BSVC_fc7.ePred7;
        end
        
        nonZeroPredictedScores = aggregatePredictedScores(testIndx);
        aggreACC = mean(nonZeroPredictedScores ==labels(testIndx)');
        
        aggregateResults_BSVC.predictedScores = aggregatePredictedScores;
        aggregateResults_BSVC.aggreACC = aggreACC;
        
        
        fprintf('Mean correlation between the aggregated predictions and GT predictions %f %f\n',aggreACC);
        
        allAccur(indx5, indx6) = aggreACC;
        
        temp_runningTimes;
        
        allRuntimes(indx5, indx6) = totalTestTime;
        
    end
end


