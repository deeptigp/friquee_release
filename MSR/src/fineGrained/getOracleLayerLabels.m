function getOracleLayerLabels(trainTestSplit)
%    trainTestSplit = 2;
    DATA_PATH = '/media/deepti/J/MSR/fineGrained/Data/';
    load(strcat(DATA_PATH,'dogs_groundTruthLabels_1-10.mat'));
    load(strcat(DATA_PATH,'pool5Compress_4096.mat'));

    load(strcat(DATA_PATH,'dogs_fc6Features_1-10.mat'));

    load(strcat(DATA_PATH,'dogs_fc7Features_1-10.mat'));

    load(strcat(DATA_PATH,'TrainingNIter_10Class.mat'));
    load(strcat(DATA_PATH,'TestingNIter_10Class.mat'));

    %% All initializations..

    labels = groundTruthLabels;
    
    
    % Get the current train/test split.
    trainIndx = TrainingNIter(:,trainTestSplit);
    testIndx = TestingNIter(:,trainTestSplit);

    perClassLabels = zeros(length(testIndx),3);
    
    
    [acc5, gt5, pred5] = runSVM_fast(pool5Compress_4096(trainIndx,:),labels(trainIndx),pool5Compress_4096(testIndx,:),labels(testIndx),'pool5');

    [acc6, gt6, pred6] = runSVM_fast(fc6Features(trainIndx,:),labels(trainIndx),fc6Features(testIndx,:),labels(testIndx),'fc6');

    [acc7, gt7, pred7] = runSVM_fast(fc7Features(trainIndx,:),labels(trainIndx),fc7Features(testIndx,:),labels(testIndx),'fc7');

    perClassLabels(:,:) = [(pred5==gt5) (pred6==gt6) (pred7==gt7)];
    

    %% Logic to assign class labels pool5 (1), fc6 (2), or fc7 (3)

    fc5Indexes = find(perClassLabels(:,1)==1);

    goldTestLayerLabels(fc5Indexes) = 1;

    fc6Indexes = find(perClassLabels(:,1)==0 & perClassLabels(:,2)== 1);

    goldTestLayerLabels(fc6Indexes) = 2;

    fc7Indexes = find(perClassLabels(:,1)==0 & perClassLabels(:,2)== 0 & perClassLabels(:,3)== 1);

    goldTestLayerLabels(fc7Indexes) = 3;

    %% There still are a few data points which were all classified incorrectly by all three layers.. As of now, I am assigning them the label pool5
    dontMatter = find(perClassLabels(:,1)==0 & perClassLabels(:,2)== 0 & perClassLabels(:,3)== 0);

    goldTestLayerLabels(dontMatter) = 1;

    save(strcat('Results/goldLayerLabels',num2str(trainTestSplit),'.mat'),'goldTestLayerLabels');
end