%%Individual layer's predictions.
%Returns mean average prec. and recall.

% function [meanPrec, meanRec] = computePrecRecall(gLabels, predY)
% 
% diagElems =[];
% 
% cMat = confusionmat(gLabels, predY);
% 
% 
% colSum = sum(cMat); % denom of prec.
% rowSum = sum(cMat,2)'; % denom    of recall.
% 
% numClasses = length(colSum);
% 
% for c = 1:numClasses
%     diagElems = [diagElems cMat(c,c)];
% end
% 
% 
% prec = mean(diagElems./colSum);
% rec = mean(diagElems./rowSum);
% 
% meanPrec = mean(prec);
% meanRec = mean(rec);
% 
% end

function [prec, recall] = computePrecRecall(gLabels, predY)
    cMat = confusionmat(gLabels, predY);
    
    %% wrt class 1
    recall = cMat(2,2)/(cMat(2,2)+cMat(2,1));
    
    prec = cMat(2,2)/(cMat(2,2)+cMat(1,2));
    
end