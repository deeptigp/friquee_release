function predictedResults = evaluateClassifier(classifierResults, layer)
    load('../interestingness/Data/pool5Features.mat');
    load('../interestingness/Data/fc6Features.mat');
    load('../interestingness/Data/fc7Features.mat');
    load('../interestingness/Data/layer4Features.mat');
    load('../interestingness/Data/layer3Features.mat');
    
    load('../interestingness/Data/interestingness_scores.mat');
    
    
    labels = interestingness_score;
    
    predictedResults = struct;

    %% Evaluation of the goodness of this prediction

    % Lets take the 80-20 train test splits.. For all the images where fc5 was
    % the predicted class, use pool5, fc6 features for class 2 and fc7 for
    % class 3.

    %%
    class1Indices = classifierResults.testIndx(classifierResults.Y_hat==1); 
    trainIndx = classifierResults.trainIndx;
    
    % Lets train an SVRs
    disp('pool5')
    [eLCC5,eRHO5, ePred5, eRMSE5, psnrVal5] = temp_linearSVR(pool5Features(trainIndx,:),labels(trainIndx)',pool5Features(class1Indices,:),labels(class1Indices)','fc5');
    disp('fc4')
    [eLCC4,eRHO4, ePred4, eRMSE4, psnrVal4] = temp_linearSVR(layer4Features(trainIndx,:),labels(trainIndx)',layer4Features(class1Indices,:),labels(class1Indices)','fc4');
    disp('fc6')
    [eLCC6,eRHO6, ePred6, eRMSE6, psnrVal6] = temp_linearSVR(fc6Features(trainIndx,:),labels(trainIndx)',fc6Features(class1Indices,:),labels(class1Indices)','fc6');
    disp('fc7')
    [eLCC7,eRHO7, ePred7, eRMSE7, psnrVal7] = temp_linearSVR(fc7Features(trainIndx,:),labels(trainIndx)',fc7Features(class1Indices,:),labels(class1Indices)','fc7');  
    disp('fc3')
    [eLCC3,eRHO3, ePred3, eRMSE3, psnrVal3] = temp_linearSVR(layer3Features(trainIndx,:),labels(trainIndx)',layer3Features(class1Indices,:),labels(class1Indices)','fc3');
    
    predictedResults.class1Indices = class1Indices;
    predictedResults.eGT = labels(class1Indices);
 
    predictedResults.eLCC3 = eLCC3;
    predictedResults.eRHO3 = eRHO3;
    predictedResults.ePred3 = ePred3;
    predictedResults.rmse3  = eRMSE3; 
    predictedResults.psnrVal3  = psnrVal3; 
 
    predictedResults.eLCC4 = eLCC4;
    predictedResults.eRHO4 = eRHO4;
    predictedResults.ePred4 = ePred4;
    predictedResults.rmse4  = eRMSE4; 
    predictedResults.psnrVal4  = psnrVal4; 
    
    predictedResults.eLCC5 = eLCC5;
    predictedResults.eRHO5 = eRHO5;
    predictedResults.ePred5 = ePred5;
    predictedResults.rmse5  = eRMSE5; 
    predictedResults.psnrVal5  = psnrVal5; 
    
    predictedResults.eLCC6 = eLCC6;
    predictedResults.eRHO6 = eRHO6;
    predictedResults.ePred6 = ePred6;
    predictedResults.rmse6  = eRMSE6; 
    predictedResults.psnrVal6  = psnrVal6; 
    
    predictedResults.eLCC7 = eLCC7;
    predictedResults.eRHO7 = eRHO7;
    predictedResults.ePred7 = ePred7;
    predictedResults.rmse7  = eRMSE7; 
    predictedResults.psnrVal7  = psnrVal7; 

   save(strcat('../interestingness/Results/predictedBinaryResults_34567_',layer,'.mat'),'predictedResults');
end