%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7

function [classifierResults, predictedResults] = binaryClassifier(layer)

   if(strcmp(layer,'pool5'))
        load('../interestingness/Data/pool5Features.mat');
        feat = pool5Features;
        classLabel = 1;
    elseif(strcmp(layer,'fc6'))
        load('../interestingness/Data/fc6Features.mat');
        feat = fc6Features;
        classLabel = 2;
    elseif(strcmp(layer,'fc7'))
        load('../interestingness/Data/fc7Features.mat');
        feat = fc7Features;
        classLabel = 3;
    elseif(strcmp(layer,'fc4'))
        load('../interestingness/Data/layer4Features.mat');
        feat = layer4Features;
        classLabel = 4;
    elseif(strcmp(layer,'fc3'))
        load('../interestingness/Data/layer3Features.mat');
        feat = layer3Features;
        classLabel = 5;
   end

    classifierResults = struct;

    load('../interestingness/Data/interestingness_scores.mat');

    labels = interestingness_score;

    load('../interestingness/Results/kFold_34567.mat');

    load('../interestingness/Data/TrainingNIter.mat');
    load('../interestingness/Data/TestingNIter.mat');

    trainIndx = TrainingNIter(:,1);
    testIndx = TestingNIter(:,1);


    % Generate a random train/test split.
    numImages = size(feat,1);

    GTLabels = kFold_34567.derivedGTLabels;

    classLabelIndxs = find(kFold_34567.derivedGTLabels == classLabel);

    binaryLabels = zeros(1,numImages);

    binaryLabels(classLabelIndxs) = 1;


    disp('Num of test images with label = 1');
    sum(binaryLabels(testIndx))

    [accuracy, Y_hat] = binarySVC(feat(trainIndx,:),binaryLabels(trainIndx)',feat(testIndx,:),binaryLabels(testIndx)');

    classifierResults.trainIndx = trainIndx;
    classifierResults.testIndx = testIndx;
    classifierResults.Y_hat = Y_hat;
    classifierResults.accuracy = accuracy;
    save(strcat('../interestingness/Results/classifierResults_34567__',layer,'.mat'),'classifierResults');

    predictedResults = evaluateClassifier(classifierResults, layer);
end