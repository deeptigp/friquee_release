%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7

%function aggregateResults_RDF = cascadedBinaryClassifiers_imNet_DF_567()
    DATA_PATH = '/media/deepti/I/data/interestingness/';
    DATA_PATH1 = '/home/deepti/research/MSR/src/interestingness/';
    load(strcat(DATA_PATH,'imNet/Data/groundTruthLabels_10Class.mat'));

    labels = groundTruthLabels;

    load(strcat(DATA_PATH,'imNet/Results/kFold_567_10Class.mat'));

    load(strcat(DATA_PATH,'imNet/Data/pool5Features_10Class.mat'));

    load(strcat(DATA_PATH,'imNet/Data/fc6Features_10Class.mat'));
    
    load(strcat(DATA_PATH,'imNet/Data/fc7Features_10Class.mat'));
    
    load(strcat(DATA_PATH,'imNet/Data/TrainingNIter_10Class.mat'));
    load(strcat(DATA_PATH,'imNet/Data/TestingNIter_10Class.mat'));

    trainIndx = TrainingNIter(:,1);
    testIndx = TestingNIter(:,1);

    %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);

    cascadeClassifierResults_aggre567_RDF_pool5 = struct;
    cascadeClassifierResults_aggre567_RDF_fc6 = struct;
    cascadeClassifierResults_aggre567_RDF_fc7 = struct;

    % Generate a random train/test split.
    numImages = length(labels);

    % Get the derived class labels.
    GTLabels = kFold_567.derivedLayerLabels;

    %% Get the label indexes.
    class1LabelIndxs = find(GTLabels== 1);
    class2LabelIndxs = find(GTLabels== 2);
    class3LabelIndxs = find(GTLabels== 3);

    binaryLabels1 = zeros(1,numImages);
    binaryLabels2 = zeros(1,numImages);
    binaryLabels3 = zeros(1,numImages);

    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1; % pool5 labels
    binaryLabels2(class2LabelIndxs) = 1; % fc6 labels
    binaryLabels3(class3LabelIndxs) = 1; % fc7 labels


    %% train with fc5, those that say no-fc5, test them with fc6.
    % Normalize the features before passing them through RDFs

    normPool5Features = featureNormalize(pool5Features);
    normFC6Features = featureNormalize(fc6Features);


    trainNX = normPool5Features(trainIndx,:);
    testNX = normPool5Features(testIndx,:);

    [accuracy, Y_hat, runTimeForAllImgs, scores, stdevs,BaggedEnsemble] = ensembleDecisionForests(trainNX,binaryLabels1(trainIndx)',testNX,binaryLabels1(testIndx)');
 
    cascadeClassifierResults_aggre567_RDF_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_RDF_pool5.testIndx = testIndx;
    cascadeClassifierResults_aggre567_RDF_pool5.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_RDF_pool5.accuracy = accuracy;
    cascadeClassifierResults_aggre567_RDF_pool5.runTimeForAllImgs = runTimeForAllImgs;


    save(strcat(DATA_PATH1,'imNet/Results/aggregateResults/cascadeClassifierResults_aggre567_RDF_pool5.mat'),'cascadeClassifierResults_aggre567_RDF_pool5');

    cascadePredictedResults_aggre567_RDF_pool5 = evaluateClassifier_imNet(cascadeClassifierResults_aggre567_RDF_pool5, 'pool5');

    save(strcat(DATA_PATH1,'imNet/Results/aggregateResults/cascadePredictedResults_aggre567_RDF_pool5.mat'),'cascadePredictedResults_aggre567_RDF_pool5');

    %% Aggregating the predictions of the data points that were predicted to belong to pool5 class..
    aggregatePredictedScores(cascadePredictedResults_aggre567_RDF_pool5.class1Indices) = cascadePredictedResults_aggre567_RDF_pool5.ePred5;


    %% Those that say no-fc5, test them with fc6.
    notFC5Indxs = cascadeClassifierResults_aggre567_RDF_pool5.testIndx(cascadeClassifierResults_aggre567_RDF_pool5.Y_hat==0);

    fprintf('Number of not fc5 images: %d',length(notFC5Indxs));

    trainNX = normFC6Features(trainIndx,:);
    testNX = normFC6Features(notFC5Indxs,:);

    [accuracy, Y_hat, runTimeForAllImgs, scores, stdevs,BaggedEnsemble] = ensembleDecisionForests(trainNX, binaryLabels2(trainIndx)', testNX, binaryLabels2(notFC5Indxs)');

    cascadeClassifierResults_aggre567_RDF_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_RDF_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre567_RDF_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre567_RDF_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre567_RDF_fc6.runTimeForAllImgs = runTimeForAllImgs;


    save(strcat(DATA_PATH1,'imNet/Results/aggregateResults/cascadeClassifierResults_aggre567_RDF_fc6.mat'),'cascadeClassifierResults_aggre567_RDF_fc6');

    cascadePredictedResults_aggre567_RDF_fc6 = evaluateClassifier_imNet(cascadeClassifierResults_aggre567_RDF_fc6, 'fc6');

    save(strcat(DATA_PATH1,'imNet/Results/aggregateResults/cascadePredictedResults_aggre567_RDF_fc6.mat'),'cascadePredictedResults_aggre567_RDF_fc6');

    aggregatePredictedScores(cascadePredictedResults_aggre567_RDF_fc6.class1Indices) = cascadePredictedResults_aggre567_RDF_fc6.ePred6;

    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre567_RDF_fc6.testIndx(cascadeClassifierResults_aggre567_RDF_fc6.Y_hat==0);

    fprintf('Number of not fc6 images: %d',length(notFC6Indxs));

    Y_hat = ones(size(notFC6Indxs));

    cascadeClassifierResults_aggre567_RDF_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre567_RDF_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre567_RDF_fc7.Y_hat = Y_hat;

    save(strcat(DATA_PATH1,'imNet/Results/aggregateResults/cascadeClassifierResults_aggre567_RDF_fc7.mat'),'cascadeClassifierResults_aggre567_RDF_fc7');

    cascadePredictedResults_aggre567_RDF_fc7 = evaluateClassifier_imNet(cascadeClassifierResults_aggre567_RDF_fc7, 'fc7');

    aggregatePredictedScores(cascadePredictedResults_aggre567_RDF_fc7.class1Indices) = cascadePredictedResults_aggre567_RDF_fc7.ePred7;
    save(strcat(DATA_PATH1,'imNet/Results/aggregateResults/cascadePredictedResults_aggre567_RDF_fc7.mat'),'cascadePredictedResults_aggre567_RDF_fc7');


    nonZeroPredictedScores = aggregatePredictedScores(testIndx);
    aggreACC = mean(nonZeroPredictedScores ==labels(testIndx));

    aggregateResults_RDF.predictedScores = aggregatePredictedScores;
    aggregateResults_RDF.aggreACC = aggreACC;

    save(strcat(DATA_PATH1,'imNet/Results/aggregateResults/aggregatePredictedScores_RDF_fc567.mat'),'aggregateResults_RDF');

    fprintf('Mean aggregate accuracy %f\n',aggreACC);
