%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM classifier for NIter numbere of times.
function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg] = runSVM(trainX, trainY, testX, testY, layer)
    addpath('..');
    fid = fopen('train_ind.txt','w');

    for itr_im = 1:size(trainX,1)
        fprintf(fid,'%d ',trainY(itr_im,1));
        for itr_feat =1:size(trainX,2)
            fprintf(fid,'%d:%f ',itr_feat,trainX(itr_im,itr_feat));
        end
        fprintf(fid,'\n');
    end

    fclose(fid);
    
    trainIndFile = strcat('train_ind_',layer,'.txt');
    rangeFile = strcat('range_',layer);
    modelFile = strcat('model_',layer);
    
    % Execute it.
    system(['./svm-scale -l -1 -u 1 -s', ' ', rangeFile,' ', trainIndFile, '> /media/deepti/SamsungSSD/ImageNet/tmp/train_scale']);

    % Construct the file ( input ) suitably for testing the SVM
    fid = fopen('test_ind.txt','w');
    for itr_im = 1:size(testX,1)
        fprintf(fid,'%d ',testY(itr_im,1));
        for itr_param = 1:size(testX,2)
            fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);

    % Scale the data.
    system(['./svm-scale  -r',' ', rangeFile,' ','test_ind.txt > /media/deepti/SamsungSSD/ImageNet/tmp/test_ind_scaled']);

    gamma = 2^-9;
    if(strcmp(layer,'pool5'))
        C = 2;
    elseif(strcmp(layer,'fc6'))
        C = 0.5;
    elseif(strcmp(layer,'fc7'))
        C = 0.25;
    end

    medACC = zeros(length(C),length(gamma));

    %% t: Kernel type. %s: svm type. g: gamma c: cost.
    %  system('svm-train  -s 0 -t 2 -b 1 -q train_scale model');
    
    for cItr = 1:length(C)
        for gItr = 1:length(gamma)
            cmd = ['./svm-train  -t 2 -s 0 -b 1 -g',' ',num2str(gamma(gItr)),' -c',' ',num2str(C(cItr)),' -q /media/deepti/SamsungSSD/ImageNet/tmp/train_scale /media/deepti/SamsungSSD/ImageNet/tmp/',modelFile];
            cmd
            system(cmd);

            tStart = tic;
              
            %system('svm-predict  -b 1 test_ind_scaled model output.txt > dump');
            system(['./svm-predict  -b 0 /media/deepti/SamsungSSD/ImageNet/tmp/test_ind_scaled /media/deepti/SamsungSSD/ImageNet/tmp/',modelFile,' ','output.txt > dump']);

            runTimeForAllImgs = toc(tStart);

            
            runTimePerImg = runTimeForAllImgs/length(testY);

            fprintf('Prediction Time for 1 image and all images is %f %d respectively',runTimePerImg, runTimeForAllImgs);

            load output.txt;
            % Predicted scores
            Y_hat=output;
            acc = mean(Y_hat == testY);
            
            medACC(cItr, gItr) = acc
            system('rm output.txt dump');
        end
    end
    
    %system('rm /media/deepti/SamsungSSD/ImageNet/tmp/train_scale range test_ind.txt train_ind.txt /media/deepti/SamsungSSD/ImageNet/tmp/test_ind_scaled');
    system('rm test_ind.txt /media/deepti/SamsungSSD/ImageNet/tmp/test_ind_scaled');
end