% Get features from pool5, fc6, and fc7 for a different dataset.
%clear;
% load('../interestingness/imNet/Data/imNet_pool5Features_1-25.mat');
% load('../interestingness/imNet/Data/imNet_fc6Features_1-25.mat');
% load('../interestingness/imNet/Data/imNet_fc7Features_1-25.mat');
% load('../interestingness/imNet/Data/imNet_groundTruthLabels_1-25.mat');

DATA_PATH = '/media/deepti/I/data/';
load(strcat(DATA_PATH,'interestingness/imNet/Data/fc6Features_10Class.mat'));
load(strcat(DATA_PATH,'interestingness/imNet/Data/fc7Features_10Class.mat'));
load(strcat(DATA_PATH,'interestingness/imNet/Data/pool5Features_10Class.mat'));
load(strcat(DATA_PATH,'interestingness/imNet/Data/groundTruthLabels_10Class.mat'));

labels = groundTruthLabels;

numImages = size(pool5Features,1);

kFold = 4;

foldIndices = crossvalind('Kfold', numImages, kFold);

predictedImNetClassLabels = zeros(numImages,3); % obtained from pool5, fc6, and fc7.

derivedLayerLabels = zeros(numImages,1);

kFold_567 = struct;

for k = 1:kFold
    k
    testIndx = find(foldIndices==k);
    trainIndx = find(foldIndices ~=k);
    
    testLabels = labels(testIndx);
    trainLabels = labels(trainIndx);
    
    [size(testIndx) size(trainIndx)]
        
    [acc5, gt5, pred5] = runSVM_fast(pool5Features(trainIndx,:),trainLabels,pool5Features(testIndx,:),testLabels,'pool5');

    [acc6, gt6, pred6] = runSVM(fc6Features(trainIndx,:),trainLabels,fc6Features(testIndx,:),testLabels,'fc6');

    [acc7, gt7, pred7] = runSVM(fc7Features(trainIndx,:),trainLabels,fc7Features(testIndx,:),testLabels,'fc7');

    predictedImNetClassLabels(testIndx,:) = [(pred5==gt5) (pred6==gt6) (pred7==gt7)];    
end

%% Logic to assign class labels pool5 (1), fc6 (2), or fc7 (3)

fc5Indexes = find(predictedImNetClassLabels(:,1));

notFC5Indexes = find(predictedImNetClassLabels(:,1) == 0);

derivedLayerLabels(fc5Indexes) = 1;

fc6Indexes = notFC5Indexes(predictedImNetClassLabels(notFC5Indexes,2) == 1);

derivedLayerLabels(fc6Indexes) = 2;

notFC6Indexes = notFC5Indexes(predictedImNetClassLabels(notFC5Indexes,2) == 0);

fc7Indexes = notFC6Indexes(predictedImNetClassLabels(notFC6Indexes,3)==1);

derivedLayerLabels(fc7Indexes) = 3;

%% There still are a few data points which were all classified incorrectly by all three layers.. As of now, I am assigning them the label pool5
notFC7Indexes = notFC6Indexes(predictedImNetClassLabels(notFC6Indexes,3)==0);

derivedLayerLabels(notFC7Indexes) = 1;


kFold_567.foldIndices = foldIndices;
kFold_567.predictedImNetClassLabels = predictedImNetClassLabels;
kFold_567.derivedLayerLabels = derivedLayerLabels;

save(strcat(DATA_PATH,'interestingness/imNet/Results/kFold_567_10Class.mat'),'kFold_567');

