function [medACC, testY, Y_hat, runTimeForAllImgs] = binarySVM_fast(trainX, trainY, testX, testY, layer)
    % Create a model
    C = '2'; gamma = '1';
    fName = strcat('../interestingness/imNet/models/binarySVC_',layer,'.mat');
    trainOpts = ['-t 0 -s 0 -b 0 -g ',gamma ' -c ',C];

    trainedModel = svmtrain(trainY,double(trainX),trainOpts);

    save(fName,'trainedModel');
    tStart = tic;

    [Y_hat, medACC , ~] = svmpredict (testY, double(testX), trainedModel, '-b 0 -q');
    runTimeForAllImgs = toc(tStart)
end