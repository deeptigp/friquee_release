function [medACC, testY, Y_hat, runTimeForAllImgs, trainedModel] = runSVM_fast(trainX, trainY, testX, testY, layer)
    % Create a model
    gamma = '0.0020';%2^-9;
    DIRPATH = '../interestingness/imNet/models/';
    if(strcmp(layer,'pool5'))
        C = '2';
    elseif(strcmp(layer,'fc6'))
        C = '0.5';
    elseif(strcmp(layer,'fc7') || strcmp(layer,'pool4'))
        C = '0.25';
    end
    trainOpts = ['-t 0 -s 0 -b 1 -g ',gamma ' -c ',C];
    
    trainedModel = svmtrain(trainY,double(trainX),trainOpts);
    
    save([DIRPATH,'trainedModel_compress_',layer,'.mat'], 'trainedModel');
    
    tStart = tic;
    [Y_hat, medACC , ~] = svmpredict (testY, double(testX), trainedModel, '-b 1 -q');
    runTimeForAllImgs = toc(tStart)
end