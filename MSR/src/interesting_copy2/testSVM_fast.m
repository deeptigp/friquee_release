function [medACC, testY, Y_hat, runTimeForAllImgs] = testSVM_fast(testX, testY, layer)
    DIRPATH = '../interestingness/imNet/models/';
    if(strcmp(layer,'pool5'))
        load([DIRPATH,'trainedModel_',layer,'_orig.mat']);
    else
        load([DIRPATH,'trainedModel_',layer,'.mat']);
    end
    tStart = tic;
    [Y_hat, medACC , ~] = svmpredict (testY, double(testX), trainedModel, '-b 0 -q');
    runTimeForAllImgs = toc(tStart)
end