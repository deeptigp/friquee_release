%% THIS METHOD TRAINS A MULTI-CLASS CLASSIFIER: CURRENTLY FOR FC5/6/7
    load('Data/interestingness_scores.mat');

    labels = interestingness_score;
    
    load('Results/aggregateResults/kFold_4567.mat');

    load('Data/layer4Features.mat');
    load('Data/pool5Features.mat');
    load('Data/fc6Features.mat');
    load('Data/fc7Features.mat');
    
    load('Data/TrainingNIter.mat');
    load('Data/TestingNIter.mat');
   
    trainIndx = TrainingNIter(:,1);
    testIndx = TestingNIter(:,1);
   
    cascadeClassifierResults_aggre_layer4 = struct;
    cascadeClassifierResults_aggre_pool5 = struct;
    cascadeClassifierResults_aggre_fc6 = struct;
    cascadeClassifierResults_aggre_fc7 = struct;
    
    % Generate a random train/test split.
    numImages = length(labels);

    % Get the derived class labels.
    GTLabels = kFold_4567.derivedGTLabels;
    
    %% Here we collect the interestingness scores predicted from the three different cascaded layers.
    aggregatePredictedScores = zeros(length(labels),1);
    
    
    %% Get the label indexes.
    class1LabelIndxs = find(kFold_4567.derivedGTLabels == 1);
    class2LabelIndxs = find(kFold_4567.derivedGTLabels == 2);
    class3LabelIndxs = find(kFold_4567.derivedGTLabels == 3);
    class4LabelIndxs = find(kFold_4567.derivedGTLabels == 4); % Layer4
    
    binaryLabels1 = zeros(1,numImages);
    binaryLabels2 = zeros(1,numImages);
    binaryLabels3 = zeros(1,numImages);
    binaryLabels4 = zeros(1,numImages); % Layer4
    
    
    %% Set the class labels for train and test data.
    binaryLabels1(class1LabelIndxs) = 1;
    binaryLabels2(class2LabelIndxs) = 1;
    binaryLabels3(class3LabelIndxs) = 1;
    binaryLabels4(class4LabelIndxs) = 1; %Layer4
    
    %% train with layer4, those that say no-layer4, test them with pool5.
    [accuracy, Y_hat] = binarySVC(layer4Features(trainIndx,:),binaryLabels4(trainIndx)',layer4Features(testIndx,:),binaryLabels4(testIndx)');

    cascadeClassifierResults_aggre_layer4.trainIndx = trainIndx;
    cascadeClassifierResults_aggre_layer4.testIndx = testIndx;
    cascadeClassifierResults_aggre_layer4.Y_hat = Y_hat;
    cascadeClassifierResults_aggre_layer4.accuracy = accuracy;
    cascadeClassifierResults_aggre_layer4.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre_layer4.runTimePerImg = runTimePerImg;
    
    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre_layer4.mat'),'cascadeClassifierResults_aggre_layer4');
    
    cascadePredictedResults_aggre_layer4 = evaluateClassifier(cascadeClassifierResults_aggre_layer4, 'layer4');
    
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre_layer4.mat'),'cascadePredictedResults_aggre_layer4');
    
    aggregatePredictedScores(cascadePredictedResults_aggre_layer4.class1Indices) = cascadePredictedResults_aggre_layer4.ePred4;
    
    notFC4Indxs = cascadeClassifierResults_aggre_layer4.testIndx(cascadeClassifierResults_aggre_layer4.Y_hat==0); 
    
    %% train with fc5, those that say no-fc5, test them with fc6.
    [accuracy, Y_hat] = binarySVC(pool5Features(trainIndx,:),binaryLabels1(trainIndx)',pool5Features(notFC4Indxs,:),binaryLabels1(notFC4Indxs)');

    cascadeClassifierResults_aggre_pool5.trainIndx = trainIndx;
    cascadeClassifierResults_aggre_pool5.testIndx = notFC4Indxs;
    cascadeClassifierResults_aggre_pool5.Y_hat = Y_hat;
    cascadeClassifierResults_aggre_pool5.accuracy = accuracy;
    cascadeClassifierResults_aggre_pool5.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre_pool5.runTimePerImg = runTimePerImg;
    
    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre_pool5.mat'),'cascadeClassifierResults_aggre_pool5');
    
    cascadePredictedResults_aggre_pool5 = evaluateClassifier(cascadeClassifierResults_aggre_pool5, 'pool5');
    
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre_pool5.mat'),'cascadePredictedResults_aggre_pool5');
    aggregatePredictedScores(cascadePredictedResults_aggre_pool5.class1Indices) = cascadePredictedResults_aggre_pool5.ePred5;
    
    %% train with fc5, those that say no-fc5, test them with fc6.
    
    notFC5Indxs = cascadeClassifierResults_aggre_pool5.testIndx(cascadeClassifierResults_aggre_pool5.Y_hat==0); 
    
    [accuracy, Y_hat] = binarySVC(fc6Features(trainIndx,:),binaryLabels2(trainIndx)',fc6Features(notFC5Indxs,:),binaryLabels2(notFC5Indxs)');
    
    cascadeClassifierResults_aggre_fc6.trainIndx = trainIndx;
    cascadeClassifierResults_aggre_fc6.testIndx = notFC5Indxs;
    cascadeClassifierResults_aggre_fc6.Y_hat = Y_hat;
    cascadeClassifierResults_aggre_fc6.accuracy = accuracy;
    cascadeClassifierResults_aggre_fc6.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre_fc6.runTimePerImg = runTimePerImg;
    
    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre_fc6.mat'),'cascadeClassifierResults_aggre_fc6');
    
    cascadePredictedResults_aggre_fc6 = evaluateClassifier(cascadeClassifierResults_aggre_fc6, 'fc6');
    
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre_fc6.mat'),'cascadePredictedResults_aggre_fc6');
    aggregatePredictedScores(cascadePredictedResults_aggre_fc6.class1Indices) = cascadePredictedResults_aggre_fc6.ePred6;
    
    %% Those rejected by fc6 will be considered by fc7.
    notFC6Indxs = cascadeClassifierResults_aggre_fc6.testIndx(cascadeClassifierResults_aggre_fc6.Y_hat==0); 
    
    [accuracy, Y_hat] = binarySVC(fc7Features(trainIndx,:),binaryLabels3(trainIndx)',fc7Features(notFC6Indxs,:),binaryLabels3(notFC6Indxs)');
    
    cascadeClassifierResults_aggre_fc7.trainIndx = trainIndx;
    cascadeClassifierResults_aggre_fc7.testIndx = notFC6Indxs;
    cascadeClassifierResults_aggre_fc7.Y_hat = Y_hat;
    cascadeClassifierResults_aggre_fc7.accuracy = accuracy;
    cascadeClassifierResults_aggre_fc7.runTimeForAllImgs = runTimeForAllImgs;
    cascadeClassifierResults_aggre_fc7.runTimePerImg = runTimePerImg;
    save(strcat('Results/aggregateResults/cascadeClassifierResults_aggre_fc7.mat'),'cascadeClassifierResults_aggre_fc7');
   
    cascadePredictedResults_aggre_fc7 = evaluateClassifier(cascadeClassifierResults_aggre_fc7, 'fc7');
    
    save(strcat('Results/aggregateResults/cascadePredictedResults_aggre_fc7.mat'),'cascadePredictedResults_aggre_fc7');
    aggregatePredictedScores(cascadePredictedResults_aggre_fc7.class1Indices) = cascadePredictedResults_aggre_fc7.ePred7;