%% This method takes in X ( data / brisque features ) and Y ( DMOS scores) and runs the SVM classifier for NIter numbere of times.
function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg] = runSVM1(trainX, trainY, testX, testY, layer)
    addpath('..');
    fid = fopen('train_ind_bc.txt','w');

    for itr_im = 1:size(trainX,1)
        fprintf(fid,'%d ',trainY(itr_im,1));
        for itr_feat =1:size(trainX,2)
            fprintf(fid,'%d:%f ',itr_feat,trainX(itr_im,itr_feat));
        end
        fprintf(fid,'\n');
    end

    fclose(fid);

    % Execute it.
    system('./svm-scale-bc -l -1 -u 1 -s range_bc train_ind_bc.txt > train_scale_bc');

    % Construct the file ( input ) suitably for testing the SVM
    fid = fopen('test_ind_bc.txt','w');
    for itr_im = 1:size(testX,1)
        fprintf(fid,'%d ',testY(itr_im,1));
        for itr_param = 1:size(testX,2)
            fprintf(fid,'%d:%f ',itr_param,testX(itr_im,itr_param));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);

    % Scale the data.
    system('./svm-scale-bc  -r range_bc test_ind_bc.txt > test_ind_bc_scaled');


  gamma = 2^-9;
    if(strcmp(layer,'pool5'))
        C = 2;
    elseif(strcmp(layer,'fc6'))
        C = 0.5;
    elseif(strcmp(layer,'fc7'))
        C = 0.25;
    end

    medACC = zeros(length(C),length(gamma));

    %% t: Kernel type. %s: svm type. g: gamma c: cost.
    %  system('svm-train  -s 0 -t 2 -b 1 -q train_scale_bc model_bc');

    for cItr = 1:length(C)
        for gItr = 1:length(gamma)
            cmd = strcat('./svm-train-bc  -t 2 -s 0 -b 1 -g',{' '},num2str(gamma(gItr)),' -c',{' '},num2str(C(cItr)),' -q train_scale_bc model_bc');
            cmd
            system(cmd{1});

            tStart = tic;
              
            %system('svm-predict  -b 1 test_ind_bc_scaled model_bc output_bc.txt > dump_bc');
            system('./svm-predict-bc  -b 0 test_ind_bc_scaled model_bc output_bc.txt > dump_bc');

            runTimeForAllImgs = toc(tStart);

            runTimePerImg = runTimeForAllImgs/length(testY);

            fprintf('Prediction Time for 1 image and all images is %f %d respectively',runTimePerImg, runTimeForAllImgs);

            load output_bc.txt;
            % Predicted scores
            Y_hat=output_bc;
            acc = mean(Y_hat == testY)
            
            medACC(cItr, gItr) = acc;
            system('rm output_bc.txt dump_bc model_bc');
        end
    end
    system('rm train_scale_bc range_bc test_ind_bc.txt train_ind_bc.txt test_ind_bc_scaled');
end