function [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg] = executeSVM1(data,labels, trainTestSplit, trainFile, testFile, layer)
    load(trainFile);
    load(testFile);
    
    trainIndx = TrainingNIter(:,trainTestSplit);
    testIndx = TestingNIter(:,trainTestSplit);
        
    trainY = labels(trainIndx)';
    testY = labels(testIndx)';

    trainFeats = data(trainIndx,:);
    testFeats = data(testIndx,:);

     [size(trainY) size(testY)]
     
     [size(trainFeats) size(testFeats)]
     
    % I want to capture the running time per image.
    [medACC, testY, Y_hat, runTimeForAllImgs, runTimePerImg] = runSVM1(trainFeats,trainY',testFeats,testY',layer);    
end