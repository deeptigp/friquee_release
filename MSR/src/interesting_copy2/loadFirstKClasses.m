load('../interestingness/imNet/Data/fc6Features_10Class.mat');
load('../interestingness/imNet/Data/fc7Features_10Class.mat');
load('../interestingness/imNet/Data/pool5Features_10Class.mat');
load('../interestingness/imNet/Data/groundTruthLabels_10Class.mat');

indx = find(groundTruthLabels <=10);

fc7Features = fc7Features(indx,:);
fc6Features = fc6Features(indx,:);
pool5Features = pool5Features(indx,:);
groundTruthLabels = groundTruthLabels(indx,:);

save(

[medACC7, testY, Y_hat7, runTimeForAllImgs7, runTimePerImg] = executeSVM(fc7Features,groundTruthLabels, 1, '../interestingness/imNet/Data/TrainingNIter_10Class.mat', '../interestingness/imNet/Data/TestingNIter_10Class.mat', 'fc7');

[medACC6, testY, Y_hat6, runTimeForAllImgs6, runTimePerImg] = executeSVM(fc6Features,groundTruthLabels, 1, '../interestingness/imNet/Data/TrainingNIter_10Class.mat', '../interestingness/imNet/Data/TestingNIter_10Class.mat', 'fc6');

[medACC5, testY, Y_hat5, runTimeForAllImgs5, runTimePerImg] = executeSVM(pool5Features,groundTruthLabels, 1, '../interestingness/imNet/Data/TrainingNIter_10Class.mat', '../interestingness/imNet/Data/TestingNIter_10Class.mat', 'pool5');