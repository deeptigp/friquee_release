close all;
hFig = figure(1);
set(hFig, 'Position', [0 0 400 400]);
set(gcf,'Units','centimeters');
set(gcf,'PaperPositionMode','auto');

load('varyThresh.mat');

%% ACCURACY PER AVERAGE TIME.
surf(varyThresh.layer5Thresh', varyThresh.layer6Thresh', varyThresh.accurPerTime')
xlabel('Layer 5 thresholds');
ylabel('Layer 6 thresholds');
zlabel('Accuracy per average time');
savefig('accurPerTime.fig');
%% ACCURACY 

surf(varyThresh.layer5Thresh', varyThresh.layer6Thresh', varyThresh.allAccur')
xlabel('Layer 5 thresholds')
ylabel('Layer 6 thresholds')
zlabel('Accuracy');
savefig('accur.fig');

surf(varyThresh.layer5Thresh', varyThresh.layer6Thresh', varyThresh.allRunTimes')
xlabel('Layer 5 thresholds')
ylabel('Layer 6 thresholds')
zlabel('Running time');
savefig('runningTime.fig');