%%%--- Plots which show the % of labels assigned to pool5, fc6, and fc7.
close all;
hFig = figure(1);
set(hFig, 'Position', [0 0 400 400]);
set(gcf,'Units','centimeters');
set(gcf,'PaperPositionMode','auto');
load('interestingness_kFold_567.mat');

GTLabels = kFold_567.strategy2DerivedLabels;

labels = {'pool5', 'fc6', 'fc7'};
numClasses = length(labels);

[n,x] = hist(GTLabels);
bar(x,n,0.6);
set(gca,'Xtick',[1:1:numClasses],'XTickLabel',[1:numClasses]);
set(gca,'XTickLabel',labels,'FontSize',20);
print(1, '-depsc2','kFold_interestingness.eps');
savefig('kFold_interestingness.fig');

%%-2
load('fineGrained_kFold_567.mat');
GTLabels = kFold_567.derivedLayerLabels;
[n,x] = hist(GTLabels);
bar(x,n,0.6);
set(gca,'Xtick',[1:1:numClasses],'XTickLabel',[1:numClasses]);
set(gca,'XTickLabel',labels,'FontSize',20);
print(1, '-depsc2','kFold_fineGrained.eps');
savefig('kFold_fineGrained.fig');
%%-3
load('imNetAlexNet_kFold_567.mat');
GTLabels = kFold_567.derivedLayerLabels;
[n,x] = hist(GTLabels);
bar(x,n,0.6);
set(gca,'Xtick',[1:1:numClasses],'XTickLabel',[1:numClasses]);
set(gca,'XTickLabel',labels,'FontSize',20);
print(1, '-depsc2','kFold_alexNet.eps');
savefig('kFold_alexNet.fig');
%%--4
load('imNetVggNet_kFold_567.mat');
GTLabels = kFold_567.derivedLayerLabels;
[n,x] = hist(GTLabels);
bar(x,n,0.6);
set(gca,'Xtick',[1:1:numClasses],'XTickLabel',[1:numClasses]);
set(gca,'XTickLabel',labels,'FontSize',20);
print(1, '-depsc2','kFold_vggNet.eps');
savefig('kFold_vggNet.fig');